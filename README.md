# Treehouse

This Rails application provides services used by members of the [Treehouse
Village Ecohousing](https://treehousevillage.ca) community.

## Development setup

1. Set up your local environment using `asdf`. Alternatively, feel free to skip the following and install Ruby, Postgresql and NodeJS manually.

   To set up your environment with `asdf`:
   1. [Install asdf](https://asdf-vm.com/guide/getting-started.html) locally.
   1. Make sure you install [dependencies needed for the asdf Postgres plugin](https://github.com/smashedtoatoms/asdf-postgres#dependencies).
   - If you're a Debian-based distro user, you may also want to add the
     following package: `libpq-dev`.
   1. Install all `asdf` plugins needed for this project, get all needed gems and setup
   your local database by running the following:
   ```bash
   bin/dev_setup
   ```

1. At this point, your local database will be setup for the application. It's
   time to seed it:

   ```bash
   bundle exec rails db:seed
   ```

1. Finally, start your server to access the app by running:

   ```bash
   foreman start
   ```

   The app is now available at [http://localhost:3000](http://localhost:3000).
   To log in with an admin, use `admin@example.com`. Otherwise, use
   `user@example.com`. The password for both these accounts is `password`.

## Tests

To run all tests:

```
bundle exec rspec
```

## Deployments

To deploy `main` to `home.treehousevillage.ca`, simply run the following from
the project root:

```
bin/deploy
```

This script will first run all tests, and if no test fails, then it will proceed
with the deployment.

### What if something is broken after deployment?

No worries! Take a deep breath then run the following command from the project
root:

```
bundle exec cap production deploy:rollback
```

This will revert to the previous deployed revision (side note: the last 5
deployment revisions are kept on the server).

After this rollback has taken effect and you've checked that everything is
working again, please consider writing a test will cover this scenario.

## Maintenance mode

On the server:

```
touch ~/maintenance_is_enabled
```

Remove the file to leave maintenance mode.
