# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.4'

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem 'rails', '~> 8.0.1'

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem 'sprockets-rails'

# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1'

# Use the Puma web server [https://github.com/puma/puma]
gem 'puma', '~> 6.4.1'

# Use JavaScript with ESM import maps [https://github.com/rails/importmap-rails]
gem 'importmap-rails'

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem 'turbo-rails'

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem 'stimulus-rails'

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', require: false

# Use Sass to process CSS
# gem "sassc-rails"

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

gem 'bootstrap', '~> 5.1.3'
gem 'carrierwave', '~> 2.2.2'
gem 'devise', '~> 4.9.3'
gem 'flipper', '~> 1.3.2'
gem 'flipper-active_record', '~> 1.3.2'
gem 'fog-backblaze', github: 'fog/fog-backblaze', branch: 'master'
gem 'haml-rails', '~> 2.0'
gem 'kaminari', '~> 1.2.2'
gem 'pg_search', '~> 2.3.6'
gem 'rails_autolink', '~> 1.1.8'
gem 'rails-settings-cached', '~> 2.9'

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem 'capybara', '~> 3.40.0'
  gem 'debug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 7.0.2'
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem 'web-console'

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"
  gem 'awesome_print'
  gem 'bcrypt_pbkdf', '>= 1.0', '< 2.0'
  gem 'bundler-audit', '~> 0.9.1'
  gem 'capistrano', '~> 3.19.2', require: false
  gem 'capistrano-passenger', '~> 0.2.0'
  gem 'capistrano-rails', '~> 1.4'
  gem 'capistrano-rbenv', '~> 2.1', '>= 2.1.4'
  gem 'ed25519', '>= 1.2', '< 2.0'
  gem 'foreman'
  gem 'letter_opener'
  # gem 'net-ssh', '~> 7.0.0'
  gem 'net-ssh', '7.0.0.beta1'
  gem 'rubocop', '~> 1.36', require: false
  gem 'spring', '~> 4.0.0'
end

group :test do
  gem 'database_cleaner-active_record'
  gem 'factory_bot_rails', '~> 6.2.0'
  gem 'rspec-parameterized', require: false
  gem 'shoulda-matchers', '~> 5.0'
  gem 'test-prof', '~> 1.0.7'
end

gem "solid_queue", "~> 1.0"
