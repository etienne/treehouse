# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VerifyRoomBookingService do
  include ActiveSupport::Testing::TimeHelpers

  let_it_be(:room1) { create(:room, :for_guests) }
  let_it_be(:room2) { create(:room, :for_guests) }

  let(:travel_date) { Date.new(2024, 2, 22) }
  let(:start_date) { travel_date + 3.days }
  let(:end_date) { start_date + 5.days }

  describe '#execute' do
    subject(:execute_service) do
      described_class.new(start_date: start_date, end_date: end_date).execute
    end

    context 'when no prior booking was made' do
      it 'returns the expected hash' do
        travel_to travel_date do
          expect(execute_service).to eq(
            {
              error_message: '',
              date_summary: 'From 25 Feb 2024 to 01 Mar 2024',
              rooms: { room1.id.to_s => true, room2.id.to_s => true }
            }
          )
        end
      end
    end

    context 'when the booking is made in the past' do
      let(:start_date) { travel_date - 4.days }

      it 'returns the expected error message' do
        travel_to travel_date do
          expect(execute_service).to eq(
            {
              error_message: "Sorry, you can't create a booking in the past.",
              date_summary: nil,
              rooms: nil
            }
          )
        end
      end
    end

    context 'when no start time or end time is passed to the service' do
      let(:start_date) { nil }
      let(:end_date) { nil }

      it 'returns the expected error message' do
        expect(execute_service).to eq(
          {
            error_message: 'Error with booking dates.',
            date_summary: nil,
            rooms: nil
          }
        )
      end
    end

    describe 'date availability' do
      using RSpec::Parameterized::TableSyntax

      where(:start_date_offset, :end_date_offset, :availability) do
        -10.days  | -12.days | true
        -2.days   | -2.days  | false
        -8.days   | -8.days  | true
        0.days    | 0.days   | false
        1.days    | -1.day   | false
        2.days    | 2.days   | false
        -2.days   | 2.days   | false
        25.days   | 26.days  | true
      end

      with_them do
        it 'shows this car as being available or not' do
          travel_to travel_date - 20.days do
            existing_booking_start = start_date + start_date_offset
            existing_booking_end = end_date + end_date_offset
            create(:room_booking, start_date: existing_booking_start, end_date: existing_booking_end, room: room1)

            expect(execute_service[:rooms]).to eq(
              { room1.id.to_s => availability, room2.id.to_s => true }
            )
          end
        end
      end
    end

    context 'when all the roms are already booked for the given time frame' do
      it 'shows the related error message' do
        travel_to travel_date do
          create(:room_booking, start_date: start_date, end_date: end_date, room: room1)
          create(:room_booking, start_date: start_date, end_date: end_date, room: room2)

          expect(execute_service).to eq(
            {
              error_message: 'Sorry, there is no room available for the dates you have selected.',
              date_summary: nil,
              rooms: nil
            }
          )
        end
      end
    end

    context 'when trying to book more than 10 nights in one time' do
      let(:end_date) { start_date + 15.days }

      it 'shows the related error message' do
        travel_to travel_date do
          expect(execute_service).to eq(
            {
              error_message: 'You cannot book more than 10 nights in a row.',
              date_summary: nil,
              rooms: nil
            }
          )
        end
      end
    end

    context 'when trying to book the same day' do
      let(:end_date) { start_date }

      it 'shows the related error message' do
        travel_to travel_date do
          expect(execute_service).to eq(
            {
              error_message: 'Please select different dates to book at least one night.',
              date_summary: nil,
              rooms: nil
            }
          )
        end
      end
    end
  end
end
