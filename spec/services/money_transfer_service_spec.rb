# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MoneyTransferService do
  let(:sender) { create(:fund, balance: 100.00) }
  let(:recipient) { create(:fund, balance: 100.00) }
  let(:sender_description) { 'a description for sender' }
  let(:recipient_description) { 'a description for recipient' }
  let(:interac) { nil }
  let(:amount) { 20.00 }
  let(:service) do
    described_class.new(
      amount: amount,
      sender_fund: sender,
      sender_description: sender_description,
      recipient_description: recipient_description,
      recipient_fund: recipient,
      interac: interac
    )
  end

  describe '#execute' do
    subject(:execute_service) { service.execute }

    context 'when the amount is non-numerical' do
      let(:amount) { 'fake' }

      it 'returns an error' do
        expect(execute_service).to eq(
          { success: false, error: 'Please choose a valid amount to add to a fund.' }
        )
      end
    end

    context 'when the amount is negative' do
      let(:amount) { -12.50 }

      it 'returns an error' do
        expect(execute_service).to eq(
          { success: false, error: 'Please choose a valid amount to add to a fund.' }
        )
      end
    end

    context 'when the amount is positive' do
      it 'proceeds with the transfer' do
        execute_service

        expect(sender.reload.balance).to eq(80.00)
        expect(recipient.reload.balance).to eq(120.00)
        expect(Transfer.last.recipient_description).to eq('[From John] a description for recipient')
        expect(Transfer.last.sender_description).to eq('[To John] a description for sender')
      end

      it 'returns true' do
        expect(execute_service).to eq({ success: true })
      end

      context 'when a SenderNotEnoughMoneyError is raised' do
        it 'returns the expect response' do
          allow_any_instance_of(Transfer).to receive(:pay_now!).and_raise(Transfer::SenderNotEnoughMoneyError)

          expect(execute_service).to eq(
            { success: false, error: 'Transfer cancelled: not enough money in sender\'s fund.' }
          )
        end

        context 'when there is a model error' do
          let(:interac) { 'abc' }

          it 'returns the model error' do
            expect(execute_service).to eq(
              { success: false, error: 'Interac number must be between 6 and 20 characters.' }
            )
          end
        end
      end
    end
  end

  describe '#add_money_to_fund' do
    subject(:add_money_to_fund) { service.add_money_to_fund }

    context 'with an interac number' do
      let(:interac) { 'abcdefghi' }

      context 'when the recipient fund is a community fund' do
        let(:recipient) { create(:fund, :community, balance: 100.00) }

        it 'returns the right recipient message' do
          add_money_to_fund

          expect(Transfer.last.recipient_description).to eq('[From John] a description for recipient (Interac: *****fghi)')
        end
      end

      context 'when the recipient fund is a user fund' do
        it 'returns the right recipient message' do
          add_money_to_fund

          expect(Transfer.last.recipient_description).to eq('[From John] a description for recipient (Interac: *****fghi)')
        end
      end
    end
  end
end
