# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarChargingService do
  let(:expected_distance) { 25 }
  let(:car_booking) { create(:car_booking, expected_distance: expected_distance) }
  let(:service) { described_class.new(car_booking) }

  describe '#event_data' do
    subject(:event_data) { service.event_data }

    it 'returns the expected hash' do
      expected_hash = {
        title: 'Charge time',
        carname: car_booking.car.title,
        start: car_booking.booking_end_time,
        end: car_booking.booking_end_time + (30 * 60),
        booking_id: "#{car_booking.id}-charging",
        eventtype: 'charging',
        chargingduration: '30 mins',
        color: '#456789'
      }

      expect(event_data).to eq(expected_hash)
    end

    describe 'charge time calculation' do
      using RSpec::Parameterized::TableSyntax

      where(:expected_distance, :suggested_charge_time_mins) do
         2 |  0
         5 |  0
        10 | 30
        20 | 30
        30 | 60
        50 | 90
      end

      with_them do
        it 'returns the relevant charge timem, in minutes, for a given distance in kms' do
          expect(event_data[:chargingduration]).to eq("#{suggested_charge_time_mins} mins")
        end
      end
    end
  end
end
