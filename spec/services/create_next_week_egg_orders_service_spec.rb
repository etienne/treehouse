# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateNextWeekEggOrdersService do
  include ActiveSupport::Testing::TimeHelpers

  let(:service) { described_class.new }

  describe '#execute' do
    subject(:execute_service) { service.execute }

    let(:schedule_data) do
      [
        {
          'delivery_day' => :tuesday,
          'cost' => 6.0,
          'closing_hour_on_day_before' => 14
        },
        {
          'delivery_day' => :friday,
          'cost' => 3.5,
          'closing_hour_on_day_before' => 6
        }
      ]
    end

    let(:date_creation) { Date.new(2024, 4, 18) } # Thursday April 18th 2024

    before do
      allow(service).to receive(:delivery_data).and_return(schedule_data)
    end

    it 'creates egg orders for next week based on data file' do
      travel_to date_creation do
        expect(execute_service).to eq({ success: true })

        orders = EggOrder.last(2)

        tuesday_order = orders[0]
        friday_order = orders[1]

        expect(tuesday_order.purchase_date).to eq(Date.new(2024, 4, 23))
        expect(tuesday_order.closing_time).to eq(DateTime.new(2024, 4, 22, 14, 0, 0, '-0300'))
        expect(tuesday_order.price).to eq(6.0)

        expect(friday_order.purchase_date).to eq(Date.new(2024, 4, 26))
        expect(friday_order.closing_time).to eq(DateTime.new(2024, 4, 25, 6, 0, 0, '-0300'))
        expect(friday_order.price).to eq(3.5)
      end
    end
  end
end
