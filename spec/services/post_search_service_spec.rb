# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostSearchService do
  describe '#execute' do
    let_it_be(:user) { create(:user) }
    let_it_be(:category1) { create(:item_category, name: 'category 1') }
    let_it_be(:category2) { create(:item_category, name: 'category 2') }
    let_it_be(:post1a) { create(:post, item_category: category1, user: user) }
    let_it_be(:post1b) { create(:post, item_category: category1, user: user) }
    let_it_be(:post2a) { create(:post, item_category: category2, user: user) }

    let(:params) { {} }

    subject(:post_search) { described_class.new(params).execute }

    context 'when no param is passed in' do
      it 'returns everything' do
        expect(post_search[:posts]).to contain_exactly(post1a, post1b, post2a)
      end
    end

    context 'when an item category is passed in' do
      let(:params) { { category: category1.id } }

      it 'returns expected results' do
        expect(post_search[:posts]).to contain_exactly(post1a, post1b)
      end
    end
  end

  describe '.number_of_results_per_page' do
    it 'returns the expected number' do
      expect(described_class.number_of_results_per_page).to eq(10)
    end
  end
end
