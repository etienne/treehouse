# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VerifyAreaBookingService do
  include ActiveSupport::Testing::TimeHelpers

  let_it_be(:room1) { create(:room, :ch_areas) }
  let_it_be(:room2) { create(:room, :ch_areas) }

  let(:travel_datetime) { DateTime.new(2024, 2, 22, 0, 0, 0, '-0400') }
  let(:start_time) { travel_datetime + 10.hours }
  let(:end_time) { travel_datetime + 13.hours }

  describe '#execute' do
    subject(:execute_service) do
      described_class.new(start_time: start_time, end_time: end_time).execute
    end

    context 'when no prior booking was made' do
      it 'returns the expected hash' do
        travel_to travel_datetime do
          expect(execute_service).to eq(
            {
              error_message: '',
              booking_datetime: 'Thu February 22, from 10:00 AM to 1:00 PM',
              rooms: { room1.id.to_s => true, room2.id.to_s => true }
            }
          )
        end
      end
    end

    context 'when the booking is made in the past' do
      let(:start_time) { travel_datetime - 4.days }

      it 'returns the expected error message' do
        travel_to travel_datetime do
          expect(execute_service).to eq(
            {
              error_message: "Sorry, you can't create a booking in the past.",
              booking_datetime: nil,
              rooms: nil
            }
          )
        end
      end
    end

    context 'when the booking is too long' do
      let(:end_time) { travel_datetime + 17.hours }

      it 'returns the expected error message' do
        travel_to travel_datetime do
          expect(execute_service).to eq(
            {
              error_message: 'You cannot book an area for more than 4 hours.',
              booking_datetime: nil,
              rooms: nil
            }
          )
        end
      end
    end

    context 'when no start time or end time is passed to the service' do
      let(:start_time) { nil }
      let(:end_time) { nil }

      it 'returns the expected error message' do
        expect(execute_service).to eq(
          {
            error_message: 'Error with booking times.',
            booking_datetime: nil,
            rooms: nil
          }
        )
      end
    end

    describe 'date availability' do
      using RSpec::Parameterized::TableSyntax

      where(:start_time_offset, :end_time_offset, :availability) do
        -10.hours  | -10.hours | true
        -2.hours   | -2.hours  | false
        0.hours    | 0.hours   | false
        1.hour     | -1.hour   | false
        2.hours    | 2.hours   | false
        -2.hours   | -2.hours  | false
        25.hours   | 25.hours  | true
      end

      with_them do
        it 'shows this area as being available or not' do
          travel_to travel_datetime - 20.days do
            existing_booking_start = start_time + start_time_offset
            existing_booking_end = end_time + end_time_offset
            create(:area_booking, start_time: existing_booking_start, end_time: existing_booking_end, room: room1)

            expect(execute_service[:rooms]).to eq(
              { room1.id.to_s => availability, room2.id.to_s => true }
            )
          end
        end
      end
    end

    context 'when all the roms are already booked for the given time frame' do
      it 'shows the related error message' do
        travel_to travel_datetime do
          create(:area_booking, start_time: start_time, end_time: end_time, room: room1)
          create(:area_booking, start_time: start_time, end_time: end_time, room: room2)

          expect(execute_service).to eq(
            {
              error_message: 'Sorry, there is no CH area available for the times you have selected.',
              booking_datetime: nil,
              rooms: nil
            }
          )
        end
      end
    end

    context 'when trying to book the same day' do
      let(:end_time) { start_time }

      it 'shows the related error message' do
        travel_to travel_datetime do
          expect(execute_service).to eq(
            {
              error_message: 'Please select different start and end times.',
              booking_datetime: nil,
              rooms: nil
            }
          )
        end
      end
    end
  end
end
