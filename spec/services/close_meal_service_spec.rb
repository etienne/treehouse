# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CloseMealService do
  describe '#execute' do
    let(:balance) { 120.0 }
    let!(:meal) { create(:meal, cost: 100.0) }
    let!(:common_meals_fund) { create(:fund, :community, :common_meals, balance: balance) }

    subject(:close_meal) { described_class.new(meal: meal).execute }

    before do
      create(:meal_payment, meal: meal, amount: 20.0)
      create(:meal_payment, meal: meal, amount: 50.0)
    end

    context 'when the meal is already closed' do
      let!(:meal) { create(:meal, cost: 100.0, closed: true) }

      it 'returns the relevant response' do
        expect(close_meal).to eq({ success: false, error: 'This meal is already closed.' })
      end
    end

    context 'when the meal is not closed yet' do
      it 'executes the relevant money transfer' do
        expect(MoneyTransferService).to receive(:new).with(
          amount: 100.0,
          sender_fund: Fund.common_meals,
          sender_description: "Reimbursement for #{meal.meal_date.strftime('%b %d')} meal."
        ).and_call_original

        close_meal
      end

      it 'closes the meal' do
        expect(meal).not_to be_closed

        close_meal

        expect(meal).to be_closed
      end
    end
  end
end
