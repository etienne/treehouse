# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MealTeamService do
  let!(:seating1) { create(:meal_seating, meal_time: Date.today + 17.hours) }
  let!(:seating2) { create(:meal_seating, meal_time: Date.today + 19.hours) }
  let(:meal) { create(:meal, meal_seatings: [seating1, seating2]) }
  let(:service) { described_class.new(meal: meal) }

  subject(:execute_service) { service.execute }

  describe '#execute' do
    before do
      allow(service).to receive(:meal_team_input).and_return(data_input)
    end

    context 'when no prep and no cleaners are provided' do
      let(:data_input) do
        {
          'preps' => 0,
          'cleaners_by_seating' => 0,
          'finish_cleaners' => 0
        }
      end

      it 'still creates the related meal_team_members' do
        expect { execute_service }.to change { MealTeamMember.count }.by(2)

        expect(MealTeamMember.all.map(&:role)).to contain_exactly('Meal lead', 'Service lead')
      end
    end

    context 'when preps and cleaners are provided' do
      let(:data_input) do
        {
          'preps' => 2,
          'cleaners_by_seating' => 2,
          'finish_cleaners' => 1
        }
      end

      context 'when there are two sittings for the meal' do
        it 'creates the related meal_team_members' do
          expect { execute_service }.to change { MealTeamMember.count }.by(9)

          expect(MealTeamMember.all.map(&:role)).to contain_exactly(
            'Meal lead',
            'Service lead',
            'Meal prep 1',
            'Meal prep 2',
            '5.00 pm seating clean-up 1',
            '5.00 pm seating clean-up 2',
            '7.00 pm seating clean-up 1',
            '7.00 pm seating clean-up 2',
            '7.00 pm seating clean-up 3'
          )
        end
      end
    end
  end
end
