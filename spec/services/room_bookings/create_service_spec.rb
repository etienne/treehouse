# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RoomBookings::CreateService do
  let_it_be(:user) { create(:user) }

  let(:booking) { build(:room_booking, start_date: start_date, end_date: start_date + 5.days) }
  let(:start_date) { Date.current + 1.month }

  describe '#execute' do
    subject(:execute_service) { described_class.new(booking, user: user).execute }

    it 'saves a room booking linked to passed in user' do
      expect { execute_service }.to change { RoomBooking.count }.by(1)
      expect(RoomBooking.last.user).to eq(user)
    end

    context 'when the booking is made more than 4 months in advance' do
      let(:start_date) { Date.current + 5.month }

      it 'saves a pending booking' do
        saved_booking = execute_service

        expect(saved_booking.state).to eq('pending')
      end
    end

    context 'when the booking is made less than 4 months in advance' do
      it 'saves a confirmed booking' do
        saved_booking = execute_service

        expect(saved_booking.state).to eq('confirmed')
      end
    end
  end
end
