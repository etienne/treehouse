# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VerifyCarBookingService do
  let_it_be(:car1) { create(:car) }
  let_it_be(:car2) { create(:car) }

  let(:start_time) { DateTime.new(2033, 6, 20, 9, 0, 0, '-0300') }
  let(:end_time) { DateTime.new(2033, 6, 20, 14, 0, 0, '-0300') }

  describe '#execute' do
    subject(:execute_service) do
      described_class.new(start_time: start_time, end_time: end_time).execute
    end

    context 'when no prior booking was made' do
      it 'returns the expected hash' do
        expect(execute_service).to eq(
          {
            error_message: '',
            time_summary: 'Mon Jun 20, from 9:00 AM to 2:00 PM',
            cars: { car1.id.to_s => true, car2.id.to_s => true }
          }
        )
      end
    end

    context 'when the booking is made in the past' do
      let(:start_time) { DateTime.new(2020, 6, 20, 9, 0, 0, '-0300') }

      it 'returns the expected error message' do
        expect(execute_service).to eq(
          {
            error_message: "Sorry, you can't create a booking in the past.",
            time_summary: nil,
            cars: nil
          }
        )
      end
    end

    context 'when no start time or end time is passed to the service' do
      let(:start_time) { nil }
      let(:end_time) { nil }

      it 'returns the expected error message' do
        expect(execute_service).to eq(
          {
            error_message: 'Error with booking dates.',
            time_summary: nil,
            cars: nil
          }
        )
      end
    end

    describe 'time availability' do
      using RSpec::Parameterized::TableSyntax

      where(:start_time_offset, :end_time_offset, :availability) do
        -30.hours | -25.hours | true
        -2.hours  | -2.hours  | false
        0.hour    | 0.hour    | false
        1.hour    | -1.hour   | false
        2.hours   | 2.hours   | false
        -2.hours  | 2.hours   | false
        25.hours  | 28.hours  | true
      end

      with_them do
        it 'shows this car as being available or not' do
          existing_booking_start = start_time + start_time_offset
          existing_booking_end = end_time + end_time_offset
          create(:car_booking, start_time: existing_booking_start, end_time: existing_booking_end, car: car1)

          expect(execute_service[:cars]).to eq(
            { car1.id.to_s => availability, car2.id.to_s => true }
          )
        end
      end
    end

    context 'when all the cars are already booked for the given time frame' do
      it 'shows the related error message' do
        create(:car_booking, start_time: start_time, end_time: end_time, car: car1)
        create(:car_booking, start_time: start_time, end_time: end_time, car: car2)

        expect(execute_service).to eq(
          {
            error_message: 'Sorry, there is no car available for the time you have selected.',
            time_summary: nil,
            cars: nil
          }
        )
      end
    end
  end
end
