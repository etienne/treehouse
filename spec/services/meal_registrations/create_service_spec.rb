# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MealRegistrations::CreateService do
  let(:meal) { create(:meal) }
  let(:user) { create(:user, :with_household, first_name: 'John') }
  let!(:other_user) { create(:user, first_name: 'Alice', household: user.household) }
  let!(:meal_seating) { create(:meal_seating, meal: meal) }
  let(:portions) do
    build_list(:meal_portion, 2) do |portion, i|
      portion.description = "Choice #{i}"
      portion.amount = 5.0 + i # $5 for first portion, $6 for second.
      portion.meal = meal
      portion.save!
    end
  end

  let(:params) do
    {
      choices: { portions.first.id.to_s => 2, portions.last.id.to_s => 3 },
      seating: meal_seating.id,
      people: %w[John Alice]
    }
  end

  subject(:execute_service) do
    described_class.new(
      meal: meal,
      user: user,
      params: params
    ).execute
  end

  before do
    user.fund.update(balance: balance)
  end

  describe '#execute' do
    context 'when there is not enough fund' do
      let(:balance) { 1.0 }

      it 'returns false and an error message' do
        result = execute_service

        expect(result[:success]).to be_falsey
        expect(result[:errors].first)
          .to eq('You don\'t have enough money to pay for this meal. Please top-up your personal fund and try again.')
      end
    end

    context 'when there is enough fund' do
      let(:balance) { 150.0 }

      it 'creates meal portions choices' do
        expect { execute_service }.to change { MealPortionChoice.count }.by(2)

        expect(MealPortionChoice.first.quantity).to eq(2)
        expect(MealPortionChoice.last.quantity).to eq(3)
      end

      it 'creates meal payment attendees' do
        expect { execute_service }.to change { MealPaymentAttendee.count }.by(2)

        expect(MealPaymentAttendee.last(2).map(&:attendee)).to contain_exactly(user, other_user)
      end

      it 'creates a meal payment and triggers it' do
        expect { execute_service }.to change { MealPayment.count }.by(1)

        payment = MealPayment.last

        expect(payment).to be_paid

        attendees = payment.reload.meal_payment_attendees.map(&:attendee)
        names = attendees.map(&:first_name).join(', ')
        expect(names).to eq('John, Alice')
        expect(payment.amount).to eq(28.0)
      end

      it 'returns true' do
        expect(execute_service).to be_truthy
      end

      context 'when a MealPortion objcet cannot be found' do
        it 'raises an exception' do
          allow(MealPortion).to receive(:find_by).and_return(nil)

          expect { execute_service }.to raise_error(described_class::MealPortionInvalid)
        end
      end
    end
  end
end
