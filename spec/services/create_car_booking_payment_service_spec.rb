# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateCarBookingPaymentService do
  let(:user) { create(:user) }
  let(:period_start_date) { Date.current }
  let(:period_end_date) { Date.current + 3.days }

  subject(:execute_service) do
    described_class.new(
      user: user,
      period_start_date: period_start_date,
      period_end_date: period_end_date
    ).execute
  end

  context 'when there is no booking summary' do
    it 'returns no payment' do
      allow(CarBooking).to receive(:booking_summary_for).and_return(nil)
      expect(CarBookingPayment).not_to receive(:create)

      expect(execute_service).to eq({ success: true })
    end
  end

  context 'when there is a booking summary' do
    it 'returns the relevant payment' do
      booking_summary = [{ cost: 4.5 }, { cost: 10.0 }]
      allow(CarBooking).to receive(:booking_summary_for).and_return(booking_summary)

      result = execute_service

      expect(result[:success]).to be_truthy
      expect(result[:payment]).to have_attributes(
        user: user,
        period_start: period_start_date,
        period_end: period_end_date,
        amount: 14.5
      )
    end
  end
end
