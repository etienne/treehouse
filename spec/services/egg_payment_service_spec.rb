# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EggPaymentService do
  include ActiveSupport::Testing::TimeHelpers

  describe '#execute' do
    let_it_be(:user) { create(:user) }

    let(:egg_order) { create(:egg_order) }
    let(:number_of_boxes) { 2 }

    subject(:service) do
      described_class.new(
        user: user,
        number_of_boxes: number_of_boxes,
        egg_order_id: egg_order.id
      )
    end

    before do
      user.fund.update(balance: 100.0)
    end

    context 'when the params are correct' do
      it 'creates an egg payment' do
        expect(service.execute).to eq({ success: true, paid: true })

        expect(EggPayment.last.reload).to have_attributes(
          egg_order_id: egg_order.id,
          user_id: user.id,
          number_of_boxes: number_of_boxes
        )
      end

      context 'when we are past the deadline' do
        let(:past_deadline) { egg_order.closing_time + 1.hour }

        it 'does not create an egg payment and return an error message' do
          travel_to past_deadline do
            expect(EggPayment).not_to receive(:create!)

            expect(service.execute).to eq(
              {
                success: false,
                error: 'Sorry, you cannot order these eggs, as we are past registration time for this order.'
              }
            )
          end
        end
      end
    end

    context 'when the egg order is not next week' do
      let(:egg_order) { create(:egg_order, purchase_date: Date.current - 3.days) }

      it 'does not create an EggPayment object' do
        expect(EggPayment).not_to receive(:create!)

        expect(service.execute).to eq(
          {
            success: false,
            error: 'Sorry, an error occurred. Registration for egg order has not been processed.'
          }
        )
      end
    end

    context 'when the passed number of boxes is not a number' do
      let(:number_of_boxes) { 'word' }

      it 'does not create an EggPayment object' do
        expect(service.execute).to eq(
          {
            success: false,
            error: 'Sorry, an error occurred. Registration for egg order has not been processed.'
          }
        )
      end
    end
  end
end
