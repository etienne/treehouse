# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@example.com" }
    first_name { 'John' }
    last_name { 'Doe' }
    password { 'password' }
    confirmed_at { DateTime.now }
    phone_number { nil }
    roles { [] }

    trait :random do
      roles { [Role.find_by(slug: 'random') || create(:role, :random)] }
    end

    trait :guest_room_coordinator do
      roles { [Role.find_by(slug: 'guest_room_coordinator') || create(:role, :guest_room_coordinator)] }
    end

    trait :area_booking_coordinator do
      roles { [Role.find_by(slug: 'area_booking_coordinator') || create(:role, :area_booking_coordinator)] }
    end

    trait :car_admin do
      roles { [Role.find_by(slug: 'car_admin') || create(:role, :car_admin)] }
    end

    trait :egg_admin do
      roles { [Role.find_by(slug: 'egg_admin') || create(:role, :egg_admin)] }
    end

    trait :fund_admin do
      roles { [Role.find_by(slug: 'fund_admin') || create(:role, :fund_admin)] }
    end

    trait :with_household do
      household { create(:household) }
    end
  end
end
