# frozen_string_literal: true

FactoryBot.define do
  factory :room_booking do
    start_date { Date.current }
    end_date { start_date + 1.week }
    description { 'This is a test description' }
    user { User.last || create(:user) }
    room { create(:room) }
    confirmed_at { nil }
    free { false }

    trait :confirmed do
      confirmed_at { Date.current }
    end

    trait :free do
      free { true }
    end
  end
end
