# frozen_string_literal: true

FactoryBot.define do
  factory :meal_payment do
    amount { 100.0 }
    state { 'pending' }
    meal
    user
    household
    meal_seating
  end
end
