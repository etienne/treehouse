# frozen_string_literal: true

FactoryBot.define do
  factory :car do
    title { '2015 Toyoto Corolla' }
    description { 'Make Model Year...' }
    odometer { 85_000 }
    calendar_color { '#123456' }
    photos { [] }
    documents { [] }
    electric { true }
  end
end
