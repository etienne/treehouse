# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    title { 'Post title' }
    description { 'This is the post description.' }
    price { 10.00 }
    user
    item_category
    photos { [] }
  end
end
