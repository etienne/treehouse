# frozen_string_literal: true

FactoryBot.define do
  factory :room do
    name { 'Guest room 1' }
    slug { 'guestroomone' }
    description { 'This is a room for testing' }
    calendar_color { '#123456' }
    purpose { 'purpose' }

    trait :for_guests do
      purpose { 'guest' }
    end

    trait :ch_areas do
      purpose { 'ch_area' }
    end
  end
end
