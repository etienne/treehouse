# frozen_string_literal: true

FactoryBot.define do
  factory :meal_seating do
    meal_time { DateTime.now }
    meal
  end
end
