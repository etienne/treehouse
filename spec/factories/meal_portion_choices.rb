# frozen_string_literal: true

FactoryBot.define do
  factory :meal_portion_choice do
    meal_payment
    meal_portion
    quantity { 1 }
  end
end
