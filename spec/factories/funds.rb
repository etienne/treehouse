# frozen_string_literal: true

FactoryBot.define do
  factory :fund do
    balance { 250.0 }
    archived { false }
    community { false }
    user

    trait :community do
      community { true }
    end

    trait :common_meals do
      name { 'Meals' }
    end
  end
end
