# frozen_string_literal: true

FactoryBot.define do
  factory :car_booking_payment do
    user
    period_start { Date.current.beginning_of_week }
    period_end { Date.current.end_of_week }
    amount { 25.0 }
  end
end
