# frozen_string_literal: true

FactoryBot.define do
  factory :eligibility do
    email { 'test@example.com' }
  end
end
