# frozen_string_literal: true

FactoryBot.define do
  factory :item_category do
    name { 'Item category' }
    description { 'The item category description.' }
  end
end
