# frozen_string_literal: true

FactoryBot.define do
  factory :meal do
    title { 'Meal title' }
    description { 'This is a description about the meal' }
    meal_date { Date.current + 3.days }
    cost { 100.0 }
    user
  end
end
