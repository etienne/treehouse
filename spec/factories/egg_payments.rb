# frozen_string_literal: true

FactoryBot.define do
  factory :egg_payment do
    egg_order { build(:egg_order) }
    user
    number_of_boxes { 3 }
    state { 0 }

    trait :pending do
      state { 2 }
    end
  end
end
