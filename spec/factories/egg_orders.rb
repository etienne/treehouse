# frozen_string_literal: true

FactoryBot.define do
  factory :egg_order do
    purchase_date { Date.current + 3.days }
    closing_time { DateTime.now + 2.days }
    price { 5.5 }
  end
end
