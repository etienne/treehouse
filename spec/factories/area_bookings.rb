# frozen_string_literal: true

FactoryBot.define do
  factory :area_booking do
    start_time { DateTime.now + 1.hour }
    end_time { start_time + 2.hours }
    description { 'This is a test description' }
    name { 'Birthday party' }
    user { User.last || create(:user) }
    room { create(:room) }
  end
end
