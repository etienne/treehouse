# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    name { 'Test' }
    slug { 'test' }

    trait :super do
      slug { 'super' }
    end

    trait :random do
      slug { 'random' }
    end

    trait :guest_room_coordinator do
      slug { 'guest_room_coordinator' }
    end

    trait :area_booking_coordinator do
      slug { 'area_booking_coordinator' }
    end

    trait :car_admin do
      slug { 'car_admin' }
    end

    trait :egg_admin do
      slug { 'egg_admin' }
    end

    trait :fund_admin do
      slug { 'fund_admin' }
    end
  end
end
