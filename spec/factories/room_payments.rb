# frozen_string_literal: true

FactoryBot.define do
  factory :room_payment do
    description { 'Payment from this room booking.' }
    amount { 45.00 }
    user
    room_booking
    state { RoomPayment.states[:paid] }

    trait :pending do
      state { RoomPayment.states[:pending] }
    end
  end
end
