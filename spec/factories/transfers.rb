# frozen_string_literal: true

FactoryBot.define do
  factory :transfer do
    amount { 100.00 }
    association :sender
    association :recipient
    recipient_new_balance { 90.0 }
    sender_new_balance { 70.0 }
    interac { nil }

    trait :no_sender do
      sender { nil }
      sender_new_balance { nil }
    end

    trait :no_recipient do
      recipient { nil }
      recipient_new_balance { nil }
    end
  end
end
