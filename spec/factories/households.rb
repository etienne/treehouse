# frozen_string_literal: true

FactoryBot.define do
  factory :household do
    name { 'The Simpson' }
  end
end
