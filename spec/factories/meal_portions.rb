# frozen_string_literal: true

FactoryBot.define do
  factory :meal_portion do
    description { 'Small portion' }
    amount { 2.5 }
    meal
  end
end
