# frozen_string_literal: true

FactoryBot.define do
  factory :car_booking do
    start_time { DateTime.now + 2.hours }
    end_time { start_time + 3.hours }
    user { User.last || create(:user) }
    car { Car.last || create(:car) }
    note { 'this is a note' }
    expected_distance { 10 }
  end
end
