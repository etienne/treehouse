# frozen_string_literal: true

require 'rails_helper'

describe 'Treehouse user pages' do
  using RSpec::Parameterized::TableSyntax

  let(:role) { nil }
  let(:content_if_not_role) { nil }

  shared_examples 'accessing page' do
    it 'visits the page or redirects' do
      visit path

      expected_content = role ? content_if_not_role : content
      expect(page).to have_content expected_content

      if role
        r = Role.create(slug: role, name: 'Role')
        user.roles << r

        visit path
        expect(page).to have_content content
      end
    end
  end

  context 'when not logged in' do
    it_behaves_like 'accessing page' do
      let(:path) { '/' }
      let(:content) { 'Log in to Treehouse' }
    end

    it_behaves_like 'accessing page' do
      let(:path) { '/users/password/new' }
      let(:content) { 'Did you forget your password?' }
    end

    it_behaves_like 'accessing page' do
      let(:path) { '/users/sign_up' }
      let(:content) { 'Sign up to Treehouse' }
    end
  end

  context 'when logged-in' do
    let(:user) { create(:user) }

    before do
      login_as(user, scope: :user)
    end

    where(:path, :content, :role, :content_if_not_role) do
      '/' | 'Home page' | nil | nil
      '/links' | 'Links to Treehouse resources' | nil | nil
      '/transfers/new' | 'Send money from your fund' | nil | nil
      '/cars' | 'Cars at Treehouse' | nil | nil
      '/bookings/cars' | 'Book a car' | nil | nil
      '/bookings/guests' | 'Book a guest room' | nil | nil
      '/bookings/areas' | 'Book a Common House area' | nil | nil
      '/marketplace' | 'Marketplace' | nil | nil
      '/funds' | 'Funds and transactions' | nil | nil
      '/users/edit' | 'Edit my profile' | nil | nil
      '/settings' | 'My settings' | nil | nil
      '/rooms' | 'Rooms at Treehouse' | nil | nil
      '/meals' | 'Meals' | nil | nil
      '/meals/new' | 'Organize a common meal' | nil | nil
      '/cars/new' | 'Add a new car' | :car_admin | 'You cannot add a car or edit an existing one'
      # Admin pages
      '/admin/guest_rooms' | 'Coordinate guest rooms' | :guest_room_coordinator | 'You cannot coordinate guest rooms'
      '/admin/money' | 'Add money to a fund' | :fund_admin | 'You cannot manage funds'
      '/admin/egg_orders' | 'Manage egg orders' | :egg_admin | 'You cannot manage egg orders'
      '/admin/area_bookings' | 'Coordinate CH area bookings' | :area_booking_coordinator | 'You cannot coordinate area bookings'
    end

    with_them { it_behaves_like 'accessing page' }
  end
end
