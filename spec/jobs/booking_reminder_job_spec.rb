# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BookingReminderJob, type: :job do
  include ActiveJob::TestHelper

  describe '#perform' do
    let(:bookings) do
      [create(:room_booking),
       create(:room_booking)]
    end

    before do
      allow(RoomBooking).to receive(:to_be_reminded_about_today).and_return(bookings)
    end

    subject(:job) { described_class.perform_later }

    it 'enqueues the job' do
      expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
    end

    it 'calls last reminder on all returned bookings' do
      bookings.each do |booking|
        expect(RoomBookingMailer).to receive(:last_reminder)
          .with(booking).and_return(double('mailer', deliver_later: true))
      end

      perform_enqueued_jobs { job }
    end
  end
end
