# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateCarBookingPaymentJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  it 'enqueues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  context 'when there was a car booking last week' do
    let(:last_week_start) { Date.current.beginning_of_week - 5.days }
    let(:car) { create(:car) }
    let!(:user) { create(:user) }
    let(:job) { described_class.new }

    before do
      booking = build(
        :car_booking,
        start_time: last_week_start.to_time,
        end_time: last_week_start.to_time + 4.hours,
        user: user
      )

      booking.save!(validate: false)
    end

    it 'calls the create car payment service with the expected args' do
      expect(CreateCarBookingPaymentService).to receive(:new).with(
        user: user,
        period_start_date: last_week_start.beginning_of_week,
        period_end_date: last_week_start.end_of_week
      ).and_call_original

      expect_any_instance_of(CreateCarBookingPaymentService) do |service|
        expect(service).to receive(:execute)
      end

      job.perform

      expect(CarBookingPayment.last).to have_attributes(
        period_start: last_week_start.beginning_of_week,
        period_end: last_week_start.end_of_week,
        user: user,
        amount: 32.0
      )
    end

    context 'when the user is exempted from payments' do
      before do
        allow(job).to receive(:exempted_from_car_payments?).and_return(true)
      end

      it 'does not run a car payment service nor the mailer' do
        expect(CreateCarBookingPaymentService).not_to receive(:new)
        expect(CarBookingMailer).not_to receive(:send_car_booking_usage_summary)

        job.perform
      end
    end
  end
end
