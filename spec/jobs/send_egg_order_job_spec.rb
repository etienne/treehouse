# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SendEggOrderJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  it 'enqueues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'sends an email to users with egg setting on' do
    allow(Flipper).to receive(:enabled?).and_return(true)

    user1 = create(:user)
    user2 = create(:user)

    user2.settings.update(eggs: true)

    allow(EggsMailer).to receive(:request_for_next_week).with(recipient: user2)

    expect(EggsMailer).not_to receive(:request_for_next_week).with(recipient: user1)
    expect(EggsMailer)
      .to receive(:request_for_next_week)
      .with(recipient: user2).and_return(double('mailer', deliver_later: true))

    perform_enqueued_jobs { job }
  end
end
