# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateEggOrderJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  it 'enqueues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'calls the egg order creation service' do
    expect_any_instance_of(CreateNextWeekEggOrdersService) do |service|
      expect(service).to receive(:execute)
    end

    job
  end
end
