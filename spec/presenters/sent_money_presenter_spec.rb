# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SentMoneyPresenter do
  let(:sender_fund) { create(:fund, balance: 60.0) }
  let(:recipient) { create(:user, first_name: 'John', last_name: 'Doe') }
  let(:recipient_fund) { create(:fund, user: recipient, name: recipient.full_name) }
  let(:recipient_description) { nil }
  let(:sender_description) { nil }
  let!(:transfer) do
    create(
      :transfer,
      sender: sender_fund,
      recipient: recipient_fund,
      sender_description: sender_description,
      recipient_description: recipient_description,
      sender_new_balance: 40.0,
      amount: 20.0
    )
  end

  describe '#present' do
    it 'returns the related data' do
      expect(described_class.new(fund: sender_fund).present).to eq(
        [{
          timestamp: transfer.created_at.to_i,
          date: transfer.created_at.strftime('%e %b %Y'),
          description: 'Money sent to John Doe.',
          plus_or_minus: '-',
          amount: 20.0,
          new_balance: 40.0
        }]
      )
    end

    context 'when a description was persisted' do
      let(:recipient_description) { 'a custom description for recipient' }
      let(:sender_description) { 'a custom description for sender' }

      it 'returns the related data' do
        expect(described_class.new(fund: sender_fund).present).to eq(
          [{
            timestamp: transfer.created_at.to_i,
            date: transfer.created_at.strftime('%e %b %Y'),
            description: 'a custom description for sender',
            plus_or_minus: '-',
            amount: 20.0,
            new_balance: 40.0
          }]
        )
      end
    end
  end
end
