# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReceivedMoneyPresenter do
  describe '#present' do
    let(:recipient) { create(:user, first_name: 'John', last_name: 'Doe') }
    let(:recipient_fund) { create(:fund, user: recipient, balance: 50.0) }
    let(:recipient_description) { nil }
    let(:interac) { nil }
    let!(:transfer) do
      create(
        :transfer,
        sender: sender_fund,
        recipient: recipient_fund,
        sender_new_balance: 40.0,
        recipient_new_balance: 70.0,
        recipient_description: recipient_description,
        interac: interac,
        amount: 20.0
      )
    end

    context 'when there is a sender' do
      let(:sender) { create(:user, first_name: 'Sarah', last_name: 'Smith') }
      let(:sender_fund) { create(:fund, balance: 60.0, user: sender, name: sender.full_name) }

      it 'returns the related data' do
        expect(described_class.new(fund: recipient_fund).present).to eq(
          [{
            timestamp: transfer.created_at.to_i,
            date: transfer.created_at.strftime('%e %b %Y'),
            description: 'You received money from Sarah Smith.',
            plus_or_minus: '+',
            amount: 20.0,
            new_balance: 70.0
          }]
        )
      end

      context 'when a description was saved' do
        let(:recipient_description) { 'a custom description' }

        it 'returns the related data' do
          expect(described_class.new(fund: recipient_fund).present).to eq(
            [{
              timestamp: transfer.created_at.to_i,
              date: transfer.created_at.strftime('%e %b %Y'),
              description: 'a custom description',
              plus_or_minus: '+',
              amount: 20.0,
              new_balance: 70.0
            }]
          )
        end
      end
    end

    context 'when there is no sender (fund refill by admin)' do
      let(:sender_fund) { nil }
      let(:recipient_description) { 'You added money.' }

      subject(:recipient_presenter) { described_class.new(fund: recipient_fund).present }

      it 'returns the related data' do
        expect(recipient_presenter).to eq(
          [{
            timestamp: transfer.created_at.to_i,
            date: transfer.created_at.strftime('%e %b %Y'),
            description: 'You added money.',
            plus_or_minus: '+',
            amount: 20.0,
            new_balance: 70.0
          }]
        )
      end
    end
  end
end
