# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarsHelper, type: :helper do
  describe '#meal_date_and_organizer_for' do
    it 'shows the right message' do
      user = build(:user, first_name: 'John', last_name: 'Doe')
      meal = build(:meal, meal_date: Date.new(2024, 2, 16), user: user)

      expected_message = 'Friday February 16 - organized by John Doe'

      expect(helper.meal_date_and_organizer_for(meal)).to eq(expected_message)
    end
  end
end
