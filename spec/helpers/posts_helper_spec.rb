# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostsHelper, type: :helper do
  describe '#col_classes_for_show_post' do
    let(:post) { build(:post, photos: photos) }

    subject(:css_classes) { helper.col_classes_for_show_post(post) }

    context 'when photos have been uploaded to the post' do
      let(:photos) { [double, double] }

      it 'returns the expected classes' do
        expect(css_classes).to eq('col-12 col-md-6')
      end
    end

    context 'when no photo has been uploaded to the post' do
      let(:photos) { [] }

      it 'returns the expected classes' do
        expect(css_classes).to eq('col-12 col-md-9')
      end
    end
  end

  describe '#result_summary' do
    let_it_be(:user) { create(:user) }
    let_it_be(:category1) { create(:item_category, name: 'category 1') }
    let_it_be(:category2) { create(:item_category, name: 'category 2') }
    let_it_be(:category3) { create(:item_category, name: 'category 3') }
    let_it_be(:post1a) { create(:post, title: 'a title with Search keyword', item_category: category1, user: user) }
    let_it_be(:post1b) { create(:post, item_category: category1, user: user) }
    let_it_be(:post1c) { create(:post, item_category: category1, user: user) }
    let_it_be(:post2a) do
      create(:post,
             description: 'a description with search Keyword.',
             item_category: category2,
             user: user)
    end

    let(:page) { nil }
    let(:category_id) { nil }
    let(:search) { nil }
    let(:params) do
      ActionController::Parameters.new(
        page: page,
        category: category_id,
        search: search
      )
    end

    before do
      allow(PostSearchService).to receive(:number_of_results_per_page).and_return(3)
      allow(helper).to receive(:params).and_return(params)
      search_results = PostSearchService.new(params).execute
      assign(:posts, search_results[:posts])
      assign(:total_count, search_results[:total_count])
    end

    subject(:summary) { helper.result_summary }

    context 'when there is no param' do
      it 'returns the expected summary' do
        expect(summary).to eq('Showing 1 - 3 from 4 items.')
      end
    end

    context 'when there is a page param' do
      let(:page) { 2 }

      it 'returns the expected summary' do
        expect(summary).to eq('Showing 4 - 4 from 4 items.')
      end
    end

    context 'when doing a keyword search only' do
      context 'when the searched expression exists' do
        let(:search) { 'search keyword' }

        it 'returns the expeted summary' do
          expect(summary).to eq('Showing 1 - 2 from 2 items with keyword "search keyword".')
        end
      end

      context 'when the searched expression does not exist' do
        let(:search) { 'abcd' }

        it 'returns the expeted summary' do
          expect(summary).to eq('Sorry, there is no post matching your request.')
        end
      end
    end

    context 'when a category is selected' do
      let(:category_id) { category1.id }

      it 'returns the expected summary' do
        expect(summary).to eq('Showing 1 - 3 from 3 category 1 items.')
      end

      context 'and a search keyword is entered' do
        let(:search) { 'search keyword' }

        it 'returns the expected summary' do
          expect(summary).to eq('Showing 1 - 1 from 1 category 1 item with keyword "search keyword".')
        end
      end
    end
  end

  describe '#contact_summary_html_for' do
    let(:user) { build(:user) }

    subject(:contact_summary) { helper.contact_summary_html_for(user) }

    context 'when the user does not have any phone number' do
      it 'shows the expected summary html' do
        expected_message = "Contact #{user.first_name} at <a href='mailto:#{user.email}'>#{user.email}</a>."

        expect(contact_summary).to eq(expected_message)
      end
    end

    context 'when the user has a phone number' do
      let(:user) { build(:user, phone_number: '902-111-2222') }

      it 'shows the expected summary html' do
        expected_message =
          "Contact #{user.first_name} at <a href='mailto:#{user.email}'>#{user.email}</a> " \
          "or call <a href='tel:9021112222'>902-111-2222</a>."

        expect(contact_summary).to eq(expected_message)
      end
    end
  end

  describe '#price_options' do
    it 'returns expected array' do
      expect(helper.price_options.map(&:to_h)).to eq(
        [
          { id: 0, name: 'default', label: 'Default price' },
          { id: 1, name: 'per_item', label: 'Price per item' },
          { id: 2, name: 'see_description', label: 'Price(s) in description' },
          { id: 3, name: 'contact_seller', label: 'Contact seller' },
          { id: 4, name: 'barter', label: 'Let\'s negociate!' },
          { id: 5, name: 'best_offer', label: 'Best offer' },
          { id: 6, name: 'free', label: 'Free!' },
          { id: 7, name: 'borrow', label: 'Borrow' }
        ]
      )
    end
  end

  describe '#price_option_label_for' do
    using RSpec::Parameterized::TableSyntax

    subject(:label_for_post) { helper.price_option_label_for(post) }

    where(:price_option, :label) do
      0 | '$6.50'
      1 | '$6.50 each'
      2 | 'Price(s) in description'
      3 | 'Contact seller'
      4 | 'Let\'s negociate!'
      5 | 'Best offer'
      6 | 'Free!'
    end

    with_them do
      let(:post) { build(:post, price: 6.50, price_option: price_option) }

      it 'returns the correct label' do
        expect(label_for_post).to eq(label)
      end
    end
  end
end
