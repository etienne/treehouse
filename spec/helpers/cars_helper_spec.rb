# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarsHelper, type: :helper do
  let_it_be(:user1) { create(:user) }
  let_it_be(:car) { create(:car) }
  let_it_be(:daytime) { DateTime.new(2024, 2, 11, 12, 0, 0, '-0400') }

  include ActiveSupport::Testing::TimeHelpers

  describe '#last_booking_summary_for' do
    subject(:summary) { helper.last_booking_summary_for(car) }

    context 'when there was no prior booking' do
      it 'returns the related summary' do
        expect(summary).to eq('This car has not been booked yet.')
      end
    end

    context 'when there were a few past bookings' do
      it 'returns the related summary' do
        create(:car_booking, user: user1, start_time: daytime - 5.days)
        create(:car_booking, user: user1, start_time: daytime - 2.days)
        create(:car_booking, user: user1, start_time: daytime - 9.days)

        expect(summary).to eq("Last booking: Fri Feb 9, from 12:00 PM to 3:00 PM, by #{user1.full_name}.")
      end
    end
  end

  describe '#upcoming_booking_summary_for' do
    subject(:summary) { helper.upcoming_booking_summary_for(car) }

    context 'when there is no future booking' do
      it 'returns the related summary' do
        expect(summary).to eq('No upcoming booking yet.')
      end
    end

    context 'when there are a few upcoming bookings' do
      it 'returns the related summary' do
        travel_to daytime do
          create(:car_booking, user: user1, start_time: daytime + 5.days)
          create(:car_booking, user: user1, start_time: daytime + 2.days)
          create(:car_booking, user: user1, start_time: daytime + 9.days)

          expect(summary).to eq("Next booking: Tue Feb 13, from 12:00 PM to 3:00 PM, by #{user1.full_name}.")
        end
      end
    end
  end
end
