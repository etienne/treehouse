# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Bookings::GuestsHelper, type: :helper do
  describe '#booking_credit_left_for' do
    let(:year) { 2022 }
    let(:credits) do
      h = {}
      h[2022] = num_of_booked_nights
      h
    end

    subject(:credit_message) { helper.booking_credit_left_for(credits, year) }

    context 'when no nights have been booked for given year' do
      let(:num_of_booked_nights) { 0 }

      it 'returns the correct message' do
        expect(credit_message).to eq('2022: 10 free nights left')
      end
    end

    context 'when a few nights have been booked for given year' do
      let(:num_of_booked_nights) { 4 }

      it 'returns the correct message' do
        expect(credit_message).to eq('2022: 6 free nights left')
      end
    end

    context 'when all free nights have been booked for given year' do
      let(:num_of_booked_nights) { 10 }

      it 'returns the correct message' do
        expect(credit_message).to eq('2022: $15 per night')
      end
    end
  end

  describe '#days_since_confirmation_email_for' do
    subject(:days) { helper.days_since_confirmation_email_for(booking) }

    context 'when no booking is passed' do
      let(:booking) { nil }

      it 'returns 0' do
        expect(days).to eq(0)
      end
    end

    context 'when the booking is already confirmed' do
      let(:booking) { build(:room_booking, confirmed_at: Date.current) }

      it 'returns 0' do
        expect(days).to eq(0)
      end
    end

    context 'when we are before the sending of the confirmation email' do
      let(:start_date) { Date.current + 6.months }
      let(:booking) { build(:room_booking, start_date: start_date, end_date: start_date + 4.days) }

      it 'returns 0' do
        expect(days).to eq(0)
      end
    end

    context 'when we are after the sending of the confirmation email' do
      let(:start_date) { Date.current + 5.days }
      let(:booking) { build(:room_booking, start_date: start_date, end_date: start_date + 4.days) }

      it 'returns the expected number of days' do
        expected_num = (
          Date.current - (booking.start_date - RoomBooking::CONFIRMATION_BEFORE_START_MONTHS.months).to_date
        )

        expect(days).to eq(expected_num)
      end
    end
  end
end
