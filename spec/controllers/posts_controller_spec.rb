# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostsController do
  describe 'authenticated user' do
    let(:user) { create(:user) }

    before do
      sign_in user
    end

    it 'shows the expected view' do
      get :index

      expect(response).to have_http_status(:success)
    end

    context 'destroy' do
      let(:post) { create(:post, user: user) }

      subject(:destroy) { delete :destroy, params: { id: post.id } }

      context 'when the current user is the writer of the post' do
        it 'is successful' do
          destroy

          expect(controller).to set_flash[:notice].to('Your post was successfully deleted.')
          expect(response).to redirect_to(posts_url)
        end
      end

      context 'when the current user is not the writer of the post' do
        let(:another_user) { create(:user) }

        before do
          allow(controller).to receive(:current_user).and_return(another_user)
        end

        it 'redirects to the post path with an alert message' do
          destroy

          expect(controller).to set_flash[:error].to('You cannot edit or delete this post.')
          expect(response).to redirect_to(post_path(post))
        end
      end
    end

    context 'edit page' do
      let(:post) { create(:post, user: user) }

      subject(:edit) { get :edit, params: { id: post.id } }

      context 'when the current user is the writer of the post' do
        it 'shows the edit page' do
          edit

          expect(response).to have_http_status(:success)
        end
      end

      context 'when the current user is not the writer of the post' do
        let(:another_user) { create(:user) }

        before do
          allow(controller).to receive(:current_user).and_return(another_user)
        end

        it 'redirects to the post path with an alert message' do
          edit

          expect(controller).to set_flash[:error].to('You cannot edit or delete this post.')
          expect(response).to redirect_to(post_path(post))
        end
      end
    end

    describe 'GET #new' do
      it 'provides a Post object with price being zero' do
        expect(Post).to receive(:new).with(price: 0)

        get :new
      end
    end

    describe 'POST #create' do
      it 'creates a new post with the current_user as user' do
        ic = create(:item_category)

        params = {
          post: {
            title: 'title',
            description: 'description',
            price: '12.0',
            item_category_id: ic.id
          }
        }

        expect { post :create, params: params }.to change { Post.count }.by(1)
        expect(Post.last.user_id).to eq(user.id)
      end
    end
  end
end
