# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Bookings::CarsController do
  let(:user) { create(:user) }

  before do
    sign_in user
  end

  describe 'GET #index' do
    context 'when the user is allowed to access the car booking room page' do
      it 'shows the expected view' do
        get :index

        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'POST #create' do
    it 'creates a new car booking with the current_user as user' do
      car = create(:car)

      params = {
        car_booking: {
          start_time: DateTime.now + 2.hours,
          end_time: DateTime.now + 4.hours,
          car_id: car.id,
          expected_distance: 10
        }
      }

      expect { post :create, params: params }.to change { CarBooking.count }.by(1)
      expect(CarBooking.last.user_id).to eq(user.id)
    end
  end
end
