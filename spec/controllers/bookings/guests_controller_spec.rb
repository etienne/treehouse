# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Bookings::GuestsController do
  let_it_be(:user) { create(:user) }
  let(:booking) { create(:room_booking, user: user) }

  before do
    sign_in user
  end

  describe 'GET #index' do
    it 'shows the expected view' do
      get :index

      expect(response).to have_http_status(:success)
    end

    context 'destroy' do
      subject(:destroy) { delete :destroy, params: { id: booking.id } }

      context 'when the current user is the host of the booking' do
        it 'deletes successfully' do
          start_date = booking.start_date.to_date
          end_date = booking.end_date.to_date
          message = "Your booking from #{start_date} to #{end_date} has now been cancelled."

          destroy

          expect(controller).to set_flash[:success].to(message)
          expect(response).to redirect_to(bookings_guests_path)
        end
      end

      context 'when the current user is a guest room cordinator' do
        let(:coordinator) { create(:user, :guest_room_coordinator) }

        before do
          allow(controller).to receive(:current_user).and_return(coordinator)
        end
        it 'deletes successfully' do
          start_date = booking.start_date.to_date
          end_date = booking.end_date.to_date
          message = "The booking from #{start_date} to #{end_date} has now been cancelled."

          destroy

          expect(controller).to set_flash[:success].to(message)
          expect(response).to redirect_to(admin_guest_rooms_path)
        end
      end

      context 'when the current user is not the host of the bookings' do
        let(:another_user) { create(:user) }

        before do
          allow(controller).to receive(:current_user).and_return(another_user)
        end

        it 'redirects to the booking path with an alert message' do
          destroy

          expect(controller).to set_flash[:error].to('You cannot edit or delete this booking.')
          expect(response).to redirect_to(bookings_guest_path(booking))
        end
      end
    end

    context 'edit page' do
      subject(:edit) { get :edit, params: { id: booking.id } }

      context 'when the current user is the host of the booking' do
        it 'shows the edit page' do
          edit

          expect(response).to have_http_status(:success)
        end
      end

      context 'when the current user is not the host of the booking' do
        let(:another_user) { create(:user) }

        before do
          allow(controller).to receive(:current_user).and_return(another_user)
        end

        it 'redirects to the booking guest path with an alert message' do
          edit

          expect(controller).to set_flash[:error].to('You cannot edit or delete this booking.')
          expect(response).to redirect_to(bookings_guest_path(booking))
        end
      end
    end
  end

  describe '#confirm_booking' do
    subject(:confirm_booking) { get :confirm_booking, params: { id: booking.id } }

    let(:booking) { create(:room_booking, state: 'pending') }

    context 'when the current user is the host of the booking' do
      it 'confirms the pending booking and shows the correct flash message' do
        allow(controller).to receive(:current_user).and_return(booking.user)

        confirm_booking

        expect(response).to redirect_to(bookings_guests_path)
        expect(controller).to set_flash[:notice].to('Your guest room booking is now confirmed.')
        expect(booking.reload.state).to eq('confirmed')
      end
    end

    context 'when the current user is not the host of the booking' do
      it 'shows an error message' do
        allow(controller).to receive(:current_user).and_return(build(:user))

        confirm_booking

        expect(controller).to set_flash[:error].to('You cannot edit or delete this booking.')
        expect(response).to redirect_to(bookings_guest_path(booking))
        expect(booking.reload.state).to eq('pending')
      end
    end
  end

  describe '#fetch_booking' do
    subject(:fetch_bookings) { get :fetch_bookings, params: params }

    context 'when no date is passed' do
      let(:params) { {} }

      it 'returns an empty array' do
        fetch_bookings

        expect(response.body).to eq({ bookings: [] }.to_json)
      end
    end

    context 'when invalid dates are passed' do
      let(:params) { { start_date: 'fake', end_date: 'bad' } }

      it 'returns an empty array' do
        fetch_bookings

        expect(response.body).to eq({ bookings: [] }.to_json)
      end
    end

    context 'when valid dates are passed' do
      using RSpec::Parameterized::TableSyntax

      let_it_be(:today) { Date.current }
      let_it_be(:booking1) { create(:room_booking, user: user, start_date: today + 4.days, end_date: today + 6.days) }
      let_it_be(:booking2) { create(:room_booking, user: user, start_date: today + 10.days, end_date: today + 15.days) }
      let_it_be(:booking3) { create(:room_booking, user: user, start_date: today + 20.days, end_date: today + 28.days) }

      where(:start_date, :end_date, :expected_bookings) do
        Date.current             | (Date.current + 5.days)  | [ref(:booking1)]
        (Date.current + 5.days)  | (Date.current + 17.days) | [ref(:booking1), ref(:booking2)]
        (Date.current + 8.days)  | (Date.current + 25.days) | [ref(:booking2), ref(:booking3)]
        (Date.current + 30.days) | (Date.current + 32.days) | []
      end

      with_them do
        let(:params) { { start_date: start_date, end_date: end_date } }

        it 'returns the expected booking details' do
          fetch_bookings

          expected_payload = { bookings: expected_bookings.map{|b| b.serialize_for_calendar_for(user)} }.to_json

          expect(response.body).to eq(expected_payload)
        end
      end
    end
  end
end
