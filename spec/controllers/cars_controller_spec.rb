# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarsController do
  let(:user) { create(:user) }
  let(:car) { create(:car) }

  describe 'authenticated user' do
    context 'when the user is signed-in' do
      it 'shows the expected view' do
        sign_in user

        get :index

        expect(response).to have_http_status(:success)
      end
    end

    context 'when the user is not signed-in' do
      it 'redirects to the login page' do
        get :index

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'actions' do
    before do
      sign_in user
    end

    context 'destroy' do
      subject(:destroy) { delete :destroy, params: { id: car.id } }

      context 'when the current user is a car admin' do
        it 'is successful' do
          allow(controller.current_user).to receive(:car_admin?).and_return(true)

          destroy

          expect(controller).to set_flash[:notice].to('This car was successfully removed, and related car bookings were also deleted.')
          expect(response).to redirect_to(cars_url)
        end
      end

      context 'when the current user is not a car admin' do
        it 'redirects to the post path with an alert message' do
          destroy

          expect(controller).to set_flash[:error].to('You cannot add a car or edit an existing one.')
          expect(response).to redirect_to(cars_path)
        end
      end
    end

    context 'edit page' do
      subject(:edit) { get :edit, params: { id: car.id } }

      context 'when the current user is the writer of the post' do
        it 'shows the edit page' do
          allow(controller.current_user).to receive(:car_admin?).and_return(true)

          edit

          expect(response).to have_http_status(:success)
        end
      end

      context 'when the current user is not the writer of the post' do
        it 'redirects to the post path with an alert message' do
          edit

          expect(controller).to set_flash[:error].to('You cannot add a car or edit an existing one.')
          expect(response).to redirect_to(cars_path)
        end
      end
    end
  end
end
