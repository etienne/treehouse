# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Fund, type: :model do
  describe 'associations' do
    it { should belong_to(:user).optional }
    it { should have_many(:sent_transfers) }
    it { should have_many(:received_transfers) }
  end

  describe 'validations' do
    it { should validate_presence_of(:balance) }
  end

  describe 'scopes' do
    let_it_be(:user_one) { create(:user) }
    let_it_be(:fund_one) { create(:fund, :community, user: user_one) }
    let_it_be(:fund_two) { create(:fund, :community, user: nil) }
    let_it_be(:user_two) { create(:user) }

    describe '.community' do
      it 'returns the relevant objects' do
        expect(described_class.community).to contain_exactly(fund_one, fund_two)
      end
    end

    describe '.personal' do
      it 'returns the relevant objects' do
        expect(described_class.personal).to contain_exactly(user_one.fund, user_two.fund)
      end
    end
  end

  describe '#community?' do
    subject(:community_fund?) { fund.community? }

    context 'when the fund is marked as community' do
      let(:fund) { build(:fund, :community) }

      it { is_expected.to be_truthy }
    end

    context 'when the fund is not marked as community' do
      let(:fund) { build(:fund) }

      it { is_expected.to be_falsey }
    end
  end

end
