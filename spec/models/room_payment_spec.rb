# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RoomPayment, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:room_booking) }
  end

  describe 'validations' do
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:room_booking) }
    it { should validate_presence_of(:amount) }
  end

  it { should define_enum_for(:state).with_values(%i[pending paid cancelled]) }

  describe 'pay_now!' do
    it 'changes the state from pending to paid' do
      payment = create(:room_payment, :pending)

      expect(payment).to be_pending

      payment.pay_now!

      expect(payment).to be_paid
    end
  end
end
