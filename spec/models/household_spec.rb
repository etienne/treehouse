# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Household, type: :model do
  describe 'associations' do
    it { should have_many(:users) }
    it { should have_many(:kids) }
    it { should have_many(:eligibilities) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end
end
