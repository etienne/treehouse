# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Meal, type: :model do
  describe 'associations' do
    it { should have_many(:meal_portions) }
    it { should have_many(:meal_payments) }
    it { should have_many(:meal_payment_attendees) }
    it { should have_many(:meal_seatings) }
    it { should have_many(:meal_team_members) }
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:meal_date) }
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:user) }
  end

  describe '#attendence_for' do
    let!(:meal) { create(:meal) }
    let!(:user) { create(:user) }

    subject(:already_attending?) { meal.attendence_for?(user) }

    context 'when a user has not registered for a meal' do
      it { is_expected.to be_falsey }
    end

    context 'when a user has registered for a meal' do
      before do
        create(:meal_payment, meal: meal, user: user)
      end

      it { is_expected.to be_truthy }

      context 'when the passed user is someone else from the same household' do
        it 'returns true for this user' do
          other_user = create(:user)
          create(:household, users: [user, other_user])

          expect(meal.attendence_for?(other_user)).to be_truthy
        end
      end
    end
  end

  describe '#community_fund_contribution' do
    context 'when the cost has not been specified in the meal' do
      let(:meal) { create(:meal, cost: nil) }

      it 'returns nil' do
        expect(meal.community_fund_contribution).to be_nil
      end
    end

    context 'when the cost has been specified in the meal' do
      let!(:meal) { create(:meal, cost: 60.0) }

      it 'returns the relevant amount' do
        create(:meal_payment, meal: meal, amount: 20.0)

        expect(meal.community_fund_contribution).to eq(40.0)

        create(:meal_payment, meal: meal, amount: 50.0)

        expect(meal.reload.community_fund_contribution).to eq(-10.0)
      end
    end
  end
end
