# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarBooking, type: :model do
  include ActiveSupport::Testing::TimeHelpers

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:car) }
  end

  describe 'callbacks' do
    describe 'check_before_update' do
      it 'cannot be updated after booking ended' do
        b = create(:car_booking)
        end_time = b.end_time

        travel_to 1.year.from_now do
          b.end_time = end_time + 1.hour
          b.save

          expect(b).not_to be_valid
          expect(b.errors.full_messages.first).to eq(
            'This booking cannot end before now.'
          )
          expect(CarBooking.last.end_time).to eq(end_time)
        end
      end
    end

    # describe 'check_before_destroy' do
    #   it 'cannot be destroyed after booking started' do
    #     b = create(:car_booking)
    #
    #     travel_to b.start_time + 1.minute do
    #       b.destroy
    #
    #       expect(b.reload).to be_a(CarBooking)
    #     end
    #   end
    # end
  end

  it do
    should define_enum_for(:state)
      .with_values(%i[scheduled verified closed])
  end

  describe '#serialize_for_calendar_for' do
    let_it_be(:user) { create(:user) }

    let(:booking) { create(:car_booking, user: user, car: car) }
    let(:car) { create(:car, calendar_color: '#123456', electric: electric) }
    let(:starttime) { Time.parse(booking.start_time.to_s).strftime('%l:%M %p').strip }
    let(:endtime) { Time.parse(booking.end_time.to_s).strftime('%l:%M %p').strip }
    let(:the_date) { Date.parse(booking.start_time.to_s).strftime('%a %b %d') }
    let(:expected_booking) do
      {
        carname: car.title,
        title: "#{user.household_mention} 💬",
        start: booking.start_time,
        end: booking.end_time,
        period: "#{the_date}, from #{starttime} to #{endtime}",
        booking_id: booking.id,
        viewcartitle: "Car booked by #{user.household_mention}",
        expected: expected_distance,
        color: '#123456',
        note: booking.note,
        eventtype: 'booking',
        show_link: Rails.application.routes.url_helpers.bookings_car_url(booking)
      }
    end

    context 'when the car is not electric' do
      let(:electric) { false }
      let(:expected_distance) { 'N/A' }

      it 'returns the expected data' do
        expect(booking.serialize_for_calendar_for(user)).to eq(
          expected_booking.merge({ charging: 'N/A', electric: false })
        )
      end
    end

    context 'when the car is electric' do
      let(:electric) { true }
      let(:expected_distance) { "#{booking.expected_distance} kms" }

      it 'returns the expected data' do
        expect(booking.serialize_for_calendar_for(user)).to eq(
          expected_booking.merge({ charging: '30 mins ', electric: true })
        )
      end
    end
  end

  describe '#booking_start_time' do
    it 'returns the booking start time' do
      booking = build(:car_booking)

      expect(booking.booking_start_time).to eq(booking.start_time)
    end
  end

  describe '#booking_end_time' do
    it 'returns the booking end time' do
      booking = build(:car_booking)

      expect(booking.booking_end_time).to eq(booking.end_time)
    end
  end

  describe '#booking_color_to_use' do
    it 'returns the color to use for the calendar' do
      booking = build(:car_booking)

      expect(booking.booking_color_to_use).to eq(booking.car.calendar_color)
    end
  end

  describe '#day_time_summary' do
    it 'returns the relevant string' do
      booking = build(
        :car_booking,
        start_time: DateTime.new(2023, 6, 20, 9, 30, 0, '-0300'),
        end_time: DateTime.new(2023, 6, 20, 14, 0, 0, '-0300')
      )

      expect(booking.day_time_summary).to eq(
        'Tue Jun 20, from 9:30 AM to 2:00 PM'
      )
    end
  end

  describe '.booking_summary_for' do
    let(:monday) { Date.new(2024, 7, 15) }

    it 'returns the relevant data' do
      travel_to monday do
        user = create(:user)
        tue = monday + 1.day
        wed = monday + 2.days
        thu = monday + 3.days
        next_tue = monday + 8.days

        create(:car_booking, user: user, start_time: monday, end_time: monday + 3.hours)
        create(:car_booking, user: user, start_time: tue, end_time: tue + 3.hours)
        create(:car_booking, user: user, start_time: tue + 5.hours, end_time: tue + 11.hours)
        create(:car_booking, user: user, start_time: wed + 5.hours, end_time: wed + 11.hours)
        create(:car_booking, user: user, start_time: thu + 5.hours, end_time: thu + 7.hours)
        create(:car_booking, user: user, start_time: next_tue, end_time: next_tue + 2.hours)

        expected_data = [
          { date: monday, usage_time: 3.0, cost: 24.0 },
          { date: tue, usage_time: 9.0, cost: 40.0 },
          { date: wed, usage_time: 6.0, cost: 40.0 },
          { date: thu, usage_time: 2.0, cost: 16.0 }
        ]

        expect(
          described_class.booking_summary_for(
            user: user,
            period_start: monday,
            period_end: monday.end_of_week
          )
        ).to eq(expected_data)
      end
    end
  end

  describe '.max_number_daily_billable_hours' do
    it 'returns the correct number' do
      expect(described_class.max_number_daily_billable_hours).to eq(5.0)
    end
  end

  describe '.hourly_rate' do
    it 'returns the correct number' do
      expect(described_class.hourly_rate).to eq(8.0)
    end
  end

  describe '#diff_in_hours' do
    it 'returns the expected difference in hours', :time_freeze do
      now = DateTime.now
      booking1 = build(:car_booking, start_time: now + 2.hours, end_time: now + 4.hours)
      booking2 = build(:car_booking, start_time: now + 2.hours, end_time: now + 5.hours + 30.minutes)

      expect(booking1.diff_in_hours).to eq(2.0)
      expect(booking2.diff_in_hours).to eq(3.5)
    end
  end

  describe '#cost' do
    it 'returns the expected cost', :time_freeze do
      now = DateTime.now
      booking1 = build(:car_booking, start_time: now + 2.hours, end_time: now + 4.hours)
      booking2 = build(:car_booking, start_time: now + 2.hours, end_time: now + 5.hours + 30.minutes)
      booking3 = build(:car_booking, start_time: now + 1.hours, end_time: now + 9.hours)

      expect(booking1.cost).to eq(16.0)
      expect(booking2.cost).to eq(28.0)
      expect(booking3.cost).to eq(40.0)
    end
  end
end
