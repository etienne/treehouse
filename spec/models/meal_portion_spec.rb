# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MealPortion, type: :model do
  describe 'associations' do
    it { should belong_to(:meal) }
    it { should have_many(:meal_portion_choices) }
  end
end
