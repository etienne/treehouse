# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Transfer, type: :model do
  describe 'associations' do
    it { should belong_to(:sender).optional }
    it { should belong_to(:recipient).optional }
  end

  describe 'validations' do
    it { should validate_length_of(:interac).is_at_least(6).with_message('number must be between 6 and 20 characters.') }
    it { should validate_length_of(:interac).is_at_most(20).with_message('number must be between 6 and 20 characters.') }
  end

  describe '#pay_now!' do
    subject(:pay_transfer_now!) { transfer.pay_now! }

    context 'when a transfer has both a recipient and a sender' do
      let!(:transfer) { build(:transfer, recipient: recipient, sender: sender, amount: 20.0) }
      let!(:sender) { create(:fund, balance: 30.0) }
      let!(:recipient) { create(:fund, balance: 60.0) }

      it 'moves money from one fund to another accordingly' do
        expect(sender.balance).to eq(30.0)
        expect(recipient.balance).to eq(60.0)

        pay_transfer_now!

        expect(sender.reload.balance).to eq(10.0)
        expect(recipient.reload.balance).to eq(80.0)
      end

      it 'sets the new balances for both funds on the transfer' do
        pay_transfer_now!

        expect(transfer.reload.sender_new_balance).to eq(10.0)
        expect(transfer.reload.recipient_new_balance).to eq(80.0)
      end

      it 'marks the transfer as paid' do
        expect(transfer).to be_pending

        pay_transfer_now!

        expect(transfer).to be_paid
      end

      context 'when the recipient and the sender are the same' do
        let(:sender) { recipient }

        it 'raises an error' do
          expect(transfer).not_to be_valid
          expect(transfer.errors.full_messages.first).to eq('The sender and recipient cannot be the same fund.')
        end
      end

      context 'when the sender does not have enough fund for the transfer' do
        let!(:sender) { create(:fund, balance: 5.0) }

        it 'raises an exception and does not save anything' do
          expect { pay_transfer_now! }.to raise_error(described_class::SenderNotEnoughMoneyError)

          expect(sender.reload.balance).to eq(5.0)
          expect(recipient.reload.balance).to eq(60.0)
          expect(transfer.persisted?).to be_falsey
        end
      end
    end

    context 'when a transfer only has a recipient' do
      let!(:transfer) { create(:transfer, :no_sender, recipient: recipient, amount: 20.0) }
      let!(:recipient) { create(:fund, balance: 90.0) }

      it 'changes the recipient balance' do
        expect(recipient.balance).to eq(90.0)

        pay_transfer_now!

        expect(transfer.sender).to be_nil
        expect(recipient.reload.balance).to eq(110.0)
      end

      it 'sets the new balance on the transfer' do
        pay_transfer_now!

        expect(transfer.reload.sender_new_balance).to be_nil
        expect(transfer.reload.recipient_new_balance).to eq(110.0)
      end
    end

    context 'when a transfer only has a sender' do
      let!(:transfer) { create(:transfer, :no_recipient, sender: sender, amount: 20.0) }
      let!(:sender) { create(:fund, balance: 90.0) }

      it 'changes the sender balance' do
        expect(sender.balance).to eq(90.0)

        pay_transfer_now!

        expect(transfer.recipient).to be_nil
        expect(sender.reload.balance).to eq(70.0)
      end

      it 'sets the new balance on the transfer' do
        pay_transfer_now!

        expect(transfer.reload.sender_new_balance).to eq(70.0)
        expect(transfer.reload.recipient_new_balance).to be_nil
      end
    end
  end

  describe '#masked_interac' do
    it 'returns the expected masked value' do
      transfer = build(:transfer, sender: build(:fund), recipient: build(:fund), interac: 'aabbccddee')

      expect(transfer.masked_interac).to eq('******ddee')
    end
  end
end
