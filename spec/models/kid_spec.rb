# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Kid, type: :model do
  describe 'associations' do
    it { should belong_to(:household) }
    it { should have_many(:meal_payment_attendees) }
  end

  describe 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
  end
end
