# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarBookingPayment, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:period_start) }
    it { should validate_presence_of(:period_end) }
    it { should validate_presence_of(:amount) }
    it { should validate_presence_of(:state) }
  end

  describe 'associations' do
    it { should belong_to(:user) }
  end

  describe 'callbacks' do
    let(:user) { create(:user) }
    let(:amount) { 10.0 }
    let(:period_start) { Date.new(2024, 7, 15) }
    let(:period_end) { Date.new(2024, 7, 21) }

    subject(:payment) do
      create(:car_booking_payment,
             amount: amount,
             user: user,
             period_start: period_start,
             period_end: period_end)
    end

    before do
      user.fund.update(balance: balance)
    end

    context 'when the renter has enough money in their fund' do
      let(:balance) { 60.0 }

      it 'creates a new transfer' do
        expect(MoneyTransferService).to receive(:new).with(
          sender_fund: user.fund,
          recipient_fund: Fund.car_rentals,
          amount: 10.0,
          sender_description: 'Payment for car sharing (2024-07-15 - 2024-07-21)',
          recipient_description: 'Car sharing payment (2024-07-15 - 2024-07-21)'
        ).and_call_original

        payment
      end

      it 'marks the car booking payment as paid' do
        payment

        expect(payment.reload).to be_paid
      end
    end

    context 'when the renter does not have enough money in their fund' do
      let(:balance) { 2.0 }

      it 'does not create a new transfer' do
        expect(MoneyTransferService).not_to receive(:new)

        payment
      end

      it 'marks the car booking payment as pending' do
        payment

        expect(payment.reload).to be_pending
      end
    end
  end
end
