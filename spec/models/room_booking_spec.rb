# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RoomBooking, type: :model do
  let_it_be(:today) { Date.current }
  let_it_be(:user) { create(:user) }
  let_it_be(:room1) { create(:room, name: 'Room 1') }
  let_it_be(:room2) { create(:room, name: 'Room 2') }
  let(:start_date) { today + 2.months }
  let(:end_date) { start_date + 1.week }
  let(:future_booking) do
    create(:room_booking,
           user: user,
           start_date: start_date,
           end_date: end_date,
           room: room2)
  end

  it { should define_enum_for(:state).with_values(%i[initial confirmed pending payment_required]) }

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:room) }
    it { should have_one(:room_payment) }
  end

  describe 'validations' do
    it { should validate_presence_of(:start_date) }
    it { should validate_presence_of(:end_date) }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:room_id) }

    describe 'check_date_overlap' do
      using RSpec::Parameterized::TableSyntax

      let(:booking) do
        build(:room_booking,
              user: user,
              room: room1,
              start_date: today + 5.days,
              end_date: today + 10.days)
      end

      # rubocop:disable Lint/AmbiguousOperatorPrecedence
      where(:start_date, :end_date, :overlap) do
        Date.current           | Date.current + 2.days   | false
        Date.current + 2.days  | Date.current + 10.days  | true
        Date.current           | Date.current + 5.days   | false # allowing check-out then check-in on same day.
        Date.current + 5.days  | Date.current + 8.days   | true
        Date.current + 8.days  | Date.current + 15.days  | true
        Date.current + 30.days | Date.current + 33.days  | false # allowing check-out then check-in on same day.
      end
      # rubocop:enable Lint/AmbiguousOperatorPrecedence

      with_them do
        it 'returns whether a new booking is overlapping with a previous one' do
          create(:room_booking, user: user, room: room1, start_date: start_date, end_date: end_date)

          if overlap
            expect(booking.valid?).to be_falsey
            expect(booking.errors.first.full_message)
              .to eq('This booking is overlapping with another booking ' \
                     'for this same room. Please review the room booking calendar.')
          else
            expect(booking.valid?).to be_truthy
          end
        end
      end
    end

    describe 'start_date_and_end_date' do
      it 'checks if the start date is before today\'s date' do
        booking = build(:room_booking, user: user, room: room1, start_date: Date.current - 5.days)

        expect(booking.valid?).to be_falsey
        expect(booking.errors.first.full_message)
          .to eq('This booking cannot start in the past.')
      end

      it 'checks if the start date is before the end date' do
        booking = build(
          :room_booking,
          user: user,
          room: room1,
          start_date: Date.current + 5.days,
          end_date: Date.current + 2.days
        )

        expect(booking.valid?).to be_falsey
        expect(booking.errors.first.full_message)
          .to eq('This booking cannot end before it starts.')
      end
    end

    describe 'validate_number_of_nights' do
      using RSpec::Parameterized::TableSyntax

      where(:num_days, :valid) do
        4  | true
        10 | true
        11 | false
        45 | false
      end

      with_them do
        it 'checks the max number of nights is not exceeded' do
          start_date = Date.current
          booking = build(:room_booking, start_date: start_date, end_date: start_date + num_days.days)
          expect(booking.valid?).to eq(valid)
          unless booking.valid?
            expect(booking.errors.first.full_message).to eq('Room bookings cannot exceed 10 nights.')
          end
        end
      end
    end
  end

  describe 'scopes' do
    let_it_be(:today_booking) { create(:room_booking, user: user, room: room1) }

    describe '.upcoming' do
      it 'returns the upcoming room booking' do
        current_booking = create(:room_booking, start_date: today, end_date: today + 7.days, user: user)

        expect(described_class.upcoming).to contain_exactly(current_booking, today_booking, future_booking)
      end
    end

    describe '.for_year' do
      it 'returns the relevant room bookings' do
        booking_next_year = create(
          :room_booking,
          start_date: (Date.current + 1.year),
          end_date: (Date.current + 1.year + 2.days)
        )
        create(
          :room_booking,
          start_date: (Date.current + 2.years),
          end_date: (Date.current + 2.years + 2.days)
        )

        expect(described_class.for_year(Date.current.next_year.year))
          .to contain_exactly(booking_next_year)
      end
    end

    describe '.this_year_and_next_year' do
      it 'returns the relevant room bookings' do
        year = Date.current.year
        between_years = create(:room_booking, start_date: Date.new(year, 12, 30), end_date: Date.new(year + 1, 1, 2))
        next_year = create(:room_booking, start_date: Date.current + 1.year, end_date: Date.current + 1.year + 2.days)
        create(:room_booking, start_date: (Date.current + 2.years), end_date: (Date.current + 2.years + 2.days))

        expect(described_class.this_year_and_next_year).to contain_exactly(today_booking, between_years, next_year)
      end
    end

    describe '.close_to_start_date' do
      using RSpec::Parameterized::TableSyntax

      where(:start_date, :close_to_start_date) do
        Date.current             | false
        Date.current + 8.days    | true
        Date.current + 3.months  | true
        Date.current + 110.days  | false
      end

      with_them do
        let(:end_date) { start_date + 9.days }

        it 'returns relevant bookings' do
          if close_to_start_date
            expect(described_class.close_to_start_date).to include(future_booking)
          else
            expect(described_class.close_to_start_date).not_to include(future_booking)
          end
        end
      end
    end

    describe '.with_pending_confirmation' do
      it 'returns the bookings with pending confirmation' do
        future_booking.update(confirmed_at: Date.current)

        expect(described_class.with_pending_confirmation).to contain_exactly(today_booking)
      end
    end

    describe '.around_same_period' do
      it 'returns bookings around a period given a booking dates' do
        near_future_booking = create(:room_booking, start_date: today + 45.days, end_date: today + 50.days, user: user)
        create(:room_booking, start_date: today + 7.months, end_date: today + 7.months + 2.days, user: user)

        expect(described_class.around_same_period(future_booking))
          .to contain_exactly(future_booking, near_future_booking)
      end
    end

    describe '.for_room' do
      it 'returns bookings for a given room' do
        expect(described_class.for_room(room1)).to contain_exactly(today_booking)
      end
    end

    describe '.confirmable_today' do
      it 'returns relevant bookings' do
        create(:room_booking, start_date: (Date.current + 3.months - 1.day))
        booking = create(:room_booking, start_date: (Date.current + 3.months))
        create(:room_booking, start_date: (Date.current + 3.months + 1.day))

        expect(described_class.confirmable_today).to contain_exactly(booking)
      end
    end

    describe '.to_be_reminded_about_today' do
      it 'returns relevant bookings' do
        create(:room_booking, start_date: (Date.current + 1.month - 1.day))
        booking = create(:room_booking, start_date: (Date.current + 1.month))
        create(:room_booking, start_date: (Date.current + 1.month + 1.day))

        expect(described_class.to_be_reminded_about_today).to contain_exactly(booking)
      end
    end
  end

  describe '#serialize_for_calendar_for' do
    context 'when it is for the booking host' do
      it 'returns the expect hash' do
        booking = create(:room_booking, user: user, room: room1)

        expect(booking.serialize_for_calendar_for(user)).to eq(
          title: "#{user.household_mention} 💬",
          start: "#{booking.start_date.to_date} 12:00",
          end: "#{booking.end_date.to_date} 12:00",
          max_nights: 10,
          period: '',
          booking_id: booking.id,
          color: booking.room.calendar_color,
          room: 'guestroomone',
          datesummary: booking.date_summary,
          roomname: 'Room 1',
          notes: booking.description,
          viewroomtitle: "Room booking for #{user.full_name}",
          show_link: Rails.application.routes.url_helpers.bookings_guest_url(booking)
        )
      end
    end

    context 'when it is for another user than the booking host' do
      it 'returns the expect hash' do
        booking = build(:room_booking, user: user, room: room1)
        another_user = create(:user)

        expect(booking.serialize_for_calendar_for(another_user)).to eq(
          title: "#{booking.user.full_name} 💬",
          start: "#{booking.start_date.to_date} 12:00",
          end: "#{booking.end_date.to_date} 12:00",
          max_nights: 10,
          booking_id: booking.id,
          period: '',
          color: booking.room.calendar_color,
          datesummary: booking.date_summary,
          room: 'guestroomone',
          roomname: 'Room 1',
          notes: booking.description,
          viewroomtitle: "Room booking for #{booking.user.full_name}",
          show_link: ''
        )
      end
    end
  end

  describe '#summary' do
    it 'returns the correct summary' do
      booking = build(:room_booking, start_date: Date.current, end_date: Date.current + 2.days, room: room1)

      expect(booking.summary).to eq('2 nights in Room 1')
    end
  end

  describe '#success_message' do
    let(:booking) { build(:room_booking) }

    subject(:message) { booking.success_message }

    context 'when the booking is in confirmed state' do
      it 'returns the expected message' do
        booking.state = 'confirmed'

        expect(message).to eq("Your guest room booking has been saved and confirmed. You're all good!")
      end
    end

    context 'when the booking is in pending state' do
      it 'returns the expected message' do
        booking.state = 'pending'
        expected_message =
          'Your guest room booking has been saved. ' \
          "You'll be sent an email three months before your booking starts to reconfirm it."

        expect(message).to eq(expected_message)
      end
    end

    context 'when the booking has a pending payment state' do
      let(:booking) { create(:room_booking) }

      it 'returns the expected message' do
        booking.state = 'payment_required'
        create(:room_payment, user: booking.user, room_booking: booking, amount: 90.00)
        expected_message =
          'Your guest room booking has been saved. ' \
          'You will need to pay $90.00 to confirm it.'

        expect(message).to eq(expected_message)
      end
    end
  end

  describe '#number_of_nights' do
    it 'returns the correct number of nights for a booking' do
      booking = build(:room_booking, start_date: Date.current, end_date: Date.current + 1.day)
      expect(booking.number_of_nights).to eq(1)

      booking = build(:room_booking, start_date: Date.current + 2.days, end_date: Date.current + 5.days)
      expect(booking.number_of_nights).to eq(3)
    end

    context 'when no start date or end date set' do
      it 'returns 0' do
        booking = build(:room_booking, start_date: nil, end_date: nil)

        expect(booking.number_of_nights).to eq(0)
      end
    end
  end

  describe '#number_of_nights_per_year' do
    using RSpec::Parameterized::TableSyntax

    where(:start_date, :end_date, :this_year_num_nights, :next_year_num_nights) do
      Date.new(2022, 4, 1) | Date.new(2022, 4, 7) | 6 | 0
      Date.new(2022, 12, 29) | Date.new(2023, 1, 4) | 2 | 4
      Date.new(2023, 5, 10) | Date.new(2023, 5, 17) | 7 | 0
    end

    with_them do
      let(:booking) { build(:room_booking, start_date: start_date, end_date: end_date) }

      subject(:number_of_nights_per_year) { booking.number_of_nights_per_year }

      it 'returns the correct number of nights' do
        expected_results = {}
        year = start_date.year

        expected_results[year] = this_year_num_nights
        expected_results[year + 1] = next_year_num_nights

        expect(number_of_nights_per_year).to eq(expected_results)
      end
    end
  end

  describe '#show_edit_link_for?' do
    let_it_be(:user) { create(:user, :with_household) }
    let_it_be(:booking) { create(:room_booking, user: user) }

    context 'when the passed user is the booking creator' do
      subject(:show_edit_link) { booking.show_edit_link_for?(user) }

      it { is_expected.to be_truthy }
    end

    context 'when the passed user is not the creator' do
      subject(:show_edit_link) { booking.show_edit_link_for?(another_user) }

      context 'when both users are part of the same household' do
        let(:another_user) { create(:user, household: user.household) }

        it { is_expected.to be_truthy }
      end

      context 'when both users are not part of the same household' do
        let(:another_household) { create(:household) }
        let(:another_user) { create(:user, household: another_household) }

        it { is_expected.to be_falsey }
      end
    end
  end
end
