# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EggOrder, type: :model do
  describe 'associations' do
    it { should have_many(:egg_payments) }
  end

  describe 'validations' do
    it { should validate_presence_of(:purchase_date) }
    it { should validate_presence_of(:closing_time) }
    it { should validate_presence_of(:price) }
    it { should validate_numericality_of(:price).is_greater_than_or_equal_to(0.0) }
  end

  describe '#delivery_within_next_week?' do
    let(:order) { create(:egg_order, purchase_date: purchase_date) }

    subject(:delivery_within_next_week) { order.delivery_within_next_week? }

    context 'when it is less than one week from now' do
      let(:purchase_date) { Date.current + 4.days }

      it { is_expected.to be_truthy }
    end

    context 'when it is one week from now' do
      let(:purchase_date) { Date.current + 7.days }

      it { is_expected.to be_truthy }
    end

    context 'when it is more than one week from now' do
      let(:purchase_date) { Date.current + 9.days }

      it { is_expected.to be_falsey }
    end
  end
end
