# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Eligibility, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
  end

  describe 'associations' do
    it { should belong_to(:household).optional }
  end
end
