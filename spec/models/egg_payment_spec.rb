# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EggPayment, type: :model do
  include ActiveSupport::Testing::TimeHelpers

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:egg_order) }
  end

  describe 'validations' do
    it { should validate_numericality_of(:number_of_boxes).is_greater_than_or_equal_to(0) }

    describe 'check_order_closing_time' do
      let(:egg_payment) { build(:egg_payment, egg_order: egg_order) }
      let(:egg_order) { create(:egg_order, closing_time: closing) }

      context 'when the related egg_order is not passed its closing time' do
        let(:closing) { DateTime.now + 3.days }

        it 'does not add any error' do
          expect(egg_payment).to be_valid
        end
      end

      context 'when we are passed the closing time of the related egg order' do
        let(:closing) { DateTime.now - 3.days }

        it 'adds an error' do
          expect(egg_payment).not_to be_valid
          expect(egg_payment.errors.full_messages.first).to eq('Sorry, the registration for this order is now closed.')
        end
      end

      context 'when the egg payment is already created' do
        let(:egg_payment) { build(:egg_payment, :pending, egg_order: egg_order) }
        let(:closing) { DateTime.now + 3.days }

        before do
          egg_payment.save!
        end

        it 'update the state even after closing time' do
          travel_to (Date.current + 1.week) do
            egg_payment.paid!

            expect(egg_payment.reload).to be_paid
          end
        end
      end
    end
  end

  describe 'callbacks' do
    describe 'process_payment' do
      let(:egg_order) { create(:egg_order, price: 5.0) }
      let(:recipient_fund) { create(:fund, balance: 0.0) }
      let(:user) { create(:user) }

      subject(:payment) do
        create(
          :egg_payment,
          egg_order: egg_order,
          number_of_boxes: 2,
          user: user
        )
      end

      before do
        allow(Fund).to receive(:egg_orders).and_return(recipient_fund)
      end

      context 'when the purchaser does not have enough money in fund' do
        before do
          user.fund.update(balance: 2.0)
        end

        it 'sets the payment as pending' do
          payment

          expect(payment.reload).to be_pending
        end
      end

      context 'when the purchaser has enough money in fund' do
        before do
          user.fund.update(balance: 200.0)
        end

        it 'sets the payment as paid' do
          payment

          expect(payment.reload).to be_paid
        end

        it 'transfers money accordingly and with correct description' do
          payment
          date = egg_order.delivery_date_label_short

          expect(user.fund.reload.balance).to eq(190.0)
          expect(recipient_fund.balance).to eq(10.0)
          expect(Transfer.last.sender_description).to eq('[To John] Purchase of 2 dozen of eggs.')
          expect(Transfer.last.recipient_description).to eq("[From John] Purchase for #{date} - 2 dozen")
        end
      end
    end
  end

  it { should define_enum_for(:state).with_values(%i[initial paid pending]) }
end
