# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MealPayment, type: :model do
  include ActiveSupport::Testing::TimeHelpers

  describe 'associations' do
    it { should belong_to(:meal) }
    it { should belong_to(:user) }
    it { should belong_to(:meal_seating).optional }
    it { should have_many(:meal_payment_attendees) }
    it { should have_many(:meal_portion_choices) }
  end

  describe 'validations' do
    describe 'registration_before_deadline' do
      let_it_be(:meal) { create(:meal, meal_date: Date.current + 7.days) }

      subject(:payment) { build(:meal_payment, meal: meal) }

      context 'when user registers more than three days in advance' do
        it 'is valid' do
          expect(payment).to be_valid
        end
      end

      context 'when user registers three days in advance' do
        it 'is valid' do
          travel_to Date.current + 4.days do
            expect(payment).to be_valid
          end
        end
      end

      context 'when user registers less than three days in advance' do
        it 'is not valid' do
          travel_to Date.current + 5.days do
            expect(payment).not_to be_valid
          end
        end
      end
    end
  end

  describe 'process_payment' do
    let(:user) { create(:user) }
    let(:meal) { create(:meal) }

    subject(:payment) do
      user.fund.update(balance: balance)

      create(:meal_payment, meal: meal, amount: 50.0, user: user)
    end

    context 'when there is enough money in the fund' do
      let(:balance) { 80.0 }

      it 'creates a transfer and mark the payment to paid' do
        short_date = meal.meal_date.strftime('%a %b %d')

        expect(MoneyTransferService).to receive(:new).with(
          sender_fund: user.fund,
          recipient_fund: Fund.common_meals,
          amount: 50.0,
          sender_description: "#{short_date} meal payment",
          recipient_description: "#{short_date} meal payment"
        ).and_call_original

        expect(payment.reload).to be_paid
      end
    end

    context 'when there is not enough money in the fund' do
      let(:balance) { 20.0 }

      it 'does not change the payment state and fund' do
        expect(MoneyTransferService).not_to receive(:new)

        expect(payment).to be_pending
      end
    end
  end
end
