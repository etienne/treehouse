# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MealPortionChoice, type: :model do
  describe 'associations' do
    it { should belong_to(:meal_portion) }
    it { should belong_to(:meal_payment) }
    it { should belong_to(:meal_seating_choice).optional }
  end
end
