# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:item_category) }
  end

  it do
    should define_enum_for(:price_option)
      .with_values(%i[default per_item see_description contact_seller barter best_offer free borrow])
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:item_category) }
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:price_option) }

    describe '.max_number_of_photos' do
      let(:post) { build(:post) }
      context 'when more than 3 photos are being uploaded' do
        it 'is not valid' do
          post.photos = [double, double, double, double]

          expect(post.valid?).to be_falsey
          expect(post.errors.full_messages.first).to eq('You cannot upload more than 3 photos.')
        end
      end

      context 'when 3 photos are being uploaded' do
        it 'is valid' do
          post.photos = [double, double, double]

          expect(post.valid?).to be_truthy
        end
      end
    end
  end

  describe 'scopes' do
    describe '.by_most_recent_first' do
      it 'sorts posts by date of creation, descending order' do
        oldest = create(:post)
        other_post = create(:post)
        newest = create(:post)

        expect(described_class.by_most_recent_first).to eq([newest, other_post, oldest])
      end
    end
  end
end
