# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MealSeatingAttendee, type: :model do
  describe 'associations' do
    it { should belong_to(:attendee) }
    it { should belong_to(:meal_seating_choice) }
  end
end
