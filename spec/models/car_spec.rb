# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Car, type: :model do
  describe 'associations' do
    it { should have_many(:car_bookings) }
    it { should belong_to(:user).optional }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:odometer) }
    it { should validate_presence_of(:calendar_color) }

    describe '.max_number_of_photos' do
      let(:car) { build(:car) }

      context 'when more than 3 photos are being uploaded' do
        it 'is not valid' do
          car.photos = [double, double, double, double]

          expect(car.valid?).to be_falsey
          expect(car.errors.full_messages.first).to eq('You cannot upload more than 3 photos.')
        end
      end

      context 'when 3 photos are being uploaded' do
        it 'is valid' do
          car.photos = [double, double, double]

          expect(car.valid?).to be_truthy
        end
      end
    end
  end

  describe '#title_with_emoji' do
    let(:car) { build(:car, title: 'a car', electric: electric) }

    subject(:title_with_emoji) { car.title_with_emoji }

    context 'when the car is electric' do
      let(:electric) { true }

      it 'returns the relevant title' do
        expect(title_with_emoji).to eq('a car ⚡')
      end
    end

    context 'when the car is not electric' do
      let(:electric) { false }

      it 'returns the relevant title' do
        expect(title_with_emoji).to eq('a car')
      end
    end
  end
end
