# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Room, type: :model do
  describe 'associations' do
    it { should have_many(:room_bookings) }
    it { should have_many(:area_bookings) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:calendar_color) }
  end

  describe 'scopes' do
    describe '.for_guests' do
      let!(:room_for_guest1) { create(:room, purpose: 'guest') }
      let!(:room_for_guest2) { create(:room, purpose: 'guest') }
      let!(:another_room) { create(:room, purpose: 'another') }

      it 'returns only guest rooms' do
        expect(described_class.for_guests).to contain_exactly(room_for_guest1, room_for_guest2)
      end
    end
  end
end
