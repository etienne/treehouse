# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AreaBooking, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:room) }
  end

  it { should define_enum_for(:level).with_values(%i[open_event semi_private_event private_event]) }

  it { should define_enum_for(:state).with_values(%i[confirmed pending]) }

  describe 'validations' do
    let(:datetime) { DateTime.tomorrow.beginning_of_day }
    let(:room) { create(:room) }
    let(:area_booking) do
      build(:area_booking, room: room, start_time: datetime + 5.hours, end_time: datetime + 9.hours)
    end

    it { should validate_presence_of(:start_time) }
    it { should validate_presence_of(:end_time) }
    it { should validate_presence_of(:name) }

    describe 'check_datetime_overlap' do
      using RSpec::Parameterized::TableSyntax

      where(:start_offset, :end_offset, :overlap) do
        0.hours  | 2.hours  | false
        2.hours  | 6.hours  | true
        1.hours  | 5.hours  | false # allowing check-out then check-in at the same time.
        5.hours  | 8.hours  | true
        8.hours  | 11.hours | true
        30.hours | 33.hours | false
      end

      with_them do
        it 'returns whether a new booking is overlapping with a previous one' do
          create(:area_booking, room: room, start_time: datetime + start_offset, end_time: datetime + end_offset)

          if overlap
            expect(area_booking.valid?).to be_falsey
            expect(area_booking.errors.first.full_message)
              .to eq('This booking is overlapping with another booking ' \
                     'for this same room. Please review the room booking calendar.')
          else
            expect(area_booking.valid?).to be_truthy
          end
        end
      end
    end

    describe 'start_datetime_and_end_datetime' do
      it 'checks if the start time is in the past' do
        booking = build(:area_booking, start_time: DateTime.now - 5.days)

        expect(booking.valid?).to be_falsey
        expect(booking.errors.first.full_message)
          .to eq('This booking cannot start in the past.')
      end

      it 'checks if the booking ends before it starts' do
        booking = build(:area_booking, start_time: DateTime.now + 5.hours, end_time: DateTime.now + 2.hours)

        expect(booking.valid?).to be_falsey
        expect(booking.errors.first.full_message)
          .to eq('This booking cannot end before it starts.')
      end
    end

    describe 'four_hours_max' do
      let(:start_time) { DateTime.now + 2.hours }
      let(:area_booking) { build(:area_booking, start_time: start_time, end_time: start_time + duration) }

      context 'when a booking is less than four hours long' do
        let(:duration) { 2.hours }

        it 'is valid' do
          expect(area_booking).to be_valid
        end
      end

      context 'when a booking is four hours long' do
        let(:duration) { 4.hours }

        it 'is valid' do
          expect(area_booking).to be_valid
        end
      end

      context 'when a booking is more than four hours long' do
        let(:duration) { 6.hours }

        it 'is not valid' do
          expect(area_booking).not_to be_valid
        end
      end
    end
  end

  describe '#serialize_for_calendar_for' do
    let_it_be(:user) { create(:user) }

    let(:booking) { create(:area_booking, user: user, room: room) }
    let(:room) { create(:room) }
    let(:starttime) { Time.parse(booking.start_time.to_s).strftime('%l:%M %p').strip }
    let(:endtime) { Time.parse(booking.end_time.to_s).strftime('%l:%M %p').strip }
    let(:the_date) { Date.parse(booking.start_time.to_s).strftime('%a %B %d') }
    let(:expected_booking) do
      {
        areaname: booking.room.name,
        title: '🟢 Birthday party',
        arealevel: '🟢 Open event',
        start: booking.start_time,
        end: booking.end_time,
        period: "#{the_date}, from #{starttime} to #{endtime}",
        booking_id: booking.id,
        show_message: false,
        host: 'John Doe',
        color: '#123456',
        notes: booking.description,
        eventtype: 'booking',
        show_link: Rails.application.routes.url_helpers.bookings_area_url(booking)
      }
    end

    it 'returns the expected data' do
      expect(booking.serialize_for_calendar_for(user)).to eq(expected_booking)
    end
  end

  describe '.green_level' do
    subject(:green) { described_class.green_level }

    it 'returns the correct struct' do
      expect(green.color).to eq('🟢')
      expect(green.label).to eq('Open')
      expect(green.id).to eq(0)
    end
  end

  describe '.yellow_level' do
    subject(:yellow) { described_class.yellow_level }

    it 'returns the correct struct' do
      expect(yellow.color).to eq('🟡')
      expect(yellow.label).to eq('Semi-private')
      expect(yellow.id).to eq(1)
    end
  end

  describe '.red_level' do
    subject(:red) { described_class.red_level }

    it 'returns the correct struct' do
      expect(red.color).to eq('🔴')
      expect(red.label).to eq('Private')
      expect(red.id).to eq(2)
    end
  end
end
