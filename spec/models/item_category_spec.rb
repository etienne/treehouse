# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ItemCategory, type: :model do
  describe 'associations' do
    it { should have_many(:posts) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
  end
end
