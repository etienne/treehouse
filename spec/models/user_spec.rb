# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it { should have_many(:room_bookings) }
    it { should have_many(:posts) }
    it { should have_many(:room_payments) }
    it { should have_many(:car_bookings) }
    it { should have_many(:car_booking_payments) }
    it { should have_many(:area_bookings) }
    it { should have_many(:meals) }
    it { should have_many(:meal_payments) }
    it { should have_many(:egg_payments) }
    it { should have_many(:funds) }
    it { should have_and_belong_to_many(:roles) }
    it { should belong_to(:household).optional }
    it { should have_one(:car) }
    it { should have_one(:user_setting) }
  end

  describe 'validations' do
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
  end

  describe 'callbacks' do
    describe 'set_household' do
      let_it_be(:household) { create(:household) }
      let_it_be(:user_email) { 'test@email.com' }
      let!(:eligibility) { create(:eligibility, email: user_email, household: household) }

      subject(:new_user) { build(:user, email: user_email) }

      context 'when the user has no household set in their eligibility' do
        let(:household) { nil }

        it 'saves the user without a household' do
          new_user.save!

          expect(new_user.reload.household).to be_nil
        end
      end

      context 'when the user has a household set in their eligibility' do
        let(:household) { create(:household) }

        it 'saves the user with this household' do
          new_user.save!

          expect(new_user.reload.household).to eq(household)
        end
      end
    end

    describe 'create_new_personal_fund!' do
      let(:user) { create(:user) }

      it 'creates a new fund for the newly created user' do
        expect { user }.to change(Fund, :count).by 1

        expect(Fund.last.user).to eq(user)
      end
    end

    describe 'create_user_settings!' do
      let(:user) { create(:user) }

      it 'creates a new UserSetting object' do
        expect { user }.to change(Fund, :count).by 1

        expect(UserSetting.last.user).to eq(user)
      end
    end
  end

  describe '#eligibility?' do
    let(:email) { 'test@example.com' }
    let(:user) { build(:user, email: email) }

    context 'when a related Eligible instance does not exist' do
      it 'raises an error' do
        allow(user).to receive(:override_eligibility_check?).and_return(false)
        expect { user.save }.not_to(change { User.count })

        expect(user.errors.first.full_message).to eq('This email address does not belong to an eligible TVE member')
      end
    end

    context 'when a related Eligible instance exists' do
      it 'creates an user' do
        create(:eligibility, email: email)

        expect { user.save }.to change { User.count }.by(1)
      end

      context 'when the email address is provided with a different casing' do
        let(:email) { 'TesT@Example.com' }

        it 'creates an user' do
          create(:eligibility, email: email.downcase)

          expect { user.save }.to change { User.count }.by(1)
        end
      end
    end
  end

  shared_examples 'allowing to access a page' do
    describe 'allowing to access a page' do
      let(:user) { create(:user) }

      it { is_expected.to be_falsey }

      context 'when the passed feature flag is turned on for this user' do
        it 'returns true' do
          Flipper.enable_actor(feature_flag, user)

          expect(user_allowed_to_access_page).to be_truthy
        end
      end
    end
  end

  describe '#admin?' do
    subject(:admin?) { user.admin? }

    context 'when the user has no role' do
      let(:user) { build(:user) }

      it { is_expected.to be_falsey }
    end

    context 'when the user has a role' do
      let(:user) { build(:user, :guest_room_coordinator) }

      it { is_expected.to be_truthy }
    end
  end

  describe '#area_booking_coordinator?' do
    subject(:coordinator?) { user.area_booking_coordinator? }

    context 'when the user has no role' do
      let(:user) { build(:user) }

      it { is_expected.to be_falsey }
    end

    context 'when the user has the area_booking_coordinator role' do
      let(:user) { build(:user, :area_booking_coordinator) }

      it { is_expected.to be_truthy }
    end

    context 'when the user has another role' do
      let(:user) { build(:user, :random) }

      it { is_expected.to be_falsey }
    end
  end

  describe '#guest_room_coordinator?' do
    subject(:coordinator?) { user.guest_room_coordinator? }

    context 'when the user has no role' do
      let(:user) { build(:user) }

      it { is_expected.to be_falsey }
    end

    context 'when the user has the guest_room_coordinator role' do
      let(:user) { build(:user, :guest_room_coordinator) }

      it { is_expected.to be_truthy }
    end

    context 'when the user has another role' do
      let(:user) { build(:user, :random) }

      it { is_expected.to be_falsey }
    end
  end

  describe '#car_admin?' do
    subject(:car_admin?) { user.car_admin? }

    context 'when the user has no role' do
      let(:user) { build(:user) }

      it { is_expected.to be_falsey }
    end

    context 'when the user has the car_admin role' do
      let(:user) { build(:user, :car_admin) }

      it { is_expected.to be_truthy }
    end

    context 'when the user has another role' do
      let(:user) { build(:user, :random) }

      it { is_expected.to be_falsey }
    end
  end

  describe '#egg_admin?' do
    subject(:egg_admin?) { user.egg_admin? }

    context 'when the user has no role' do
      let(:user) { build(:user) }

      it { is_expected.to be_falsey }
    end

    context 'when the user has the egg_admin role' do
      let(:user) { build(:user, :egg_admin) }

      it { is_expected.to be_truthy }
    end

    context 'when the user has another role' do
      let(:user) { build(:user, :random) }

      it { is_expected.to be_falsey }
    end
  end

  describe '#fund_admin?' do
    subject(:fund_admin?) { user.fund_admin? }

    context 'when the user has no role' do
      let(:user) { build(:user) }

      it { is_expected.to be_falsey }
    end

    context 'when the user has the fund_admin role' do
      let(:user) { build(:user, :fund_admin) }

      it { is_expected.to be_truthy }
    end

    context 'when the user has another role' do
      let(:user) { build(:user, :random) }

      it { is_expected.to be_falsey }
    end
  end

  describe '#household_mention' do
    subject(:household_mention) { user.household_mention }

    context 'when the user has no household attached' do
      let(:user) { build(:user, first_name: 'John', last_name: 'Doe', household_id: nil) }

      it 'returns the full name' do
        expect(household_mention).to eq('John Doe')
      end
    end

    context 'when the user has a household attached' do
      let(:household) { create(:household, name: 'The Does') }
      let(:user) { build(:user, first_name: 'John', last_name: 'Doe', household: household) }

      it 'returns the household mention' do
        expect(household_mention).to eq('The Does household')
      end
    end
  end

  describe '#num_booked_nights' do
    include ActiveSupport::Testing::TimeHelpers

    let_it_be(:household) { create(:household) }
    let_it_be(:user1) { create(:user, household: household) }
    let_it_be(:user2) { create(:user, household: household) }

    it 'returns the relevant number of nights for this year and next year' do
      travel_to Date.new(2024, 2, 2) do
        allow(Date).to receive(:current).and_return(Date.new(2024, 4, 3))

        create(
          :room_booking,
          user: user1,
          start_date: Date.new(2024, 5, 4),
          end_date: Date.new(2024, 5, 7) # 3 nights
        )
        create(
          :room_booking,
          :free,
          user: user1,
          start_date: Date.new(2024, 6, 4),
          end_date: Date.new(2024, 6, 7) # 3 free nights - they don't count
        )
        create(
          :room_booking,
          user: user2,
          start_date: Date.new(2024, 7, 5),
          end_date: Date.new(2024, 7, 10) # 5 nights
        )
        create(
          :room_booking,
          user: user2,
          start_date: Date.new((2024 + 1), 7, 5),
          end_date: Date.new((2024 + 1), 7, 9) # 4 nights
        )
        create(
          :room_booking,
          user: user1,
          start_date: Date.new((2024 + 2), 7, 5),
          end_date: Date.new((2024 + 2), 7, 9)
        )

        expected_result = {}
        expected_result[2024] = 8
        expected_result[2025] = 4

        expect(user1.num_booked_nights).to eq(expected_result)
      end
    end
  end

  describe '#price_to_pay_for' do
    using RSpec::Parameterized::TableSyntax

    let_it_be(:user) { create(:user) }

    where(:remaining_this_year, :remaining_next_year, :start_date, :end_date, :expected_price) do
      10      | 10     | Date.new(2050, 3, 4) | Date.new(2050, 3, 10)   | 0
      4       | 10     | Date.new(2050, 3, 4) | Date.new(2050, 3, 8)    | 0
      1       | 10     | Date.new(2050, 3, 4) | Date.new(2050, 3, 10)   | 75
      0       | 10     | Date.new(2050, 12, 27) | Date.new(2051, 1, 5)  | 60
      8       | 0      | Date.new(2050, 12, 27) | Date.new(2051, 1, 5)  | 75
      0       | 0      | Date.new(2050, 12, 28) | Date.new(2051, 1, 4)  | 105
    end

    with_them do
      it 'returns the correct price' do
        booking = create(:room_booking, user: user, start_date: start_date, end_date: end_date)
        allow(user).to receive(:remaining_nights_for).with(2050).and_return(remaining_this_year)
        allow(user).to receive(:remaining_nights_for).with(2051).and_return(remaining_next_year)

        expect(user.price_to_pay_for(booking)).to eq(expected_price)
      end
    end
  end

  describe 'archive!' do
    let(:user) { create(:user) }

    subject(:archive_user) { user.archive! }

    context 'when a user who created posts and bookings is archived' do
      it 'archives this user and deletes posts and bookings' do
        another_user = create(:user)
        create(:room_booking, user: user)
        create(:post, user: user)
        create(:post, user: user)
        create(:eligibility, email: user.email)

        create(:room_booking, user: another_user)
        create(:post, user: another_user)

        expect(Post.all.count).to eq(3)
        expect(RoomBooking.all.count).to eq(2)
        expect(Eligibility.all.count).to eq(1)

        result = archive_user

        expect(result).to be_truthy
        expect(user.reload.archived).to be_truthy
        expect(Post.all.reload.count).to eq(1)
        expect(RoomBooking.all.reload.count).to eq(1)
        expect(Eligibility.all.count).to eq(0)
      end
    end

    context 'when a user with pending payments is archived' do
      it 'is not actually archived and returns an error' do
        room_booking = create(:room_booking, user: user)
        create(:room_payment, :pending, room_booking: room_booking, user: user)

        result = archive_user

        expect(result).to be_falsey
        expect(user.reload.archived).to be_falsey
        expect(user.errors.full_messages.first)
          .to eq('This account cannot be archived as it has payments that are still pending.')
      end
    end

    context 'when a user with paid payments is archived' do
      it 'is archived' do
        room_booking = create(:room_booking, user: user)
        create(:room_payment, room_booking: room_booking, user: user)

        result = archive_user

        expect(result).to be_truthy
        expect(user.reload.archived).to be_truthy
        expect(RoomBooking.all.reload.count).to eq(0)
        expect(RoomPayment.all.reload.count).to eq(1)
      end
    end
  end

  describe '#upcoming_meals' do
    include ActiveSupport::Testing::TimeHelpers

    let(:user) { create(:user) }
    let(:user) { create(:user, :with_household) }
    let(:meal1) { create(:meal, meal_date: 4.days.from_now) }
    let(:meal2) { create(:meal, meal_date: 8.days.from_now) }

    subject(:upcoming_meals) { user.upcoming_meals }

    before do
      create(:meal, meal_date: 12.days.from_now)

      create(:meal_payment, user: user, meal: meal1)
      create(:meal_payment, user: user, meal: meal2)
    end

    context 'when the user belongs to a household' do
      it 'returns the future meals the user signed up for' do
        travel_to 5.days.from_now do
          expect(upcoming_meals).to contain_exactly(meal2)
        end
      end

      context 'when we check upcoming meals for someone else from the same household' do
        it 'returns the future meals the original user signed up for' do
          other_user = create(:user, household: user.household)

          travel_to 5.days.from_now do
            expect(other_user.upcoming_meals).to contain_exactly(meal2)
          end
        end
      end
    end

    context 'when the user does now belong to a household' do
      it 'returns the future meals the user signed up for' do
        travel_to 5.days.from_now do
          expect(upcoming_meals).to contain_exactly(meal2)
        end
      end
    end
  end
end
