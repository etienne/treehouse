class AddInteracToTransfers < ActiveRecord::Migration[7.0]
  def change
    add_column :transfers, :interac, :string, limit: 20
  end
end
