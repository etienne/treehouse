class AddNewBalanceRemoveDescriptionToTransfers < ActiveRecord::Migration[7.0]
  def change
    add_column :transfers, :sender_new_balance, :float, null: true
    add_column :transfers, :recipient_new_balance, :float, default: 0.0
    remove_column :transfers, :description, :string
  end
end
