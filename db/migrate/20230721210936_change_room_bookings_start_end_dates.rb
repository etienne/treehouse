class ChangeRoomBookingsStartEndDates < ActiveRecord::Migration[7.0]
  def change
    change_column :room_bookings, :start_date, :date
    change_column :room_bookings, :end_date, :date
  end
end
