class CreatePayments < ActiveRecord::Migration[7.0]
  DEFAULT_STATE = 0

  def change
    create_table :payments do |t|
      t.string :description
      t.references :user, null: false, foreign_key: true
      t.references :source, polymorphic: true, null: false
      t.decimal :amount, precision: 8, scale: 2
      t.integer :state, null: false, default: DEFAULT_STATE, limit: 1

      t.timestamps
    end
  end
end
