class CreateHouseholds < ActiveRecord::Migration[7.0]
  def change
    create_table :households do |t|
      t.string :name

      t.timestamps
    end
    add_reference :users, :household, null: true, foreign_key: true
  end
end
