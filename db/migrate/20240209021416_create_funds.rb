class CreateFunds < ActiveRecord::Migration[7.0]
  def up
    create_table :funds do |t|
      t.float :balance, default: 0.0
      t.references :user, null: true, foreign_key: true

      t.timestamps
    end
  end

  def down
    drop_table :funds
  end
end
