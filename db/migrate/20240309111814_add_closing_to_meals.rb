class AddClosingToMeals < ActiveRecord::Migration[7.0]
  def change
    add_column :meals, :closing_reminded_at, :datetime
    add_column :meals, :closed, :boolean, default: false
  end
end
