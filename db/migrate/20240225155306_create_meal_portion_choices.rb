class CreateMealPortionChoices < ActiveRecord::Migration[7.0]
  def change
    create_table :meal_portion_choices do |t|
      t.references :meal_portion, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :quantity, default: 0

      t.timestamps
    end
  end
end
