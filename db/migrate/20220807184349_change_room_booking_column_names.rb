class ChangeRoomBookingColumnNames < ActiveRecord::Migration[7.0]
  def change
    rename_column :room_bookings, :start, :start_date
    rename_column :room_bookings, :end, :end_date
  end
end
