class CreateEggOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :egg_orders do |t|
      t.date :purchase_date
      t.datetime :closing_time
      t.float :price, default: 0.0

      t.timestamps
    end
  end
end
