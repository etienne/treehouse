class AddReimburseToFundToUserSettings < ActiveRecord::Migration[7.1]
  def change
    add_column :user_settings, :reimburse_to_fund, :boolean, default: false
  end
end
