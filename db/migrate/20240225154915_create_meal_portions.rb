class CreateMealPortions < ActiveRecord::Migration[7.0]
  def change
    create_table :meal_portions do |t|
      t.references :meal, null: false, foreign_key: true
      t.string :description
      t.float :amount, default: 0.0

      t.timestamps
    end
  end
end
