class AddCommunityToFunds < ActiveRecord::Migration[7.2]
  def change
    add_column :funds, :community, :boolean, default: false, null: false
  end
end
