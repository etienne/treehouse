class CreateEggPayments < ActiveRecord::Migration[7.0]
  DEFAULT_STATE = 0

  def change
    create_table :egg_payments do |t|
      t.references :egg_order, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :number_of_boxes, default: 0
      t.integer :state, default: DEFAULT_STATE, null: false, limit: 1

      t.timestamps
    end
  end
end
