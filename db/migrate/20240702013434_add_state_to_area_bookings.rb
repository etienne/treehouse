class AddStateToAreaBookings < ActiveRecord::Migration[7.0]
  CONFIRMED = 0

  def change
    add_column :area_bookings, :state, :integer, default: CONFIRMED, null: false, limit: 1
  end
end
