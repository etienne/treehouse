class AddGuestsToMealSeatingChoice < ActiveRecord::Migration[8.0]
  def change
    add_column :meal_seating_choices, :guests, :integer, default: 0
  end
end
