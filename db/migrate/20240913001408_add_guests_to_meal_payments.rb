class AddGuestsToMealPayments < ActiveRecord::Migration[7.0]
  def change
    add_column :meal_payments, :guests, :integer, default: 0
  end
end
