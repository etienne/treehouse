class CreateCars < ActiveRecord::Migration[7.0]
  def change
    create_table :cars do |t|
      t.string :title
      t.text :description
      t.integer :odometer, null: false, limit: 3
      t.json :photos
      t.json :documents

      t.timestamps
    end
  end
end
