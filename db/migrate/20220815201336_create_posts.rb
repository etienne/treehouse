class CreatePosts < ActiveRecord::Migration[7.0]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :description
      t.decimal :price, precision: 8, scale: 2
      t.boolean :price_per_item, default: false
      t.boolean :multiple_prices, default: false
      t.references :user, null: false, foreign_key: true
      t.references :item_category, null: false, foreign_key: true

      t.timestamps
    end
  end
end
