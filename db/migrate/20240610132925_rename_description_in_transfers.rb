class RenameDescriptionInTransfers < ActiveRecord::Migration[7.0]
  def change
    rename_column :transfers, :description, :recipient_description
  end
end
