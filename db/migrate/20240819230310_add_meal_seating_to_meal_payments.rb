class AddMealSeatingToMealPayments < ActiveRecord::Migration[7.0]
  def change
    add_reference :meal_payments, :meal_seating, null: true, foreign_key: true
    add_column :meal_payments, :take_out, :boolean, default: true
  end
end
