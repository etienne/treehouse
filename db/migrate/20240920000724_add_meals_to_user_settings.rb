class AddMealsToUserSettings < ActiveRecord::Migration[7.0]
  def change
    add_column :user_settings, :meals, :boolean, default: true
  end
end
