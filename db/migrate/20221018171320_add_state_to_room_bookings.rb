class AddStateToRoomBookings < ActiveRecord::Migration[7.0]
  DEFAULT_STATE = 0

  def change
    add_column :room_bookings, :state, :integer, default: DEFAULT_STATE, null: false, limit: 1
  end
end
