class CreateAreaBooking < ActiveRecord::Migration[7.0]
  def change
    create_table :area_bookings do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.references :user, null: false, foreign_key: true
      t.references :room, null: false, foreign_key: true
      t.text :description

      t.timestamps
    end
  end
end
