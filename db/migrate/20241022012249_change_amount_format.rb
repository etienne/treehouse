class ChangeAmountFormat < ActiveRecord::Migration[7.1]
  def up
    change_column :funds, :balance, :decimal, precision: 7, scale: 2
    change_column :transfers, :amount, :decimal, precision: 7, scale: 2
    change_column :transfers, :sender_new_balance, :decimal, precision: 7, scale: 2
    change_column :transfers, :recipient_new_balance, :decimal, precision: 7, scale: 2
  end

  def down; end
end
