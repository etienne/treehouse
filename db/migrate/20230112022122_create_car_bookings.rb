class CreateCarBookings < ActiveRecord::Migration[7.0]
  DEFAULT_STATE = 0

  def change
    create_table :car_bookings do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.references :user, null: false, foreign_key: true
      t.references :car, null: false, foreign_key: true
      t.boolean :issue_to_report, null: true
      t.text :issue_description
      t.integer :state, null: false, limit: 1, default: DEFAULT_STATE

      t.timestamps
    end
  end
end
