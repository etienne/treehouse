class CreateMealSeatingChoice < ActiveRecord::Migration[8.0]
  def change
    create_table :meal_seating_choices do |t|
      t.references :meal_payment, null: false, foreign_key: true
      t.references :meal_seating, null: false, foreign_key: true

      t.timestamps
    end
  end
end
