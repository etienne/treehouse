class AddUserRefToCars < ActiveRecord::Migration[7.0]
  def change
    add_reference :cars, :user, null: true, foreign_key: true
  end
end
