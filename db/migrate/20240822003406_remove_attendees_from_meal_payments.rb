class RemoveAttendeesFromMealPayments < ActiveRecord::Migration[7.0]
  def change
    remove_column :meal_payments, :attendees
  end
end
