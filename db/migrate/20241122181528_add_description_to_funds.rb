class AddDescriptionToFunds < ActiveRecord::Migration[7.2]
  def change
    add_column :funds, :description, :text
  end
end
