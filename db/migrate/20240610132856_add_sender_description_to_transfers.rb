class AddSenderDescriptionToTransfers < ActiveRecord::Migration[7.0]
  def change
    add_column :transfers, :sender_description, :string
  end
end
