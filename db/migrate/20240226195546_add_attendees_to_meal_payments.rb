class AddAttendeesToMealPayments < ActiveRecord::Migration[7.0]
  def change
    add_column :meal_payments, :attendees, :string
  end
end
