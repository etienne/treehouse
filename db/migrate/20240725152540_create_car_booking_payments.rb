class CreateCarBookingPayments < ActiveRecord::Migration[7.0]
  DEFAULT_STATE = 0

  def change
    create_table :car_booking_payments do |t|
      t.references :user, null: false, foreign_key: true
      t.date :period_start
      t.date :period_end
      t.float :amount, precision: 7, scale: 2
      t.integer :state, default: DEFAULT_STATE, null: false, limit: 1

      t.timestamps
    end
  end
end
