class ChangeDefaultForTakeOutOnMealPayments < ActiveRecord::Migration[7.0]
  def change
    change_column_default :meal_payments, :take_out, from: true, to: false
  end
end
