class CreateRoomBookings < ActiveRecord::Migration[7.0]
  def change
    create_table :room_bookings do |t|
      t.datetime :start
      t.datetime :end
      t.text :description
      t.references :user, null: false, foreign_key: true
      t.references :room, null: false, foreign_key: true

      t.timestamps
    end
  end
end
