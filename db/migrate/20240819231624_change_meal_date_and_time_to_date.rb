class ChangeMealDateAndTimeToDate < ActiveRecord::Migration[7.0]
  def up
    change_column :meals, :date_and_time, :date
    rename_column :meals, :date_and_time, :meal_date
  end

  def down
    rename_column :meals, :meal_date, :date_and_time
    change_column :meals, :date_and_time, :datetime
  end
end
