class RemoveMealTimeNonNullConstraint < ActiveRecord::Migration[8.0]
  def change
    change_column_null :meal_seatings, :meal_time, true
  end
end
