class AddNoteToCarBookings < ActiveRecord::Migration[7.0]
  def change
    add_column :car_bookings, :note, :text
  end
end
