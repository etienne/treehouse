class AddNameToFunds < ActiveRecord::Migration[7.0]
  def change
    add_column :funds, :name, :string
  end
end
