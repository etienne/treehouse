class CreateMealPayments < ActiveRecord::Migration[7.0]
  def change
    create_table :meal_payments do |t|
      t.references :meal, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.float :amount, default: 0.0

      t.timestamps
    end
  end
end
