class ChangePaymentsToRoomPayments < ActiveRecord::Migration[7.0]
  DEFAULT_STATE = 0

  def up
    create_table :room_payments do |t|
      t.references :user, null: false, foreign_key: true
      t.references :room_booking, foreign_key: true
      t.string :description
      t.decimal :amount, precision: 8, scale: 2
      t.integer :state, null: false, default: DEFAULT_STATE, limit: 1

      t.timestamps
    end

    drop_table :payments
  end

  def down
    create_table :payments do |t|
      t.string :description
      t.references :user, null: false, foreign_key: true
      t.references :source, polymorphic: true, null: false
      t.decimal :amount, precision: 8, scale: 2
      t.integer :state, null: false, default: DEFAULT_STATE, limit: 1

      t.timestamps
    end

    drop_table :room_payments
  end
end
