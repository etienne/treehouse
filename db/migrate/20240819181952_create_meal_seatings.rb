class CreateMealSeatings < ActiveRecord::Migration[7.0]
  def change
    create_table :meal_seatings do |t|
      t.references :meal, null: false, foreign_key: true
      t.datetime :meal_time, null: false

      t.timestamps
    end
  end
end
