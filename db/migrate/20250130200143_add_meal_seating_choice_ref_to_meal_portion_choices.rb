class AddMealSeatingChoiceRefToMealPortionChoices < ActiveRecord::Migration[8.0]
  def change
    add_reference :meal_portion_choices, :meal_seating_choice, null: true, foreign_key: true
  end
end
