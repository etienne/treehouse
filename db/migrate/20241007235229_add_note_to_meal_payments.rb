class AddNoteToMealPayments < ActiveRecord::Migration[7.1]
  def change
    add_column :meal_payments, :note, :string
  end
end
