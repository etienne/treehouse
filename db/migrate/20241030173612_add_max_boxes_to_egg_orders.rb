class AddMaxBoxesToEggOrders < ActiveRecord::Migration[7.1]
  def change
    add_column :egg_orders, :max_boxes, :integer
  end
end
