class AddHouseholdToEligibility < ActiveRecord::Migration[7.0]
  def change
    add_reference :eligibilities, :household, null: true, foreign_key: true
  end
end
