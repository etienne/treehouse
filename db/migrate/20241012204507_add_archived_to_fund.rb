class AddArchivedToFund < ActiveRecord::Migration[7.1]
  def change
    add_column :funds, :archived, :boolean, default: false, null: false
  end
end
