class AddMultipleColumnsToAreaBookings < ActiveRecord::Migration[7.0]
  OPEN_LEVEL = 0

  def change
    add_column :area_bookings, :level, :integer, default: OPEN_LEVEL, null: false, limit: 1
    add_column :area_bookings, :name, :string
  end
end
