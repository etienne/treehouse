class AddEmeritusToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :emeritus, :boolean, default: false
  end
end
