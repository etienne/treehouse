class CreateMeals < ActiveRecord::Migration[7.0]
  def change
    create_table :meals do |t|
      t.string :title
      t.text :description
      t.datetime :date_and_time
      t.float :cost

      t.timestamps
    end
  end
end
