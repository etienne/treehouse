class AddPricingOptionToPost < ActiveRecord::Migration[7.0]
  DEFAULT_STATE = 0

  def up
    add_column :posts, :price_option, :integer, default: DEFAULT_STATE, null: false

    if ActiveRecord::Base.connection.column_exists?(:posts, :multiple_prices)
      remove_column :posts, :multiple_prices
    end

    if ActiveRecord::Base.connection.column_exists?(:posts, :price_per_item)
      remove_column :posts, :price_per_item
    end
  end

  def down
    remove_column :posts, :price_option
  end
end
