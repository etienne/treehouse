class AddDescriptionToTransfers < ActiveRecord::Migration[7.0]
  def change
    add_column :transfers, :description, :string
    change_column_null :transfers, :recipient_id, true
  end
end
