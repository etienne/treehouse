class UpdateSourceIdInPayments < ActiveRecord::Migration[7.0]
  def change
    change_column_null :payments, :source_id, true
    change_column_null :payments, :source_type, true
  end
end
