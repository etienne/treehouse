class CreateMealSeatingAttendees < ActiveRecord::Migration[8.0]
  def change
    create_table :meal_seating_attendees do |t|
      t.references :attendee, null: false, polymorphic: true
      t.references :meal_seating_choice, null: false, foreign_key: true

      t.timestamps
    end
  end
end
