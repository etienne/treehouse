class AddCalendarColorToRooms < ActiveRecord::Migration[7.0]
  def change
    add_column :rooms, :calendar_color, :string
  end
end
