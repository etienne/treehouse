class AddElectricToCars < ActiveRecord::Migration[7.0]
  def change
    add_column :cars, :electric, :boolean, null: false, default: false
  end
end
