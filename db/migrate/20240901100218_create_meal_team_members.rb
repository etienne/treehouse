class CreateMealTeamMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :meal_team_members do |t|
      t.references :meal, null: false, foreign_key: true
      t.references :user, null: true, foreign_key: true
      t.string :role

      t.timestamps
    end
  end
end
