class AddHouseholdToMealPayments < ActiveRecord::Migration[7.1]
  def change
    add_reference :meal_payments, :household, foreign_key: true
  end
end
