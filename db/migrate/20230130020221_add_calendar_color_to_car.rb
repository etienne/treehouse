class AddCalendarColorToCar < ActiveRecord::Migration[7.0]
  def change
    add_column :cars, :calendar_color, :string
  end
end
