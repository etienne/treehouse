class CreateMealPaymentAttendees < ActiveRecord::Migration[7.0]
  def change
    create_table :meal_payment_attendees do |t|
      t.references :meal, null: false, foreign_key: true
      t.references :meal_payment, null: false, foreign_key: true
      t.references :attendee, polymorphic: true

      t.timestamps
    end
  end
end
