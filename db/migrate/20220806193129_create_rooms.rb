class CreateRooms < ActiveRecord::Migration[7.0]
  def change
    create_table :rooms do |t|
      t.string :name
      t.string :description
      t.string :slug
      t.string :purpose

      t.timestamps
    end
  end
end
