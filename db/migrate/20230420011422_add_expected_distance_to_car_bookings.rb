class AddExpectedDistanceToCarBookings < ActiveRecord::Migration[7.0]
  def change
    add_column :car_bookings, :expected_distance, :integer, null: false, default: 0
  end
end
