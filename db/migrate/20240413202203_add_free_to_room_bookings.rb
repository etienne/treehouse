class AddFreeToRoomBookings < ActiveRecord::Migration[7.0]
  def change
    add_column :room_bookings, :free, :boolean, default: false
  end
end
