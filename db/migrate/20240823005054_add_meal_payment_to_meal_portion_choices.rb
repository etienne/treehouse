class AddMealPaymentToMealPortionChoices < ActiveRecord::Migration[7.0]
  def change
    add_reference :meal_portion_choices, :meal_payment, null: false, foreign_key: true
    remove_reference :meal_portion_choices, :user
  end
end
