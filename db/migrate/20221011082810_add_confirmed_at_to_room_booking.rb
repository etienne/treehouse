class AddConfirmedAtToRoomBooking < ActiveRecord::Migration[7.0]
  def change
    add_column :room_bookings, :confirmed_at, :datetime, default: nil
  end
end
