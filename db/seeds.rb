# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

if Room.count.zero?
  Room.create(
    name: 'Guest Room 1',
    slug: 'guestroomone',
    description: 'room with a double bed.',
    purpose: 'guest',
    calendar_color: '#cc0000'
  )
  Room.create(
    name: 'Guest Room 2',
    slug: 'guestroomtwo',
    description: 'room with a double bed and two single bunk beds.',
    purpose: 'guest',
    calendar_color: '#2e2eb8'
  )
end

if ItemCategory.count.zero?
  ItemCategory.create(name: 'Books', description: 'A category for all sorts of books.')
  ItemCategory.create(name: 'Clothing', description: 'Everything related to clothing items, for all ages.')
  ItemCategory.create(
    name: 'Decoration',
    description: 'Category for items that decorates a home and/or relates to interior design.'
  )
  ItemCategory.create(
    name: 'Electronics',
    description: 'All pieces of technology that has electronic components in them.'
  )
  ItemCategory.create(
    name: 'Entertainment',
    description: 'This category includes all kinds of games and items related to music, painting, etc.'
  )
  ItemCategory.create(
    name: 'Furniture',
    description: 'This category can include the smallest items (eg. a stool) or the biggest ones (eg. a bed frame)'
  )
  ItemCategory.create(
    name: 'Kids and babies',
    description: 'Your little one is growing up! This category includes items ' \
                 'your child don\'t use (or cannot use) any longer.'
  )
  ItemCategory.create(
    name: 'Kitchenware',
    description: 'All items that are closely or loosely related to food preparation.'
  )
  ItemCategory.create(
    name: 'Linens',
    description: 'Items related to bedding, towels and sorts of cloths.'
  )
  ItemCategory.create(
    name: 'Sport and outdoors',
    description: 'All equipment used in physical activities (eg. bikes, other sport gear), ' \
                 'including items used in outdoor activities such as camping.'
  )
  ItemCategory.create(
    name: 'Vehicle equipment',
    description: 'Items that are used as auto parts or for any other sorts of motorized vehicles.'
  )
  ItemCategory.create(name: 'Other', description: 'Everything else that does not seem to fit in any category above.')
end

if Role.count.zero?
  Role.create(name: 'App admin', slug: 'app_admin')
  Role.create(name: 'Guest room coordinator', slug: 'guest_room_coordinator')
  Role.create(name: 'Car admin', slug: 'car_admin')
  Role.create(name: 'Fund admin', slug: 'fund_admin')
  Role.create(name: 'Egg admin', slug: 'egg_admin')
  Role.create(name: 'Area booking coordinator', slug: 'area_booking_coordinator')
  Role.create(name: 'Car booking payment admin', slug: 'car_booking_payment_admin')
  Role.create(name: 'Common meal admin', slug: 'common_meal_admin')
end

# Community funds
Fund.create(name: 'Meals', user: nil)

if Rails.env.development? && User.count.zero?
  Eligibility.create(email: 'admin@example.com')
  Eligibility.create(email: 'user@example.com')

  User.create(
    email: 'user@example.com',
    first_name: 'User',
    last_name: 'Example',
    confirmed_at: DateTime.now,
    password: 'password'
  )

  # admin = User.create(
  #   email: 'admin@example.com',
  #   first_name: 'Admin',
  #   last_name: 'Example',
  #   confirmed_at: DateTime.now,
  #   password: 'password'
  # )
end
