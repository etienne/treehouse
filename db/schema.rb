# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2025_02_12_021023) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"

  create_table "area_bookings", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.bigint "user_id", null: false
    t.bigint "room_id", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "level", limit: 2, default: 0, null: false
    t.string "name"
    t.integer "state", limit: 2, default: 0, null: false
    t.index ["room_id"], name: "index_area_bookings_on_room_id"
    t.index ["user_id"], name: "index_area_bookings_on_user_id"
  end

  create_table "car_booking_payments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.date "period_start"
    t.date "period_end"
    t.float "amount"
    t.integer "state", limit: 2, default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_car_booking_payments_on_user_id"
  end

  create_table "car_bookings", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.bigint "user_id", null: false
    t.bigint "car_id", null: false
    t.boolean "issue_to_report"
    t.text "issue_description"
    t.integer "state", limit: 2, default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "expected_distance", default: 0, null: false
    t.text "note"
    t.index ["car_id"], name: "index_car_bookings_on_car_id"
    t.index ["user_id"], name: "index_car_bookings_on_user_id"
  end

  create_table "cars", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "odometer", null: false
    t.json "photos"
    t.json "documents"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "calendar_color"
    t.boolean "electric", default: false, null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_cars_on_user_id"
  end

  create_table "egg_orders", force: :cascade do |t|
    t.date "purchase_date"
    t.datetime "closing_time"
    t.float "price", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "max_boxes"
  end

  create_table "egg_payments", force: :cascade do |t|
    t.bigint "egg_order_id", null: false
    t.bigint "user_id", null: false
    t.integer "number_of_boxes", default: 0
    t.integer "state", limit: 2, default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["egg_order_id"], name: "index_egg_payments_on_egg_order_id"
    t.index ["user_id"], name: "index_egg_payments_on_user_id"
  end

  create_table "eligibilities", force: :cascade do |t|
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "household_id"
    t.index ["household_id"], name: "index_eligibilities_on_household_id"
  end

  create_table "flipper_features", force: :cascade do |t|
    t.string "key", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_flipper_features_on_key", unique: true
  end

  create_table "flipper_gates", force: :cascade do |t|
    t.string "feature_key", null: false
    t.string "key", null: false
    t.text "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feature_key", "key", "value"], name: "index_flipper_gates_on_feature_key_and_key_and_value", unique: true
  end

  create_table "funds", force: :cascade do |t|
    t.decimal "balance", precision: 7, scale: 2, default: "0.0"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.boolean "archived", default: false, null: false
    t.boolean "community", default: false, null: false
    t.text "description"
    t.index ["user_id"], name: "index_funds_on_user_id"
  end

  create_table "households", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item_categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kids", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.bigint "household_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["household_id"], name: "index_kids_on_household_id"
  end

  create_table "meal_payment_attendees", force: :cascade do |t|
    t.bigint "meal_id", null: false
    t.bigint "meal_payment_id", null: false
    t.string "attendee_type"
    t.bigint "attendee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attendee_type", "attendee_id"], name: "index_meal_payment_attendees_on_attendee"
    t.index ["meal_id"], name: "index_meal_payment_attendees_on_meal_id"
    t.index ["meal_payment_id"], name: "index_meal_payment_attendees_on_meal_payment_id"
  end

  create_table "meal_payments", force: :cascade do |t|
    t.bigint "meal_id", null: false
    t.bigint "user_id", null: false
    t.float "amount", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "state", limit: 2, default: 0, null: false
    t.bigint "meal_seating_id"
    t.boolean "take_out", default: false
    t.integer "guests", default: 0
    t.bigint "household_id"
    t.string "note"
    t.index ["household_id"], name: "index_meal_payments_on_household_id"
    t.index ["meal_id"], name: "index_meal_payments_on_meal_id"
    t.index ["meal_seating_id"], name: "index_meal_payments_on_meal_seating_id"
    t.index ["user_id"], name: "index_meal_payments_on_user_id"
  end

  create_table "meal_portion_choices", force: :cascade do |t|
    t.bigint "meal_portion_id", null: false
    t.integer "quantity", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "meal_payment_id", null: false
    t.bigint "meal_seating_choice_id"
    t.index ["meal_payment_id"], name: "index_meal_portion_choices_on_meal_payment_id"
    t.index ["meal_portion_id"], name: "index_meal_portion_choices_on_meal_portion_id"
    t.index ["meal_seating_choice_id"], name: "index_meal_portion_choices_on_meal_seating_choice_id"
  end

  create_table "meal_portions", force: :cascade do |t|
    t.bigint "meal_id", null: false
    t.string "description"
    t.float "amount", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["meal_id"], name: "index_meal_portions_on_meal_id"
  end

  create_table "meal_seating_attendees", force: :cascade do |t|
    t.string "attendee_type", null: false
    t.bigint "attendee_id", null: false
    t.bigint "meal_seating_choice_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attendee_type", "attendee_id"], name: "index_meal_seating_attendees_on_attendee"
    t.index ["meal_seating_choice_id"], name: "index_meal_seating_attendees_on_meal_seating_choice_id"
  end

  create_table "meal_seating_choices", force: :cascade do |t|
    t.bigint "meal_payment_id", null: false
    t.bigint "meal_seating_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "guests", default: 0
    t.index ["meal_payment_id"], name: "index_meal_seating_choices_on_meal_payment_id"
    t.index ["meal_seating_id"], name: "index_meal_seating_choices_on_meal_seating_id"
  end

  create_table "meal_seatings", force: :cascade do |t|
    t.bigint "meal_id", null: false
    t.datetime "meal_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["meal_id"], name: "index_meal_seatings_on_meal_id"
  end

  create_table "meal_team_members", force: :cascade do |t|
    t.bigint "meal_id", null: false
    t.bigint "user_id"
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["meal_id"], name: "index_meal_team_members_on_meal_id"
    t.index ["user_id"], name: "index_meal_team_members_on_user_id"
  end

  create_table "meals", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.date "meal_date"
    t.float "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id", null: false
    t.datetime "closing_reminded_at"
    t.boolean "closed", default: false
    t.index ["user_id"], name: "index_meals_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.decimal "price", precision: 8, scale: 2
    t.bigint "user_id", null: false
    t.bigint "item_category_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "photos"
    t.integer "price_option", default: 0, null: false
    t.index ["item_category_id"], name: "index_posts_on_item_category_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_roles_users_on_role_id"
    t.index ["user_id"], name: "index_roles_users_on_user_id"
  end

  create_table "room_bookings", force: :cascade do |t|
    t.date "start_date"
    t.date "end_date"
    t.text "description"
    t.bigint "user_id", null: false
    t.bigint "room_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "confirmed_at"
    t.integer "state", limit: 2, default: 0, null: false
    t.boolean "free", default: false
    t.index ["room_id"], name: "index_room_bookings_on_room_id"
    t.index ["user_id"], name: "index_room_bookings_on_user_id"
  end

  create_table "room_payments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "room_booking_id"
    t.string "description"
    t.decimal "amount", precision: 8, scale: 2
    t.integer "state", limit: 2, default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["room_booking_id"], name: "index_room_payments_on_room_booking_id"
    t.index ["user_id"], name: "index_room_payments_on_user_id"
  end

  create_table "rooms", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "slug"
    t.string "purpose"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "calendar_color"
  end

  create_table "settings", force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["var"], name: "index_settings_on_var", unique: true
  end

  create_table "solid_queue_blocked_executions", force: :cascade do |t|
    t.bigint "job_id", null: false
    t.string "queue_name", null: false
    t.integer "priority", default: 0, null: false
    t.string "concurrency_key", null: false
    t.datetime "expires_at", null: false
    t.datetime "created_at", null: false
    t.index ["concurrency_key", "priority", "job_id"], name: "index_solid_queue_blocked_executions_for_release"
    t.index ["expires_at", "concurrency_key"], name: "index_solid_queue_blocked_executions_for_maintenance"
    t.index ["job_id"], name: "index_solid_queue_blocked_executions_on_job_id", unique: true
  end

  create_table "solid_queue_claimed_executions", force: :cascade do |t|
    t.bigint "job_id", null: false
    t.bigint "process_id"
    t.datetime "created_at", null: false
    t.index ["job_id"], name: "index_solid_queue_claimed_executions_on_job_id", unique: true
    t.index ["process_id", "job_id"], name: "index_solid_queue_claimed_executions_on_process_id_and_job_id"
  end

  create_table "solid_queue_failed_executions", force: :cascade do |t|
    t.bigint "job_id", null: false
    t.text "error"
    t.datetime "created_at", null: false
    t.index ["job_id"], name: "index_solid_queue_failed_executions_on_job_id", unique: true
  end

  create_table "solid_queue_jobs", force: :cascade do |t|
    t.string "queue_name", null: false
    t.string "class_name", null: false
    t.text "arguments"
    t.integer "priority", default: 0, null: false
    t.string "active_job_id"
    t.datetime "scheduled_at"
    t.datetime "finished_at"
    t.string "concurrency_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["active_job_id"], name: "index_solid_queue_jobs_on_active_job_id"
    t.index ["class_name"], name: "index_solid_queue_jobs_on_class_name"
    t.index ["finished_at"], name: "index_solid_queue_jobs_on_finished_at"
    t.index ["queue_name", "finished_at"], name: "index_solid_queue_jobs_for_filtering"
    t.index ["scheduled_at", "finished_at"], name: "index_solid_queue_jobs_for_alerting"
  end

  create_table "solid_queue_pauses", force: :cascade do |t|
    t.string "queue_name", null: false
    t.datetime "created_at", null: false
    t.index ["queue_name"], name: "index_solid_queue_pauses_on_queue_name", unique: true
  end

  create_table "solid_queue_processes", force: :cascade do |t|
    t.string "kind", null: false
    t.datetime "last_heartbeat_at", null: false
    t.bigint "supervisor_id"
    t.integer "pid", null: false
    t.string "hostname"
    t.text "metadata"
    t.datetime "created_at", null: false
    t.string "name", null: false
    t.index ["last_heartbeat_at"], name: "index_solid_queue_processes_on_last_heartbeat_at"
    t.index ["name", "supervisor_id"], name: "index_solid_queue_processes_on_name_and_supervisor_id", unique: true
    t.index ["supervisor_id"], name: "index_solid_queue_processes_on_supervisor_id"
  end

  create_table "solid_queue_ready_executions", force: :cascade do |t|
    t.bigint "job_id", null: false
    t.string "queue_name", null: false
    t.integer "priority", default: 0, null: false
    t.datetime "created_at", null: false
    t.index ["job_id"], name: "index_solid_queue_ready_executions_on_job_id", unique: true
    t.index ["priority", "job_id"], name: "index_solid_queue_poll_all"
    t.index ["queue_name", "priority", "job_id"], name: "index_solid_queue_poll_by_queue"
  end

  create_table "solid_queue_recurring_executions", force: :cascade do |t|
    t.bigint "job_id", null: false
    t.string "task_key", null: false
    t.datetime "run_at", null: false
    t.datetime "created_at", null: false
    t.index ["job_id"], name: "index_solid_queue_recurring_executions_on_job_id", unique: true
    t.index ["task_key", "run_at"], name: "index_solid_queue_recurring_executions_on_task_key_and_run_at", unique: true
  end

  create_table "solid_queue_recurring_tasks", force: :cascade do |t|
    t.string "key", null: false
    t.string "schedule", null: false
    t.string "command", limit: 2048
    t.string "class_name"
    t.text "arguments"
    t.string "queue_name"
    t.integer "priority", default: 0
    t.boolean "static", default: true, null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_solid_queue_recurring_tasks_on_key", unique: true
    t.index ["static"], name: "index_solid_queue_recurring_tasks_on_static"
  end

  create_table "solid_queue_scheduled_executions", force: :cascade do |t|
    t.bigint "job_id", null: false
    t.string "queue_name", null: false
    t.integer "priority", default: 0, null: false
    t.datetime "scheduled_at", null: false
    t.datetime "created_at", null: false
    t.index ["job_id"], name: "index_solid_queue_scheduled_executions_on_job_id", unique: true
    t.index ["scheduled_at", "priority", "job_id"], name: "index_solid_queue_dispatch_all"
  end

  create_table "solid_queue_semaphores", force: :cascade do |t|
    t.string "key", null: false
    t.integer "value", default: 1, null: false
    t.datetime "expires_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["expires_at"], name: "index_solid_queue_semaphores_on_expires_at"
    t.index ["key", "value"], name: "index_solid_queue_semaphores_on_key_and_value"
    t.index ["key"], name: "index_solid_queue_semaphores_on_key", unique: true
  end

  create_table "transfers", force: :cascade do |t|
    t.bigint "sender_id"
    t.bigint "recipient_id"
    t.decimal "amount", precision: 7, scale: 2, default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "sender_new_balance", precision: 7, scale: 2
    t.decimal "recipient_new_balance", precision: 7, scale: 2, default: "0.0"
    t.integer "state", limit: 2, default: 0, null: false
    t.string "recipient_description"
    t.string "interac", limit: 20
    t.string "sender_description"
    t.index ["recipient_id"], name: "index_transfers_on_recipient_id"
    t.index ["sender_id"], name: "index_transfers_on_sender_id"
  end

  create_table "user_settings", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.boolean "eggs", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "meals", default: true
    t.boolean "reimburse_to_fund", default: false
    t.index ["user_id"], name: "index_user_settings_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone_number", limit: 12
    t.bigint "household_id"
    t.boolean "archived", default: false, null: false
    t.boolean "emeritus", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["household_id"], name: "index_users_on_household_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "area_bookings", "rooms"
  add_foreign_key "area_bookings", "users"
  add_foreign_key "car_booking_payments", "users"
  add_foreign_key "car_bookings", "cars"
  add_foreign_key "car_bookings", "users"
  add_foreign_key "cars", "users"
  add_foreign_key "egg_payments", "egg_orders"
  add_foreign_key "egg_payments", "users"
  add_foreign_key "eligibilities", "households"
  add_foreign_key "funds", "users"
  add_foreign_key "kids", "households"
  add_foreign_key "meal_payment_attendees", "meal_payments"
  add_foreign_key "meal_payment_attendees", "meals"
  add_foreign_key "meal_payments", "households"
  add_foreign_key "meal_payments", "meal_seatings"
  add_foreign_key "meal_payments", "meals"
  add_foreign_key "meal_payments", "users"
  add_foreign_key "meal_portion_choices", "meal_payments"
  add_foreign_key "meal_portion_choices", "meal_portions"
  add_foreign_key "meal_portion_choices", "meal_seating_choices"
  add_foreign_key "meal_portions", "meals"
  add_foreign_key "meal_seating_attendees", "meal_seating_choices"
  add_foreign_key "meal_seating_choices", "meal_payments"
  add_foreign_key "meal_seating_choices", "meal_seatings"
  add_foreign_key "meal_seatings", "meals"
  add_foreign_key "meal_team_members", "meals"
  add_foreign_key "meal_team_members", "users"
  add_foreign_key "meals", "users"
  add_foreign_key "posts", "item_categories"
  add_foreign_key "posts", "users"
  add_foreign_key "roles_users", "roles"
  add_foreign_key "roles_users", "users"
  add_foreign_key "room_bookings", "rooms"
  add_foreign_key "room_bookings", "users"
  add_foreign_key "room_payments", "room_bookings"
  add_foreign_key "room_payments", "users"
  add_foreign_key "solid_queue_blocked_executions", "solid_queue_jobs", column: "job_id", on_delete: :cascade
  add_foreign_key "solid_queue_claimed_executions", "solid_queue_jobs", column: "job_id", on_delete: :cascade
  add_foreign_key "solid_queue_failed_executions", "solid_queue_jobs", column: "job_id", on_delete: :cascade
  add_foreign_key "solid_queue_ready_executions", "solid_queue_jobs", column: "job_id", on_delete: :cascade
  add_foreign_key "solid_queue_recurring_executions", "solid_queue_jobs", column: "job_id", on_delete: :cascade
  add_foreign_key "solid_queue_scheduled_executions", "solid_queue_jobs", column: "job_id", on_delete: :cascade
  add_foreign_key "transfers", "funds", column: "recipient_id"
  add_foreign_key "transfers", "funds", column: "sender_id"
  add_foreign_key "user_settings", "users"
  add_foreign_key "users", "households"
end
