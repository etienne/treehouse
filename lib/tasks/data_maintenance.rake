# frozen_string_literal: true

namespace :data_maintenance do
  desc 'Imports email addresses of all eligible users'
  task import_eligibilities: :environment do
    filepath = ARGV[1]

    if filepath.blank?
      puts 'Error - Please provide a input file as argument, e.g:'
      puts '$ rake data_maintenance:import_eligibilities /path/to/file'
      exit
    end

    unless File.file?(filepath)
      puts "Error - '#{filepath}' does not exist."
      exit
    end

    File.foreach(filepath, chomp: true) do |email|
      Eligibility.create(email: email.downcase)
    end

    puts "Done. We now have a total of #{Eligibility.all.count} eligible users."
  end

  task meal_payments_migration: :environment do
    Meal.all.each do |meal|
      MealSeating.find_or_create_by!(
        meal: meal,
        meal_time: nil
      )
    end

    MealPayment.includes(:meal_seating, :meal_portion_choices, :meal_payment_attendees).find_each(batch_size: 200) do |meal_payment|
      next if meal_payment.meal_seating_choices.present?

      seating_to_use = if meal_payment.meal_seating.nil?
                         MealSeating.find_by(meal: meal_payment.meal, meal_time: nil)
                       else
                         meal_payment.meal_seating
                       end

      msc = MealSeatingChoice.create(
        meal_payment: meal_payment,
        meal_seating: seating_to_use,
        guests: meal_payment.guests
      )

      meal_payment.meal_portion_choices.each do |portion_choice|
        portion_choice.update(meal_seating_choice: msc)
      end

      meal_payment.meal_payment_attendees.each do |payment_attendee|
        MealSeatingAttendee.create(
          attendee: payment_attendee.attendee,
          meal_seating_choice: msc
        )
      end

      puts "Meal payment ##{meal_payment.id} OK"
    end

    puts 'All done'
  end
end
