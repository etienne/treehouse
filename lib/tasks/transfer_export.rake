# frozen_string_literal: true

require 'csv'

namespace :transfer_export do
  desc 'Exports transfers to CSV file'
  task execute: :environment do
    CSV.open('transfer_export.csv', 'w') do |csv|
      csv << ['Transfer ID', 'Date', 'Credit', 'Fund recipient name', 'Debit', 'Fund sender name']

      Transfer.where('sender_id IS NULL OR recipient_id IS NULL').order(id: :asc).each do |transfer|
        res = [transfer.id, transfer.created_at.strftime('%a %b %d')]
        if transfer.sender_id.present?
          res += ['', '', transfer.amount.round(2), transfer.sender.name ]
        elsif transfer.recipient_id.present?
          res += [transfer.amount.round(2), transfer.recipient.name, '', '']
        end

        csv << res
      end
    end
  end
end
