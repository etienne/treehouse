# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'
  get 'metadata', to: 'home#metadata'
  get 'links', to: 'home#links'

  resources :posts, path: '/marketplace'
  get 'posts/remove/:id', to: 'posts#destroy', as: 'post_remove'
  get 'posts/remove_photos/:id', to: 'posts#remove_photos', as: 'post_remove_photos'

  resources :cars
  get 'cars/remove/:id', to: 'cars#destroy', as: 'car_remove'

  resources :rooms
  resources :egg_orders, only: %i[edit update]

  get 'transfers/new', to: 'transfers#new', as: 'send_money'
  post 'transfers', to: 'transfers#create'

  get 'setup/routing', to: 'setup#routing'
  get 'setup/welcome', to: 'setup#welcome'
  get 'setup/app', to: 'setup#app'
  post 'setup/save_app', to: 'setup#save_app'
  get 'setup/allowlist', to: 'setup#allowlist'
  post 'setup/save_allowlist', to: 'setup#save_allowlist'
  get 'setup/admin', to: 'setup#admin'
  get 'setup/done', to: 'setup#done'
  post 'setup/save_done', to: 'setup#save_done'

  resources :funds
  get 'bank_statements', to: 'funds#statements'
  get 'funds/remove/:id', to: 'funds#destroy', as: 'fund_remove'
  get 'funds/close/:id', to: 'funds#close', as: 'fund_close'

  resources :meals
  get 'meals/:id/sign-up', to: 'meal_payments#new', as: 'meal_sign_up'
  post 'meals/:id/create-sign-up', to: 'meal_payments#create', as: 'create_meal_sign_up'
  get 'meals/:id/sign-up/:payment_id', to: 'meal_payments#edit', as: 'edit_meal_sign_up'
  post 'meals/:id/update-sign-up/:payment_id', to: 'meal_payments#update', as: 'update_meal_sign_up'
  get 'meals/:id/cancel-sign-up/:payment_id', to: 'meal_payments#destroy', as: 'cancel_meal_sign_up'
  get 'meals/:id/close', to: 'meals#close', as: 'close_meal'
  post 'meals/:id/confirm_close', to: 'meals#confirm_close', as: 'confirm_close_meal'
  get 'fetch_meals', to: 'meals#fetch_meals'
  get 'fetch_meal_attendees', to: 'meals#fetch_meal_attendees'

  namespace :bookings do
    resources :guests
    get 'guests/cancel/:id', to: 'guests#destroy', as: 'cancel_room_booking'
    get 'guests/confirm_booking/:id', to: 'guests#confirm_booking', as: 'confirm_room_booking'
    get 'fetch_bookings', to: 'guests#fetch_bookings'
    get 'prepare_new_room_booking', to: 'guests#prepare_new_room_booking'

    resources :cars
    get 'cars/cancel/:id', to: 'cars#destroy', as: 'cancel_car_booking'
    get 'fetch_car_bookings', to: 'cars#fetch_car_bookings'
    get 'prepare_new_car_booking', to: 'cars#prepare_new_car_booking'

    resources :areas
    get 'areas/cancel/:id', to: 'areas#destroy', as: 'cancel_area_booking'
    get 'fetch_area_bookings', to: 'areas#fetch_area_bookings'
    get 'prepare_new_area_booking', to: 'areas#prepare_new_area_booking'
  end

  namespace :admin do
    get 'guest_rooms'
    get 'mark_balance_as_paid'
    get 'mark_booking_as_confirmed'
    get 'money'
    get 'egg_orders'
    get 'car_booking_payments'
    get 'meals'
    get 'meals/:id', to: '/admin#meal', as: 'meal'
    get 'close_meal'
    get 'download_meals', defaults: { format: :csv }
    get 'area_bookings'
    get 'mark_egg_payment_as_paid'
    get 'mark_car_booking_payment_as_paid'
    get 'notify_about_egg_delivery'
    get 'mark_area_booking_as_confirmed'
    post 'add_money_to_fund'
  end

  get 'take_egg_order', to: 'egg_payments#take_order'

  get 'settings', to: 'user_settings#edit', as: 'user_settings'
  post 'save_user_settings', to: 'user_settings#save_user_settings'

  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions' }
  devise_scope :user do
    # Redirests signing out users back to sign-in
    get 'users', to: 'devise/sessions#new'
    get '/users/sign_out', to: 'devise/sessions#destroy'
  end
end
