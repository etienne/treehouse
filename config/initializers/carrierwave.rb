# frozen_string_literal: true

CarrierWave.configure do |config|
  config.fog_credentials = {
    provider: 'backblaze',
    b2_key_id: Rails.application.credentials.dig(:storage, :key_id),
    b2_key_token: Rails.application.credentials.dig(:storage, :application_key)
  }
  config.fog_directory = Rails.application.credentials.dig(:storage, :bucket)
  config.fog_public = true
end
