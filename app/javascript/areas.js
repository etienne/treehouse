import FullCalendar from 'fullcalendar.min';
import Rails from "@rails/ujs";

var exports = {};
var byId = function( id ) { return document.getElementById( id ); };
var byClass = function( cl ) { return document.getElementsByClassName( cl )[0]; };
var mobileMaxWidth = 767;

document.addEventListener('turbo:load', function() {
  var calendarEl = byId('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    height: 950,
    themeSystem: 'bootstrap5',
    initialView: 'timeGridFourDay',
    headerToolbar: {
      left: 'timeGridWeek,timeGridFourDay,timeGridDay',
      center: 'title',
      right: 'today,prev,next'
    },
    displayEventTime: false,
    allDaySlot: false,
    selectable: true,
    selectLongPressDelay: 500,
    slotEventOverlap: false,
    views: {
      timeGridFourDay: {
        type: 'timeGrid',
        duration: { days: 4 },
        buttonText: '4-day'
      }
    },
    eventClick: function(info) {
      var eventObj = info.event;

      byId("viewAreaBooked").textContent = eventObj.title;
      byId("viewArea").textContent = eventObj.extendedProps.areaname;
      byId("viewHost").textContent = eventObj.extendedProps.host;
      byId("viewLevel").textContent = eventObj.extendedProps.arealevel;
      byId("viewBookingDateTime").textContent = eventObj.extendedProps.period;
      byId("viewAreaNotes").textContent = eventObj.extendedProps.notes;

      var showLink = eventObj.extendedProps.show_link
      if (showLink != '') {
        byId("editDelete").style.display = 'block';
        byId("editDelete").href = showLink;
      } else {
        byId("editDelete").style.display = 'none';
        byId("editDelete").href = '#';
      }

      var showMessage = eventObj.extendedProps.show_message
      if (showMessage == true){
        byId("approvalCoHo").style.display = 'block'
      } else {
        byId("approvalCoHo").style.display = 'none'
      }

      var myModal = new bootstrap.Modal(byId("viewBookingModal"), {});
      myModal.show();
    },

    select: function(info) {
      byId('confirmAreaBooking').disabled = true;

      removeExistingNew();

      prepareNewAreaBooking(info.start, info.end);
    },

    events: [],
  });

  calendar.render();

  fetchAreaBookings();

  if (isMobile()) {
    byClass('fc-timeGridDay-button').click();
  }

  byClass("fc-prev-button").addEventListener("click", function(){ fetchAreaBookings(); });
  byClass("fc-next-button").addEventListener("click", function(){ fetchAreaBookings(); });

  byId("area_booking_start_time").addEventListener("focusout", function() { updateNewEvent(); })
  byId("area_booking_end_time").addEventListener("focusout", function() { updateNewEvent(); })

  // --------------------------------------------

  function prepareNewAreaBooking(startTime, endTime) {
    Rails.ajax({
      url: "/bookings/prepare_new_area_booking",
      type: "get",
      data: 'start_time='+startTime+'&end_time='+endTime,
      success: function(data) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;

        if (data.booking.error_message != '') {
          byId('roomErrorMessage').style.display = 'block';
          byId('confirmAreaBooking').disabled = true;
          byId('roomErrorMessage').textContent = data.booking.error_message
        } else {
          byId('confirmAreaBooking').disabled = hasRoomBeenChosen()
          byId('roomErrorMessage').style.display = 'none';

          byId('area_booking_room_id').addEventListener('change', function(){
            byId('confirmAreaBooking').disabled = hasRoomBeenChosen()
          });

          var newEvent = {
            start: startTime,
            end: endTime,
            title: 'New',
            state: 'new'
          }

          calendar.addEvent(newEvent);

          byId('roomErrorMessage').style.display = 'none';
          byId('roomBookingForm').style.display = 'block';
          byId("area_booking_start_time").value = convertToDateTimeLocalString(startTime);
          byId("area_booking_end_time").value = convertToDateTimeLocalString(endTime);

          var roomOptions = byId('area_booking_room_id').options;
          for (var i = 1; i < roomOptions.length; i++) {
            roomOptions[i].disabled = !data.booking.rooms[roomOptions[i].value];
          }
        }
      }
    })
  }

  function removeExistingNew() {
    var existingNew = calendar.getEvents().filter(event => event.extendedProps.state == 'new')[0]
    if (typeof existingNew != 'undefined') {
      existingNew.remove()
    }
  }

  function updateNewEvent() {
    removeExistingNew()

    var newStart = new Date(byId("area_booking_start_time").value)
    var newEnd = new Date(byId("area_booking_end_time").value)

    prepareNewAreaBooking(newStart, newEnd)
  }

  function isMobile() {
    return (screen.width <= mobileMaxWidth)
  }

  function fetchAreaBookings() {
    var startDate = calendar.view.activeStart;
    var endDate = calendar.view.activeEnd;

    Rails.ajax({
      url: "/bookings/fetch_area_bookings",
      type: "get",
      data: 'start_date='+startDate+'&end_date='+endDate,
      success: function(data) {
        for (let i = 0; i < data.bookings.length; i++) {
          var booking = data.bookings[i];
          var existingEventIds = calendar.getEvents().map(function(b){ return  b.extendedProps.booking_id })
          if (!existingEventIds.includes(booking.booking_id)) {
            calendar.addEvent(booking);
          }
        }
      }
    })
  }

  function convertToDateTimeLocalString(date) {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");

    return `${year}-${month}-${day}T${hours}:${minutes}`;
  }

  function hasRoomBeenChosen() {
    return byId('area_booking_room_id').value == '';
  }
});

// Workaround to fix pipeline asset bug
setTimeout(function(){
  var calToolbar = document.getElementsByClassName('fc-header-toolbar').length
  if (calToolbar == 0) {
    window.location.reload();
  }
}, 2000);
