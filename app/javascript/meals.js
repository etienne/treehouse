import FullCalendar from 'fullcalendar.min';
import Rails from "@rails/ujs";

var exports = {};
var byId = function( id ) { return document.getElementById( id ); };
var byClass = function( cl ) { return document.getElementsByClassName( cl )[0]; };
var mobileMaxWidth = 767;

document.addEventListener('turbo:load', function() {
  var calendarEl = byId('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    height: 950,
    themeSystem: 'bootstrap5',
    initialView: 'dayGridMonth',
    headerToolbar: {
      right: 'today,prev,next'
    },
    displayEventTime: false,
    allDaySlot: false,
    selectable: false,
    selectLongPressDelay: 500,
    slotEventOverlap: false,
    eventClick: function(info) {
      var eventObj = info.event;
      var eventProps = eventObj.extendedProps;

      byId("viewMeal").textContent = eventObj.title;
      byId("viewDateTime").textContent = eventProps.period;
      byId("viewPortions").textContent = eventProps.portions;
      byId("viewMealNotes").textContent = eventProps.description;
      byId("signUpMeal").href = eventProps.sign_up_link;
      byId("seeMealDetails").href = eventProps.meal_link;
      byId("updateRegistrationLink").href = eventProps.update_reg_link;

      if (eventProps.attending && !eventProps.past_meal) {
        byId('viewAlreadySignedUp').style.display = 'block'
        byId("signUpMeal").style.display = 'none';
      } else {
        byId('viewAlreadySignedUp').style.display = 'none'
        byId("signUpMeal").style.display = 'block';
      }

      if (eventProps.too_late) {
        byId("signUpMeal").style.display = 'none';
      }

      fetchMealAttendees(eventObj.extendedProps.meal_id)

      var myModal = new bootstrap.Modal(byId("viewMealModal"), {});

      myModal.show();
    },

    events: [],
  });

  calendar.render();

  fetchMeals();

  if (isMobile()) {
    byClass('fc-timeGridDay-button').click();
  }

  byClass("fc-prev-button").addEventListener("click", function(){ fetchMeals(); });
  byClass("fc-next-button").addEventListener("click", function(){ fetchMeals(); });

  // --------------------------------------------

  function isMobile() {
    return (screen.width <= mobileMaxWidth)
  }

  function fetchMeals() {
    var startDate = calendar.view.activeStart;
    var endDate = calendar.view.activeEnd;

    Rails.ajax({
      url: "/fetch_meals",
      type: "get",
      data: 'start_date='+startDate+'&end_date='+endDate,
      success: function(data) {
        for (let i = 0; i < data.meals.length; i++) {
          var meal = data.meals[i];
          var existingEventIds = calendar.getEvents().map(function(b){ return  b.extendedProps.meal_id })
          if (!existingEventIds.includes(meal.meal_id)) {
            calendar.addEvent(meal);
          }
        }
      }
    })
  }

  function fetchMealAttendees(meal_id) {
    byId("viewAttendees").textContent = ''
    Rails.ajax({
      url: "/fetch_meal_attendees",
      type: "get",
      data: 'meal_id='+meal_id,
      success: function(data) {
        for (let i = 0; i < data.meal_attendee_groups.length; i++) {
          var attendee_line = data.meal_attendee_groups[i];
          byId("viewAttendees").textContent = byId("viewAttendees").textContent + attendee_line + '\n';
        }
      }
    })
  }
});

// Workaround to fix pipeline asset bug
setTimeout(function(){
  var calToolbar = document.getElementsByClassName('fc-header-toolbar').length
  if (calToolbar == 0) {
    window.location.reload();
  }
}, 2000);
