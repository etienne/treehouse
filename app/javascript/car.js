import FullCalendar from 'fullcalendar.min';
import Rails from "@rails/ujs";

var exports = {};
var byId = function( id ) { return document.getElementById( id ); };
var byClass = function( cl ) { return document.getElementsByClassName( cl )[0]; };
var mobileMaxWidth = 767;

document.addEventListener('turbo:load', function() {
  var calendarEl = byId('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    height: 950,
    themeSystem: 'bootstrap5',
    initialView: 'timeGridFourDay',
    headerToolbar: {
      left: 'timeGridWeek,timeGridFourDay,timeGridDay',
      center: 'title',
      right: 'today,prev,next'
    },
    displayEventTime: false,
    allDaySlot: false,
    selectable: true,
    selectLongPressDelay: 500,
    slotEventOverlap: false,
    views: {
      timeGridFourDay: {
        type: 'timeGrid',
        duration: { days: 4 },
        buttonText: '4-day'
      }
    },
    eventClick: function(info) {
      var eventObj = info.event;

      if (eventObj.extendedProps.eventtype == 'booking') {
        byId("viewCar").textContent = eventObj.extendedProps.carname;
        byId("viewBookingDateTime").textContent = eventObj.extendedProps.period;
        byId("viewCarBooked").textContent = eventObj.extendedProps.viewcartitle;

        if (eventObj.extendedProps.electric == true) {
          byId("dataForElectric").style.display = 'block';

          byId("viewBookingExpectedDistance").textContent = eventObj.extendedProps.expected;
          byId("viewBookingChargingTime").textContent = eventObj.extendedProps.charging;
        } else {
          byId("dataForElectric").style.display = 'none';
        }

        byId("viewCarNotes").textContent = eventObj.extendedProps.note;
        byId("editDelete").href = eventObj.extendedProps.show_link;

        var myModal = new bootstrap.Modal(byId("viewCarBookingModal"), {});
        myModal.show();
      } else {
        byId("viewChargingCar").textContent = eventObj.extendedProps.carname;
        byId("viewPreferredDuration").textContent = eventObj.extendedProps.chargingduration;

        var myModal = new bootstrap.Modal(byId("viewCarChargingModal"), {});
        myModal.show();
      }
    },

    select: function(info) {
      byId('confirmCarBooking').disabled = (byId('car_booking_car_id').value == '');

      removeExistingNew();
      prepareNewCarBooking(info.start, info.end);
    },

    events: [],
  });

  calendar.render();

  fetchCarBookings();

  if (isMobile()) {
    byClass('fc-timeGridDay-button').click();
  }

  byClass("fc-prev-button").addEventListener("click", function(){ fetchCarBookings(); });
  byClass("fc-next-button").addEventListener("click", function(){ fetchCarBookings(); });

  byId("car_booking_start_time").addEventListener("focusout", function() { updateNewEvent(); })
  byId("car_booking_end_time").addEventListener("focusout", function() { updateNewEvent(); })

  // --------------------------------------------

  function prepareNewCarBooking(startTime, endTime) {
    Rails.ajax({
      url: "/bookings/prepare_new_car_booking",
      type: "get",
      data: 'start_time='+startTime+'&end_time='+endTime,
      success: function(data) {
        if (data.booking.error_message != '') {
          byId('carErrorMessage').style.display = 'block';
          byId('carBookingForm').style.display = 'none';
          byId('carErrorMessage').textContent = data.booking.error_message
        } else {
          document.body.scrollTop = document.documentElement.scrollTop = 0;

          byId('car_booking_car_id').addEventListener('change', function(){
            byId('confirmCarBooking').disabled = (byId('car_booking_car_id').value == '');
          });

          var newEvent = {
            start: startTime,
            end: endTime,
            title: 'New',
            state: 'new'
          }

          calendar.addEvent(newEvent);

          byId('carErrorMessage').style.display = 'none';
          byId('carBookingForm').style.display = 'block';
          byId("car_booking_start_time").value = convertToDateTimeLocalString(startTime);
          byId("car_booking_end_time").value = convertToDateTimeLocalString(endTime);

          var carOptions = byId('car_booking_car_id').options;
          for (var i = 1; i < carOptions.length; i++) {
            carOptions[i].disabled = !data.booking.cars[carOptions[i].value];
          }
        }
      }
    })
  }

  function removeExistingNew() {
    var existingNew = calendar.getEvents().filter(event => event.extendedProps.state == 'new')[0]
    if (typeof existingNew != 'undefined') {
      existingNew.remove()
    }
  }

  function updateNewEvent() {
    removeExistingNew()

    var newStart = new Date(byId("car_booking_start_time").value)
    var newEnd = new Date(byId("car_booking_end_time").value)

    prepareNewCarBooking(newStart, newEnd)
  }

  function isMobile() {
    return (screen.width <= mobileMaxWidth)
  }

  function removeTime(date = new Date()) {
    const offset = date.getTimezoneOffset();
    var adjustedDate = new Date(date.getTime() - (offset*60*1000));
    return adjustedDate.toISOString().split('T')[0];
  }

  function fetchCarBookings() {
    var startDate = calendar.view.activeStart;
    var endDate = calendar.view.activeEnd;

    Rails.ajax({
      url: "/bookings/fetch_car_bookings",
      type: "get",
      data: 'start_date='+startDate+'&end_date='+endDate,
      success: function(data) {
        for (let i = 0; i < data.bookings.length; i++) {
          var booking = data.bookings[i];
          var existingEventIds = calendar.getEvents().map(function(b){ return  b.extendedProps.booking_id })
          if (!existingEventIds.includes(booking.booking_id)) {
            calendar.addEvent(booking);
          }
        }
      }
    })
  }

  function convertToDateTimeLocalString(date) {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");

    return `${year}-${month}-${day}T${hours}:${minutes}`;
  }
});

// Workaround to fix pipeline asset bug
setTimeout(function(){
  var calToolbar = document.getElementsByClassName('fc-header-toolbar').length
  if (calToolbar == 0) {
    window.location.reload();
  }
}, 2000);
