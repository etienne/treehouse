var byId = function( id ) { return document.getElementById( id ); };

const addPortion = ()=> {
  const createButton = byId('addPortion');

  createButton.addEventListener('click', () => {
    const lastId = document.querySelector('#mealPortionContainer').lastElementChild.id
    const newId = parseInt(lastId, 10) + 1;

    const newFieldset = document
      .querySelector('#mealPortionContainer > fieldset')
      .outerHTML
      .replace(/0/g,newId)
      .replace(newId+'.'+newId, "0.0")

    document.querySelector('#mealPortionContainer').insertAdjacentHTML('beforeend', newFieldset);

    var lastField = document.querySelector('#mealPortionContainer').lastElementChild;
    lastField.querySelector('.description').value = '';
    lastField.querySelector('.description').disabled = false;
    lastField.querySelector('.cost').value = '';
    lastField.querySelector('.cost').disabled = false;
  });
}

const addSeating = ()=> {
  const createButton = byId('addSeating');

  createButton.addEventListener('click', () => {
    const lastId = document.querySelector('#mealSeatingContainer').lastElementChild.id
    const newId = parseInt(lastId, 10) + 1;

    const newFieldset = document
      .querySelector('#mealSeatingContainer > fieldset')
      .outerHTML
      .replace(/0/g,newId)

    document.querySelector('#mealSeatingContainer').insertAdjacentHTML('beforeend', newFieldset);
  });
}

document.addEventListener('turbo:load', function() {
  if (document.querySelector('#mealPortionContainer')) {
    addPortion()
  }

  if (document.querySelector('#mealSeatingContainer')) {
    addSeating()
  }
})
