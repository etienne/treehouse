var byId = function( id ) { return document.getElementById( id ); };

document.addEventListener('turbo:load', function() {
  const takeOutBox = byId('take_out');
  const form = document.querySelector('#newMealPaymentForm')

  takeOutBox.addEventListener('click', () => {
    if (form.seating.length > 1) {
      form.seating.forEach(function(radio) {
        radio.disabled = takeOutBox.checked
      });
    } else {
      form.seating.disabled = takeOutBox.checked;
    }
  })
})

