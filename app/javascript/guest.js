import FullCalendar from 'fullcalendar.min';
import Rails from "@rails/ujs";

var exports = {};
var byId = function( id ) { return document.getElementById( id ); };
var byClass = function( cl ) { return document.getElementsByClassName( cl )[0]; };

document.addEventListener('turbo:load', function() {
  var calendarEl = byId('calendar');
  var bookings = JSON.parse(byId('guest-booking').dataset.bookings);

  var calendar = new FullCalendar.Calendar(calendarEl, {
    height: 750,
    themeSystem: 'bootstrap5',
    initialView: 'dayGridMonth',
    headerToolbar: {
      right: 'today,prev,next'
    },
    displayEventTime: false,
    allDaySlot: true,
    selectable: true,
    selectLongPressDelay: 500,
    eventClick: function(info) {
      var eventObj = info.event;
      byId("viewRoomHost").textContent = eventObj.extendedProps.viewroomtitle;
      byId("viewRoomRoom").textContent = eventObj.extendedProps.roomname;
      byId("viewRoomDates").textContent = eventObj.extendedProps.datesummary;
      byId("viewRoomNotes").textContent = eventObj.extendedProps.notes;

      var showLink = eventObj.extendedProps.show_link
      if (showLink != '') {
        byId("editDelete").style.display = 'block';
        byId("editDelete").href = showLink;
      } else {
        byId("editDelete").style.display = 'none';
        byId("editDelete").href = '#';
      }

      var myModal = new bootstrap.Modal(byId("viewBookingModal"), {});
      myModal.show();
    },
    select: function(info) {
      byId('confirmRoomBooking').disabled = true;
      var endDate = new Date(info.end - 1);
      var startDate = new Date(info.start);

      var existingNew = calendar.getEvents().filter(event => event.extendedProps.state == 'new')[0]
      if (typeof existingNew != 'undefined') {
        existingNew.remove()
      }

      Rails.ajax({
        url: "/bookings/prepare_new_room_booking",
        type: "get",
        data: 'start_date='+startDate+'&end_date='+endDate,
        success: function(data) {
          byId("scrollToPoint").scrollIntoView();

          var newEvent = {
            start: startDate,
            end: endDate,
            title: 'New',
            state: 'new'
          }

          calendar.addEvent(newEvent);

          if (data.booking.error_message != '') {
            byId('roomErrorMessage').style.display = 'block';
            byId('roomBookingForm').style.display = 'none';
            byId('roomErrorMessage').textContent = data.booking.error_message
          } else {
            byId('room_id').addEventListener('change', function(){
              byId('confirmRoomBooking').disabled = (byId('room_id').value == '');
            });

            byId('roomErrorMessage').style.display = 'none';
            byId('roomBookingForm').style.display = 'block';
            byId("start_date").value = startDate;
            byId("end_date").value = endDate;
            byId("newRoomBookingSummary").textContent = data.booking.date_summary;

            var roomOptions = byId('room_id').options;
            for (var i = 1; i < roomOptions.length; i++) {
              roomOptions[i].disabled = !data.booking.rooms[roomOptions[i].value];
            }
          }
        }
      })
    },

    events: bookings,

  });

  calendar.render();

  byClass("fc-prev-button").addEventListener("click", function(){ fetchMonthBookings(); });
  byClass("fc-next-button").addEventListener("click", function(){ fetchMonthBookings(); });

  // --------------------------------------------

  function fetchMonthBookings() {
    var startDate = calendar.view.activeStart;
    var endDate = calendar.view.activeEnd;

    Rails.ajax({
      url: "/bookings/fetch_bookings",
      type: "get",
      data: 'start_date='+startDate+'&end_date='+endDate,
      success: function(data) {
        for (let i = 0; i < data.bookings.length; i++) {
          var booking = data.bookings[i];
          var existingEventIds = calendar.getEvents().map(function(b){ return  b.extendedProps.booking_id }) 
          if (!existingEventIds.includes(booking.booking_id)) {
            calendar.addEvent(booking);
          }
        }
      }
    })
  }
});

// Workaround to fix pipeline asset bug
setTimeout(function(){
  var calToolbar = document.getElementsByClassName('fc-header-toolbar').length
  if (calToolbar == 0) {
    window.location.reload();
  }
}, 2000);
