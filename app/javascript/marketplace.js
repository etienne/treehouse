var byId = function( id ) { return document.getElementById( id ); };

const formElement = byId('postForm');
const priceOptionElement = byId('post_price_option');

formElement.addEventListener('submit', customSubmitHandler);
priceOptionElement.addEventListener('change', function() {
  adjustPrice(this.selectedOptions[0]);
});

function customSubmitHandler(e) {
  e.preventDefault();

  var photoField = byId('post_photos');
  var photos = photoField.files;
  if (photos != null && photos.length > 0) {
    if (photos.length > 3) {
      photoField.setCustomValidity("You cannot upload more than 3 photos.");
      photoField.reportValidity();
      photoField.value = '';
      return false;
    } else {
      photoField.setCustomValidity('');
    }

    var submitButton = byId("submitPost");

    var message = 'Uploading photo now...';
    if (photos.length > 1) {
      message = 'Uploading photos now...';
    }

    submitButton.value = message;
    submitButton.disabled = true;
  }

  formElement.removeEventListener('submit', customSubmitHandler);
  formElement.submit();
}

function adjustPrice(option) {
  var nonPriceOption = (option.value > 1);
  byId('post_price').disabled = nonPriceOption;

  if (nonPriceOption) {
    byId('post_price').value = "0.00";
  }
}

document.addEventListener('turbo:load', function() {
  adjustPrice(priceOptionElement.selectedOptions[0]);
})
