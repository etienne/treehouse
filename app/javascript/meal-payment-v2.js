var byId = function( id ) { return document.getElementById( id ); };
var byClass = function( cl ) { return document.getElementsByClassName( cl ); };

document.addEventListener('turbo:load', function() {
  const takeOutBox = byId('take_out');
  const seatings = Array.from(byClass('seating-checkbox'));

  seatings.forEach(function(seating) {
    displayHideSeatingSection(seating);
    seating.addEventListener('click', () => { displayHideSeatingSection(seating); })
  })

  // --------------------------------------------

  function displayHideSeatingSection(seating) {
    var seatingSection = byId('seatingsection_' + seating.value);
    if (seating.checked) {
      seatingSection.style.display = 'block';
    } else {
      seatingSection.style.display = 'none';
    }
  }

  function disableFieldsFor(section) {
    Array.from(section.getElementsByTagName('input')).forEach(function (input) {
      input.disabled = true;
    });
  }

  function enableFieldsFor(section) {
    Array.from(section.getElementsByTagName('input')).forEach(function (input) {
      input.disabled = false;
    });
  }
})

