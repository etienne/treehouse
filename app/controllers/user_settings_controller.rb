# frozen_string_literal: true

class UserSettingsController < ApplicationController
  before_action :authenticate_user!
  before_action :user_settings

  SETTINGS = %i[eggs meals reimburse_to_fund].freeze

  def save_user_settings
    if user_settings.update(**setting_params)
      redirect_to user_settings_path, notice: 'Settings updated successfully.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def user_settings
    @user_settings ||= current_user.settings
  end

  def setting_params
    SETTINGS.each_with_object({}) do |key, result|
      result[key] = params[key] == 'true'
    end
  end
end
