# frozen_string_literal: true

class MealPaymentsController < ApplicationController
  before_action :authenticate_user!
  before_action :meal
  before_action :attendees, :portion_choices, only: %i[edit update]
  before_action :ensure_no_meal_payment_yet, only: %i[new create]
  before_action :ensure_household_user, only: %i[edit update destroy]
  before_action :meal_payment, only: %i[edit update destroy]
  before_action :ensure_not_too_late, only: %i[new create]

  def new
    @meal_payment = MealPayment.new
    @registration_data = MealRegistrationPresenter.new(params: {}, meal: meal) if meal_v2_enabled?
  end

  def create
    result = MealRegistrations::CreateService.new(
      meal: meal,
      user: current_user,
      params: params
    ).execute

    if result[:success]
      flash[:success] = 'You have been registered for this meal! Your payment went through as well.'

      redirect_to meal_path(meal)
    else
      @errors = result[:errors]
      @meal_payment = MealPayment.new
      @registration_data = MealRegistrationPresenter.new(params: params, meal: meal) if meal_v2_enabled?

      render 'new'
    end
  end

  def edit
    @registration_data = MealRegistrationObjectPresenter.new(meal_payment: meal_payment) if meal_v2_enabled?
  end

  def update
    result = MealRegistrations::UpdateService.new(
      meal_payment: @meal_payment,
      params: params
    ).execute

    if result[:success]
      flash[:success] = 'Your meal registration has now been updated.'

      redirect_to meal_path(meal)
    else
      @registration_data = MealRegistrationPresenter.new(params: params, meal: meal) if meal_v2_enabled?
      @errors = result[:errors]

      render 'edit'
    end
  end

  def destroy
    meal_payment.reimburse
    meal_payment.meal.unassign_household_from_team(household: current_user.household)

    meal_payment.destroy

    redirect_to meal_path(meal), notice: 'Your registration to this meal is now cancelled.'
  end

  private

  def meal_payment_params
    params.require(:meal_payment).permit(:user_id, :meal_id, :household_id)
  end

  def meal
    @meal ||= Meal.find(params[:id])
  end

  def meal_payment
    @meal_payment ||= MealPayment.find(params[:payment_id])
  end

  def ensure_household_user
    return if meal_payment.household_id == current_user.household_id

    flash[:error] = 'You cannot update this meal registration.'
    redirect_to meal_path(meal)
  end

  def ensure_no_meal_payment_yet
    household_users = current_user.household.users
    payments = MealPayment.where(meal: meal, user: household_users)
    return if payments.none?

    flash[:info] = 'You (or someone in your household) have already sign-up to this meal'

    redirect_to meal_path(meal)
  end

  def attendees
    @attendees ||= meal_payment.meal_payment_attendees.map(&:attendee)
  end

  def portion_choices
    @portion_choices ||= meal_payment.meal_portion_choices.each_with_object({}) do |choice, hash|
      hash[choice.meal_portion_id] = choice.quantity
    end
  end

  def ensure_not_too_late
    return unless meal.too_late_to_register?

    flash[:info] = 'It is now too late to register to this meal'
    redirect_to meal_path(meal)
  end

  def meal_v2_enabled?
    current_user.meal_v2_enabled?
  end
end
