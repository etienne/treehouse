# frozen_string_literal: true

module Bookings
  class GuestsController < ApplicationController
    before_action :authenticate_user!
    before_action :ensure_current_tve_member
    before_action :room_booking, only: %i[show edit update destroy confirm_booking]
    before_action :ensure_host, only: %i[edit destroy confirm_booking]

    def index
      @guest_rooms = Room.for_guests
      @this_month_bookings = bookings_for(
        Date.current.beginning_of_month,
        Date.current.end_of_month
      ).to_json
    end

    def new
      @room_booking = RoomBooking.new(
        start_date: params[:start_date],
        end_date: params[:end_date]
      )
    end

    def create
      booking = RoomBooking.new(room_booking_params)
      service = RoomBookings::CreateService.new(booking, user: current_user)

      @room_booking = service.execute

      if @room_booking.valid?
        flash[:success] = @room_booking.success_message
        redirect_to bookings_guests_path
      else
        render 'new'
      end
    end

    def update
      if @room_booking.update(room_booking_params)
        redirect_to bookings_guests_path, notice: 'Your guest room booking was successfully updated.'
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      start_date = room_booking.start_date.to_date
      end_date = room_booking.end_date.to_date
      booking_user_id = room_booking.user_id

      if room_booking.destroy
        if current_user.id == booking_user_id
          flash[:success] = "Your booking from #{start_date} to #{end_date} has now been cancelled."
          redirect_to bookings_guests_path
        elsif current_user.guest_room_coordinator?
          flash[:success] = "The booking from #{start_date} to #{end_date} has now been cancelled."
          redirect_to admin_guest_rooms_path
        end

      else
        flash[:error] = 'This booking could not be cancelled.'
        redirect_to bookings_guests_path
      end
    end

    def confirm_booking
      @room_booking.confirmed!

      redirect_to bookings_guests_path, notice: 'Your guest room booking is now confirmed.'
    end

    def fetch_bookings
      dates = formatted_start_and_end_dates

      render json: { bookings: bookings_for(dates[:start], dates[:end]) }
    end

    def prepare_new_room_booking
      booking_data = VerifyRoomBookingService.new(
        start_date: params[:start_date],
        end_date: params[:end_date]
      ).execute

      render json: { booking: booking_data }
    end

    private

    def bookings_for(start_date, end_date)
      return [] unless start_date && end_date

      RoomBooking.includes(user: :household)
                 .where('(end_date BETWEEN ? AND ?) OR (start_date BETWEEN ? AND ?)',
                        start_date, end_date, start_date, end_date)
                 .map do |b|
                   b.serialize_for_calendar_for(current_user)
                 end
    end

    def formatted_start_and_end_dates
      start_calendar_date = params[:start_date]
      end_calendar_date = params[:end_date]
      return { start: nil, end: nil } unless start_calendar_date && end_calendar_date

      begin
        { start: Date.parse(start_calendar_date), end: Date.parse(end_calendar_date) }
      rescue ArgumentError
        { start: nil, end: nil }
      end
    end

    def room_booking
      @room_booking ||= RoomBooking.find(params[:id])
    end

    def ensure_host
      return if current_user.id == @room_booking.user_id
      return if current_user.guest_room_coordinator?

      flash[:error] = 'You cannot edit or delete this booking.'
      redirect_to bookings_guest_path(@room_booking)
    end

    def room_booking_params
      params.require(:room_booking).permit(:start_date, :end_date, :room_id, :description)
    end
  end
end
