# frozen_string_literal: true

module Bookings
  class AreasController < ApplicationController
    before_action :authenticate_user!
    before_action :ensure_current_tve_member
    before_action :area_booking, only: %i[show edit update destroy]

    def index
      @areas = Room.ch_areas
    end

    def new
      @area_booking = AreaBooking.new(
        start_time: params[:start_time],
        end_time: params[:end_time]
      )
    end

    def create
      booking = AreaBooking.new(area_booking_params)
      service = AreaBookings::CreateService.new(booking, user: current_user)

      @area_booking = service.execute

      if @area_booking.valid?
        set_confirmation_message

        redirect_to bookings_areas_path
      else
        render 'new'
      end
    end

    def update
      if @area_booking.update(area_booking_params)
        redirect_to bookings_areas_path, notice: 'Your booking was updated successfully.'
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def fetch_area_bookings
      dates = formatted_start_and_end_dates

      render json: { bookings: bookings_for(dates[:start], dates[:end]) }
    end

    def prepare_new_area_booking
      booking_data = VerifyAreaBookingService.new(
        start_time: params[:start_time],
        end_time: params[:end_time]
      ).execute

      render json: { booking: booking_data }
    end

    def destroy
      booking_user_id = area_booking.user_id

      if area_booking.destroy
        if current_user.id == booking_user_id
          flash[:success] = 'Your CH area booking has now been cancelled.'
          redirect_to bookings_areas_path
        end
      else
        flash[:error] = 'This booking could not be cancelled.'
        redirect_to bookings_areas_path
      end
    end

    private

    def area_booking
      @area_booking ||= AreaBooking.find(params[:id])
    end

    def bookings_for(start_date, end_date)
      return [] unless start_date && end_date

      start_time = DateTime.parse(start_date.to_s).beginning_of_day
      end_time = DateTime.parse(end_date.to_s).end_of_day

      AreaBooking.includes(:user)
                 .where('(end_time BETWEEN ? AND ?) OR (start_time BETWEEN ? AND ?)',
                        start_time, end_time, start_time, end_time)
                 .map do |b|
                   b.serialize_for_calendar_for(current_user)
                 end.flatten
    end

    def formatted_start_and_end_dates
      start_calendar_date = params[:start_date]
      end_calendar_date = params[:end_date]
      return { start: nil, end: nil } unless start_calendar_date && end_calendar_date

      begin
        { start: Date.parse(start_calendar_date), end: Date.parse(end_calendar_date) }
      rescue ArgumentError
        { start: nil, end: nil }
      end
    end

    def area_booking_params
      area_params = params.require(:area_booking).permit(:start_time, :end_time, :room_id, :name, :description, :level)

      area_params['level'] = area_params['level'].to_i

      area_params
    end

    def set_confirmation_message
      if @area_booking.pending?
        flash[:info] = 'Your reservation has been saved but it requires approval from the CoHo circle. Please contact them.'
      else
        flash[:success] = 'Your CH area booking has been saved.'
      end
    end
  end
end
