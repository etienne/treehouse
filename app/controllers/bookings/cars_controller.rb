# frozen_string_literal: true

module Bookings
  class CarsController < ApplicationController
    before_action :authenticate_user!
    before_action :ensure_current_tve_member
    before_action :car_booking, only: %i[show edit update destroy]
    before_action :ensure_host, only: %i[edit destroy]
    before_action :past_booking, only: %i[edit update destroy]

    def index
      @cars = Car.all
    end

    def new
      @car_booking = CarBooking.new(
        start_time: params[:start_time],
        end_time: params[:end_time],
        expected_distance: 10
      )
    end

    def create
      @car_booking = CarBooking.new(car_booking_params)
      @car_booking.user = current_user

      if @car_booking.save
        flash[:success] = 'Your car booking has been saved.'

        redirect_to bookings_cars_path
      else
        render 'new'
      end
    end

    def update
      if @car_booking.update(car_booking_params)
        redirect_to bookings_cars_path, notice: 'Your car booking was updated successfully.'
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      booking_user_id = car_booking.user_id

      if !car_booking.past_or_in_progress_booking? && car_booking.destroy
        if current_user.id == booking_user_id
          flash[:success] = 'Your car booking has now been cancelled.'
          redirect_to bookings_cars_path
        end
      else
        flash[:error] = 'This booking could not be cancelled.'
        redirect_to bookings_cars_path
      end
    end

    def fetch_car_bookings
      dates = formatted_start_and_end_dates

      render json: { bookings: bookings_for(dates[:start], dates[:end]) }
    end

    def prepare_new_car_booking
      booking_data = VerifyCarBookingService.new(
        start_time: params[:start_time],
        end_time: params[:end_time]
      ).execute

      render json: { booking: booking_data }
    end

    private

    def bookings_for(start_date, end_date)
      return [] unless start_date && end_date

      start_time = DateTime.parse(start_date.to_s).beginning_of_day
      end_time = DateTime.parse(end_date.to_s).end_of_day

      CarBooking.includes(:user)
                .where('(end_time BETWEEN ? AND ?) OR (start_time BETWEEN ? AND ?)',
                       start_time, end_time, start_time, end_time)
                .map do |b|
                  b.serialize_for_calendar_for(current_user)
                end.flatten
    end

    def formatted_start_and_end_dates
      start_calendar_date = params[:start_date]
      end_calendar_date = params[:end_date]
      return { start: nil, end: nil } unless start_calendar_date && end_calendar_date

      begin
        { start: Date.parse(start_calendar_date), end: Date.parse(end_calendar_date) }
      rescue ArgumentError
        { start: nil, end: nil }
      end
    end

    def car_booking_params
      params.require(:car_booking).permit(:start_time, :end_time, :car_id, :note, :expected_distance)
    end

    def car_booking
      @car_booking ||= CarBooking.find(params[:id])
    end

    def ensure_host
      return if current_user.id == @car_booking.user_id

      flash[:error] = 'You cannot edit or delete this booking.'
      redirect_to bookings_car_path(@car_booking)
    end

    def past_booking
      return unless @car_booking.past_booking?

      flash[:error] = 'You cannot update or cancel a past car booking'
      redirect_to bookings_car_path(@car_booking)
    end
  end
end
