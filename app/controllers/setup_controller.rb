# frozen_string_literal: true

class SetupController < ApplicationController
  before_action :routing

  def welcome; end

  def app; end

  def save_app
    Setting.app_name = params[:name]
    Setting.app_color = params[:color]

    redirect_to new_registration_path(:user)
  end

  def admin; end

  def allowlist; end

  def save_allowlist
    addresses = params[:eligibilities].split("\r\n").map { |e| e.downcase.gsub(/[ ,;]/, '') }.uniq.compact
    addresses.each { |a| Eligibility.create(email: a) }

    redirect_to setup_done_path
  end

  def done; end

  def save_done
    Setting.setup_done = true

    redirect_to root_path
  end

  private

  def check_setup
    nil
  end

  def routing
    return unless Setting.setup_done?

    redirect_to root_path
  end

end
