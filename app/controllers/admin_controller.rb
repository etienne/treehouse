# frozen_string_literal: true

class AdminController < ApplicationController
  require 'csv'

  before_action :authenticate_user!
  before_action :ensure_guest_rooms_coordinator, only: %i[guest_rooms mark_balance_as_paid]
  before_action :ensure_area_booking_coordinator, only: :area_bookings
  before_action :ensure_fund_admin, only: %i[money add_money_to_fund]
  before_action :ensure_egg_admin, only: %i[egg_orders mark_egg_payment_as_paid notify_about_egg_delivery]
  before_action :ensure_meal_admin, only: %i[meals meal close_meal download_meals]

  def guest_rooms
    @unconfirmed_bookings = RoomBooking.with_pending_confirmation.close_to_start_date
    @bookings_with_payments = RoomBooking.includes(:room_payment).with_payment_required
  end

  def mark_balance_as_paid
    booking = RoomBooking.includes(:room_payment).find_by(id: params[:id])

    if booking
      payment = booking.room_payment
      if booking.payment_required? && booking.confirmed! && payment.pay_now!
        formatted_amount = format('%.2f', payment.amount)
        flash[:success] = "This booking (balance of $#{formatted_amount}) has been marked as paid."
      end
    end

    redirect_to admin_guest_rooms_path
  end

  def mark_booking_as_confirmed
    booking = RoomBooking.find_by(id: params[:id])

    if booking && booking.confirmed_at.nil?
      booking.update!(confirmed_at: Date.current)
      flash[:success] = 'This booking has now been confirmed.'
    end

    redirect_to admin_guest_rooms_path
  end

  def meals
    @is_showing_closed_list = params[:closed] == '1'
    meals_to_show = @is_showing_closed_list ? Meal.closed : Meal.opened

    @recent_meals = meals_to_show.includes(:meal_payments).order(meal_date: :desc)
  end

  def meal
    @meal = Meal.find(params[:id])
    @choices = MealPortionChoice.includes(:meal_portion).where(meal_portion: { meal_id: @meal.id })
  end

  def close_meal
    meal = Meal.find(params[:id])

    result = CloseMealService.new(meal: meal).execute

    if result[:success]
      flash[:success] = 'The fund has been debitted. This meal is now closed.'
    else
      flash[:error] = 'Something went wrong. Please try again later.'
    end

    redirect_to admin_meal_path(meal)
  end

  def download_meals
    respond_to do |format|
      format.csv do
        filename = ['Closed meals', Date.today].join(' ')
        send_data Meal.closed_to_csv, filename: filename, content_type: 'text/csv'
      end
    end
  end

  def money; end

  def add_money_to_fund
    amount = params[:amount].to_f
    result = MoneyTransferService.new(
      amount: amount,
      recipient_fund: fund_to_credit,
      recipient_description: params[:recipient_description].presence,
      interac: params[:interac]
    ).add_money_to_fund

    if result[:success]
      unless fund_to_credit.community?
        FundMailer.notify_recipient(
          recipient: fund_to_credit.user,
          amount: amount
        ).deliver_later
      end

      flash[:success] = "$#{params[:amount]} was credited to #{recipient_name}."
    else
      flash[:error] = result[:error]
    end

    redirect_to admin_money_path
  end

  def egg_orders
    orders = EggOrder.includes(egg_payments: :user).order(created_at: :desc)
    @current_archive_threshold = Date.current - 3.days

    @next_egg_orders = orders.where('purchase_date >= ?', @current_archive_threshold)
    @past_egg_orders = orders.where('purchase_date < ?', @current_archive_threshold).first(10)

    @revenue_for_three_last_months = EggOrder.revenue_for_three_last_months
  end

  def mark_egg_payment_as_paid
    payment = EggPayment.find_by(id: params[:id].to_i)

    if payment&.paid!
      flash[:success] = "This payment from #{payment.user.first_name} has now been marked as paid."
    else
      flash[:error] = 'Sorry, something went wrong when marking this payment as paid. Please try again later.'
    end

    redirect_to admin_egg_orders_path
  end

  def car_booking_payments
    @pending_payments = CarBookingPayment.includes(:user).pending.order(created_at: :desc)
  end

  def mark_car_booking_payment_as_paid
    payment = CarBookingPayment.find_by(id: params[:id].to_i)

    if payment&.paid!
      flash[:success] = "This payment from #{payment.user.first_name} has now been marked as paid."
    else
      flash[:error] = 'Sorry, something went wrong when marking this payment as paid. Please try again later.'
    end

    redirect_to admin_car_booking_payments_path
  end

  def notify_about_egg_delivery
    egg_order = EggOrder.find_by(id: params[:id].to_i)

    if egg_order
      EggOrderNotifyPurchasersService.new(egg_order: egg_order).execute

      flash[:success] = "Emails sent about the #{egg_order.purchase_date} delivery."
    else
      flash[:error] = 'Emails not sent, something went wrong. Please try again later.'
    end

    redirect_to admin_egg_orders_path
  end

  def area_bookings
    @unconfirmed_bookings = AreaBooking.with_pending_confirmation
  end

  def mark_area_booking_as_confirmed
    booking = AreaBooking.find_by(id: params[:id])

    if booking&.pending?
      booking.confirmed!
      flash[:success] = 'This CH area booking has now been confirmed.'
    end

    redirect_to admin_area_bookings_path
  end

  private

  def fund_to_credit
    @fund_to_credit ||= Fund.find_by(id: params[:fund_id])
  end

  def recipient_name
    return "the #{fund_to_credit.name} fund" if fund_to_credit.community?

    "#{fund_to_credit.name}'s fund"
  end

  def ensure_fund_admin
    return if current_user.fund_admin?

    flash[:error] = 'You cannot manage funds.'
    redirect_to root_path
  end

  def ensure_guest_rooms_coordinator
    return if current_user.guest_room_coordinator?

    flash[:error] = 'You cannot coordinate guest rooms.'
    redirect_to root_path
  end

  def ensure_area_booking_coordinator
    return if current_user.area_booking_coordinator?

    flash[:error] = 'You cannot coordinate area bookings.'
    redirect_to root_path
  end

  def ensure_egg_admin
    return if current_user.egg_admin?

    flash[:error] = 'You cannot manage egg orders.'
    redirect_to root_path
  end

  def ensure_meal_admin
    return if current_user.common_meal_admin?

    flash[:error] = 'You cannot manage meals'
    redirect_to root_path
  end
end
