# frozen_string_literal: true

class CarsController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_current_tve_member
  before_action :car, only: %i[show edit update destroy]
  before_action :ensure_car_admin, only: %i[new create edit update destroy]

  def index
    @cars = Car.all
  end

  def new
    @car = Car.new
  end

  def create
    @car = Car.new(car_params)

    if @car.save
      redirect_to @car, notice: "The #{@car.title} was added successfully."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @car.update(car_params)
      redirect_to @car, notice: "The #{@car.title} was updated successfully."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @car.destroy

    redirect_to cars_url, notice: 'This car was successfully removed, and related car bookings were also deleted.'
  end

  private

  def ensure_car_admin
    return if current_user.car_admin?

    flash[:error] = 'You cannot add a car or edit an existing one.'
    redirect_to cars_path
  end

  def car
    @car ||= Car.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def car_params
    params.require(:car).permit(:title, :description, :calendar_color, :electric, :user_id,
                                :odometer, { photos: [] }, { documents: [] })
  end
end
