# frozen_string_literal: true

class FundsController < ApplicationController
  before_action :authenticate_user!
  before_action :page_number
  before_action :fund, only: %i[show edit]
  before_action :ensure_fund_viewable, only: :show
  before_action :ensure_fund_editable, only: %i[edit update destroy close]
  before_action :ensure_fund_active, only: %i[edit update destroy close]

  def index
    @fund = current_user.fund
    @community_funds = Fund.active.community.order(name: :asc)
    @archived_community_funds = Fund.community.order(name: :asc) - @community_funds
    @personal_funds_total = Fund.active.personal.sum(&:balance)
    @community_funds_total = @community_funds.sum(&:balance)
  end

  def show; end

  def new
    @fund = Fund.new
  end

  def create
    @fund = Fund.new(fund_params)
    @fund.user = current_user
    @fund.community = true

    if @fund.save
      redirect_to @fund, notice: "The \"#{@fund.name}\" fund was created successfully."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit; end

  def update
    if @fund.update(fund_params)
      redirect_to @fund, notice: "The \"#{@fund.name}\" fund was updated successfully."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    ensure_fund_destroyable

    name = fund.name
    fund.destroy

    redirect_to funds_url, notice: "The \"#{name}\" fund was successfully deleted."
  end

  def close
    result = CloseFundService.new(fund: fund).execute

    if result[:success]
      flash[:success] = "The \"#{fund.name}\" fund is now closed."

      redirect_to funds_path
    else
      flash[:error] = result[:error]

      redirect_to fund_path(fund)
    end
  end

  def statements; end

  private

  def fund
    @fund ||= Fund.find(params[:id])
  end

  def ensure_fund_destroyable
    return if fund.transaction_presenters.empty?

    flash[:error] = 'You cannot delete a fund tied to past transactions'
    redirect_to fund_path(fund)
  end

  def ensure_fund_viewable
    return if fund.user == current_user
    return if fund.community?

    flash[:error] = 'You cannot view this fund'
    redirect_to funds_path
  end

  def ensure_fund_editable
    return if fund.user == current_user

    flash[:error] = 'You cannot update this fund'
    redirect_to funds_path
  end

  def ensure_fund_active
    return unless fund.archived?

    flash[:error] = 'You cannot update a closed fund'
    redirect_to funds_path
  end

  def page_number
    @page_number = params[:page] || 1
  end

  def fund_params
    params.require(:fund).permit(:name, :description)
  end
end
