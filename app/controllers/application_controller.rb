# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :check_request_header
  before_action :check_archived
  before_action :check_setup

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  private

  def check_setup
    return if Setting.setup_done?
    return if instance_of? Users::RegistrationsController

    redirect_to setup_welcome_path
  end

  def check_request_header
    @maintenance_is_on = request.headers.fetch('Xmaintenance', nil).present?
  end

  def check_archived
    sign_out current_user if current_user&.archived?
  end

  def ensure_current_tve_member
    return unless current_user.emeritus?

    redirect_to root_path
  end

  def record_not_found
    render file: "#{Rails.root}/public/404.html", layout: false, status: 404
  end
end
