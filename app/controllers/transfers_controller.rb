# frozen_string_literal: true

class TransfersController < ApplicationController
  before_action :authenticate_user!

  def new
    @transfer = Transfer.new
    @fund = Fund.personal.find_by(user: current_user)
  end

  def create
    amount = transfer_params[:amount].to_f

    result = MoneyTransferService.new(
      amount: amount,
      sender_fund: sender_fund,
      recipient_fund: fund_to_credit,
      sender_description: description,
      recipient_description: description
    ).send_money

    if result[:success]
      unless fund_to_credit.community?
        FundMailer.notify_recipient(
          sender: current_user,
          recipient: fund_to_credit.user,
          amount: amount
        ).deliver_later
      end

      flash[:success] = "$#{transfer_params[:amount]} sent to #{fund_to_credit.name}."
    else
      flash[:error] = result[:error]
    end

    redirect_to send_money_path
  end

  private

  def transfer_params
    params.require(:transfer).permit(:sender_id, :recipient_id, :amount, :recipient_description)
  end

  def sender_fund
    return current_user.fund unless transfer_params[:sender_id].present?

    Fund.find(transfer_params[:sender_id])
  end

  def fund_to_credit
    @fund_to_credit ||= Fund.find_by(id: transfer_params[:recipient_id])
  end

  def description
    transfer_params[:recipient_description]
  end
end
