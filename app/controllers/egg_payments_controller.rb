# frozen_string_literal: true

class EggPaymentsController < ApplicationController
  before_action :authenticate_user!

  def take_order
    result = EggPaymentService.new(
      user: current_user,
      number_of_boxes: params[:number].to_i,
      egg_order_id: params[:order_id].to_i
    ).execute

    if result[:success]
      flash[:success] = success_message(paid: result[:paid])
    else
      flash[:error] = result[:error]
    end

    redirect_to root_path
  end

  private

  def success_message(paid:)
    if paid
      'Your egg order has been registered. Your personal fund has been debitted accordingly.'
    else
      'Your egg order has been registered. Please eTransfer money to Becky or give her cash.'
    end
  end
end
