# frozen_string_literal: true

class MealsController < ApplicationController
  before_action :authenticate_user!
  before_action :meal, only: %i[show edit update close confirm_close]
  before_action :ensure_organizer, only: %i[edit update close confirm_close]
  before_action :ensure_closing_date, only: %i[close confirm_close]
  before_action :ensure_not_already_closed, only: %i[close confirm_close edit update]

  def index
    @upcoming_meals = Meal.upcoming
  end

  def show
    @attendees_by_seating = meal.detailed_meal_attendees(breakdown: params[:expand] == '1')
    @meal_payments = MealPayment.includes(:user).where(meal: meal)
  end

  def new
    @meal = Meal.new
    @meal.meal_portions.build
    @meal.meal_seatings.build
  end

  def create
    @meal = Meal.new(meal_params)
    @meal.user = current_user if current_user.alone_in_household?
    @meal.meal_portions = clean_up_empty_portions
    @meal.meal_seatings = prepare_seatings

    if @meal.save
      MealTeamService.new(meal: meal).execute
      AreaBookings::CreateService.execute_for_meal(meal: meal, user: current_user)

      flash[:success] = meal_save_success_message

      redirect_to meal_path(@meal)
    else
      render 'new'
    end
  end

  def edit
    @meal = Meal.find(params[:id])
    @meal.meal_portions.build unless @meal.meal_portions
    @meal.meal_seatings.build unless @meal.meal_seatings
  end

  def update
    if @meal.update(meal_params)
      redirect_to meal_path(@meal), notice: 'Meal updated successfully.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def close
    @meal_payments = meal.meal_payments
  end

  def confirm_close
    result = CloseMealService.new(meal: meal).execute

    if result[:success]
      flash[:success] = 'Your meal is now closed. Thanks!'
    else
      flash[:error] = result[:error]
    end

    redirect_to meal_path(meal)
  end

  def fetch_meals
    dates = formatted_start_and_end_dates

    render json: { meals: meals_for(dates[:start], dates[:end]) }
  end

  def fetch_meal_attendees
    meal = Meal.find_by(id: params[:meal_id])

    if current_user.meal_v2_enabled?
      render json: { meal_attendee_groups: meal&.simple_meal_attendees_v2 || [] }
    else
      render json: { meal_attendee_groups: meal&.simple_meal_attendees || [] }
    end
  end

  private

  def meals_for(start_date, end_date)
    return [] unless start_date && end_date

    Meal
      .includes(:meal_seatings, :meal_portions)
      .where('(meal_date BETWEEN ? AND ?)', start_date, end_date)
      .map do |m|
        m.serialize_for_calendar_for(current_user)
      end
  end

  def formatted_start_and_end_dates
    start_calendar_date = params[:start_date]
    end_calendar_date = params[:end_date]
    return { start: nil, end: nil } unless start_calendar_date && end_calendar_date

    begin
      { start: Date.parse(start_calendar_date), end: Date.parse(end_calendar_date) }
    rescue ArgumentError
      { start: nil, end: nil }
    end
  end

  def ensure_organizer
    return if current_user.admin_for_meal?(meal)

    flash[:error] = 'You cannot organize or edit a meal.'
    redirect_to root_path
  end

  def ensure_closing_date
    return if meal.meal_date < Date.current

    flash[:error] = 'You cannot close this meal before it happened.'
    redirect_to meal_path(meal)
  end

  def ensure_not_already_closed
    return unless meal.closed?

    flash[:error] = 'This meal cannot be updated, it is already closed.'
    redirect_to meal_path(meal)
  end

  def meal
    @meal ||= Meal.find_by(id: params[:id])
  end

  def meal_save_success_message
    message = 'Common meal event created successfully!'
    message = "#{message} Please don\'t forget to update it with its final cost later." if @meal.cost.blank?

    message
  end

  def meal_params
    params.require(:meal)
          .permit(:meal_date, :title, :description, :cost, :user_id,
                  :meal_portions_attributes => %i[id description amount],
                  :meal_seatings_attributes => %i[id meal_time])
  end

  def clean_up_empty_portions
    @meal.meal_portions.reject { |p| p.description.blank? || p.amount.blank? || !p.amount.positive? }
  end

  def prepare_seatings
    @meal.meal_seatings.reject { |p| p.meal_time.blank? }

    @meal.meal_seatings.map do |s|
      mt = s.meal_time
      s.meal_time = meal.meal_date + mt.hour.hours + mt.min.minutes + mt.sec.seconds

      s
    end

    @meal.meal_seatings << MealSeating.new(meal_time: nil)
  end
end
