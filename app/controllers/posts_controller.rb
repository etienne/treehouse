# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :post, only: %i[show edit update remove_photos destroy]
  before_action :ensure_poster, only: %i[edit update remove_photos destroy]

  def index
    @total_count = post_search_results[:total_count]
    @posts = post_search_results[:posts]
  end

  def new
    @post = Post.new(price: 0)
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user

    if @post.save
      redirect_to @post, notice: 'Your post was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Your post was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def remove_photos
    post.remove_photos!
    post.save!

    redirect_to edit_post_path(post), notice: 'All photos have been removed from this post.'
  end

  def destroy
    @post.destroy

    redirect_to posts_url, notice: 'Your post was successfully deleted.'
  end

  private

  def ensure_poster
    return unless current_user.id != @post.user_id

    flash[:error] = 'You cannot edit or delete this post.'
    redirect_to post_path(@post)
  end

  def post_search_results
    PostSearchService.new(params).execute
  end

  def post
    @post ||= Post.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def post_params
    p_params = params.require(:post).permit(
      :title,
      :description,
      :price,
      :price_option,
      :user_id,
      :item_category_id,
      { photos: [] }
    )
    p_params[:price_option] = p_params[:price_option].to_i

    p_params
  end
end
