# frozen_string_literal: true

class HomeController < ApplicationController
  before_action :authenticate_user!, except: [:metadata]

  NUMBER_OF_RESULTS_PER_PAGE = 10

  def index
    page_number = params[:page] || 1

    @household = current_user.household
    @upcoming_room_bookings = current_user.room_bookings.includes(:room).upcoming
    @upcoming_area_bookings = current_user.area_bookings.includes(:room).upcoming
    @upcoming_car_bookings = current_user.car_bookings.includes(:car).upcoming
    @pending_car_booking_payments = current_user.car_booking_payments.pending
    @upcoming_meals = current_user.upcoming_meals
    @posts = current_user.posts.page(page_number).per(NUMBER_OF_RESULTS_PER_PAGE)
    @egg_payments = current_user.egg_payments.upcoming_deliveries
  end

  def links; end

  def metadata
    render json: { revision: short_sha }
  end

  private

  def short_sha
    if Rails.env.production?
      File.read(Rails.root.join('REVISION')).strip.freeze[0..6]
    else
      `git rev-parse --short HEAD | tr -d '\n'`
    end
  end
end
