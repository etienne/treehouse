# frozen_string_literal: true

class RoomsController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_current_tve_member
  before_action :room, only: %i[show edit update destroy]
  before_action :ensure_room_admin, only: %i[new create edit update destroy]

  def index
    @rooms = Room.all
  end

  def update
    if @room.update(room_params)
      redirect_to @room, notice: "The '#{@room.name}' room was updated successfully."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def ensure_room_admin
    return if current_user.guest_room_coordinator?

    flash[:error] = 'You cannot add a room or edit an existing one.'
    redirect_to rooms_path
  end

  def room
    @room ||= Room.find(params[:id])
  end

  def room_params
    params.require(:room).permit(:name, :description, :calendar_color)
  end
end
