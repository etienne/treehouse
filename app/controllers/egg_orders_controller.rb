# frozen_string_literal: true

class EggOrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_egg_admin
  before_action :egg_order

  def edit; end

  def update
    if @egg_order.update(egg_order_params)
      redirect_to admin_egg_orders_path, notice: 'Egg order updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def egg_order
    @egg_order ||= EggOrder.find(params[:id])
  end

  def ensure_egg_admin
    return if current_user.egg_admin?

    flash[:error] = 'You cannot edit egg orders.'
    redirect_to root_path
  end

  def egg_order_params
    params.required(:egg_order).permit(:max_boxes)
  end
end
