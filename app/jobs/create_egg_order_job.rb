# frozen_string_literal: true

class CreateEggOrderJob < ActiveJob::Base
  queue_as :default

  def perform
    CreateNextWeekEggOrdersService.new.execute
  end
end
