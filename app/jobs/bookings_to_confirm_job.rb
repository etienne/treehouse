# frozen_string_literal: true

class BookingsToConfirmJob < ActiveJob::Base
  queue_as :default

  def perform
    RoomBooking.pending.confirmable_today.each do |booking|
      RoomBookingMailer.confirmation(booking).deliver_later
    end
  end
end
