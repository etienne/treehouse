# frozen_string_literal: true

class MealNotificationJob < ActiveJob::Base
  queue_as :default

  def perform(meal)
    User.includes(:user_setting).active.each do |user|
      next unless user.settings.meals?

      MealMailer.notify_members(meal: meal, user: user).deliver_later
    end
  end
end
