# frozen_string_literal: true

class BookingReminderJob < ActiveJob::Base
  queue_as :default

  def perform
    RoomBooking.to_be_reminded_about_today.each do |booking|
      RoomBookingMailer.last_reminder(booking).deliver_later
    end
  end
end
