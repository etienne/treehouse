# frozen_string_literal: true

class CreateCarBookingPaymentJob < ActiveJob::Base
  queue_as :default

  def perform
    end_of_week_last_week = Date.current.beginning_of_week.yesterday
    beginning_of_week_last_week = end_of_week_last_week.beginning_of_week

    users_to_notify = CarBooking
                      .includes(:user)
                      .for_period(beginning_of_week_last_week, end_of_week_last_week)
                      .map(&:user).uniq

    users_to_notify.each do |user|
      next if exempted_from_car_payments?(user)

      result = CreateCarBookingPaymentService.new(
        user: user,
        period_start_date: beginning_of_week_last_week,
        period_end_date: end_of_week_last_week
      ).execute

      next unless result[:success] && result[:payment]

      CarBookingMailer.send_car_booking_usage_summary(
        user: user,
        booking_payment: result[:payment]
      ).deliver_later
    end
  end

  private

  def exempted_from_car_payments?(user)
    exempted_user_ids.include?(user.id)
  end

  def exempted_user_ids
    @exempted_user_ids ||= YAML.load_file(Rails.root.join('config', 'data', 'car_bookings.yml'))['user_ids_for_exemptions']
  end
end
