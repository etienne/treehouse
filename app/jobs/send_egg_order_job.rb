# frozen_string_literal: true

class SendEggOrderJob < ActiveJob::Base
  queue_as :default

  def perform
    User.includes(:user_setting).where(user_settings: { eggs: true }).each do |user|
      EggsMailer.request_for_next_week(recipient: user).deliver_later
    end
  end
end
