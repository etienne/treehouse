# frozen_string_literal: true

class MealClosingReminderJob < ActiveJob::Base
  queue_as :default

  def perform
    Meal.opened.past.remindable.each do |meal|
      MealClosingReminderMailer.remind(meal).deliver_later
    end
  end
end
