# frozen_string_literal: true

class EggPaymentService
  def initialize(user:, number_of_boxes:, egg_order_id:)
    @egg_order = EggOrder.find_by(id: egg_order_id)
    @number_of_boxes = number_of_boxes
    @user = user
  end

  def execute
    return { success: false, error: error_message } unless ensure_params_for_egg_order
    return { success: false, error: too_late_message } if past_deadline?
    return { success: false, error: limit_error_message } if limit_reached?
    return { success: false, error: almost_limit_error_message } if limit_about_to_be_reached?

    payment = EggPayment.create!(
      egg_order: egg_order,
      user: user,
      number_of_boxes: number_of_boxes
    )

    { success: true, paid: payment.paid? }
  end

  private

  attr_reader :egg_order, :user, :number_of_boxes

  def limit_reached?
    return false if no_max_boxes?

    total_number_of_boxes == egg_order.max_boxes
  end

  def limit_about_to_be_reached?
    return false if no_max_boxes?

    total_number_of_boxes + number_of_boxes > egg_order.max_boxes
  end

  def total_number_of_boxes
    @total_number_of_boxes ||= egg_order.total_number_of_ordered_boxes
  end

  def no_max_boxes?
    egg_order.max_boxes.nil?
  end

  def ensure_params_for_egg_order
    return false unless egg_order&.delivery_within_next_week?
    return false unless [1, 2, 3].include?(number_of_boxes)

    true
  end

  def past_deadline?
    DateTime.now > egg_order.closing_time
  end

  def error_message
    'Sorry, an error occurred. Registration for egg order has not been processed.'
  end

  def too_late_message
    'Sorry, you cannot order these eggs, as we are past registration time for this order.'
  end

  def limit_error_message
    if egg_order.max_boxes.zero?
      'No egg delivery planned for this day.'
    else
      'Sorry, your order was not taken: the limit of orders for this egg delivery has already been reached.'
    end
  end

  def almost_limit_error_message
    "This is a limited delivery: only #{egg_order.max_boxes - total_number_of_boxes} dozen of eggs are still available for delivery."
  end
end
