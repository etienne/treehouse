# frozen_string_literal: true

class CloseFundService
  def initialize(fund:)
    @fund = fund
  end

  def execute
    return { success: false, error: balance_still } unless fund.balance.zero?

    fund.update(archived: true)

    { success: true }
  end

  private

  attr_accessor :fund

  def balance_still
    "The \"#{fund.name}\" fund cannot be closed as there is still money in it."
  end
end
