# frozen_string_literal: true

class EggOrderNotifyPurchasersService
  def initialize(egg_order:)
    @egg_order = egg_order
  end

  def execute
    EggPayment.includes(:user).where(egg_order_id: egg_order.id).map(&:user).uniq.each do |recipient|
      EggsMailer.notify_purchasers_about_delivery(recipient: recipient, egg_order: egg_order).deliver_later
    end
  end

  private

  attr_reader :egg_order
end
