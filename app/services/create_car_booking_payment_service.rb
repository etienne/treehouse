# frozen_string_literal: true

class CreateCarBookingPaymentService
  def initialize(user:, period_start_date:, period_end_date:)
    @user = user
    @period_start_date = period_start_date
    @period_end_date = period_end_date
  end

  def execute
    booking_summary_items = CarBooking.booking_summary_for(
      user: user,
      period_start: period_start_date,
      period_end: period_end_date
    )

    return { success: true } unless booking_summary_items.present?

    payment = CarBookingPayment.create(
      user: user,
      period_start: period_start_date,
      period_end: period_end_date,
      amount: booking_summary_items.sum { |b| b[:cost] }
    )

    { success: true, payment: payment }
  end

  private

  attr_reader :user, :period_start_date, :period_end_date
end
