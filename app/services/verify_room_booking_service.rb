# frozen_string_literal: true

class VerifyRoomBookingService
  MAX_NIGHTS_PER_BOOKING = 10

  def initialize(start_date: nil, end_date: nil)
    @start_date = start_date.is_a?(String) ? Date.parse(start_date) : start_date
    @end_date = end_date.is_a?(String) ? Date.parse(end_date) : end_date
  rescue ArgumentError
    @start_date = nil
    @end_date = nil
  end

  def execute
    { error_message: error_message, date_summary: date_summary, rooms: room_values }
  end

  private

  attr_reader :start_date, :end_date

  def date_summary
    return unless start_date && end_date
    return unless error_message.blank?

    RoomBooking.new(start_date: start_date, end_date: end_date).date_summary
  end

  def room_values
    return unless error_message.blank?

    id_available_map = {}
    booked_rooms.each do |r|
      id_available_map[r.id.to_s] = false
    end
    available_rooms.each do |r|
      id_available_map[r.id.to_s] = true
    end

    id_available_map
  end

  def booked_rooms
    @booked_rooms ||=
      if start_date && end_date
        Room.includes(:room_bookings)
            .where(
              '(room_bookings.start_date BETWEEN ? AND ?) OR (room_bookings.end_date BETWEEN ? AND ?) ' \
              'OR (room_bookings.start_date < ? AND room_bookings.end_date > ?)',
              start_date, end_date - 1.day, start_date + 1.day, end_date, start_date, end_date
            ).references(:room_bookings)
      else
        []
      end
  end

  def available_rooms
    @available_rooms ||= Room.all - booked_rooms
  end

  def error_message
    if start_date.nil? || end_date.nil?
      'Error with booking dates.'
    elsif start_date == end_date
      'Please select different dates to book at least one night.'
    elsif start_date < Date.current
      "Sorry, you can't create a booking in the past."
    elsif Room.for_guests.count == booked_rooms.count
      'Sorry, there is no room available for the dates you have selected.'
    elsif (end_date - start_date).to_i > MAX_NIGHTS_PER_BOOKING
      "You cannot book more than #{MAX_NIGHTS_PER_BOOKING} nights in a row."
    else
      ''
    end
  end
end
