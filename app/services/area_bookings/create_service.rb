# frozen_string_literal: true

module AreaBookings
  class CreateService
    def initialize(booking, user: nil)
      @booking = booking
      @user = user
    end

    def execute
      return unless user

      booking.user = user
      booking.state = require_approval? ? AreaBooking.states[:pending] : AreaBooking.states[:confirmed]

      booking.save

      booking
    end

    def self.execute_for_meal(meal:, user:)
      meal_seatings = meal.meal_seatings.not_take_out.map(&:meal_time)

      booking = AreaBooking.new(
        start_time: meal_seatings.min,
        end_time: meal_seatings.max + 1.hour,
        user: user,
        room: Meal.dining_hall,
        name: 'Common meal',
        description: "Gathering Hall booked for the '#{meal.title}' community meal."
      )

      new(booking, user: user).execute
    end

    private

    attr_reader :user
    attr_accessor :booking

    def require_approval?
      booking.private_event? && booking.room == Room.gathering_hall
    end
  end
end
