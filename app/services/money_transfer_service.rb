# frozen_string_literal: true

class MoneyTransferService
  def initialize(amount:, recipient_fund: nil, sender_fund: nil, recipient_description: nil, sender_description: nil, interac: nil)
    @amount = amount
    @sender_fund = sender_fund
    @recipient_fund = recipient_fund
    @sender_description = sender_description
    @recipient_description = recipient_description
    @interac = interac
  end

  def execute
    return { success: false, error: error_message } unless amount_is_a_positive_number?
    return { success: false, error: recipient_archived_message } if recipient_fund&.archived?

    transfer = Transfer.new(
      amount: amount,
      sender: sender_fund,
      recipient: recipient_fund,
      sender_description: sender_description_with_prefix,
      recipient_description: recipient_description_with_prefix,
      interac: interac
    )

    return { success: false, error: transfer.errors.full_messages.first } unless transfer.valid?

    transfer.pay_now!

    { success: true }
  rescue Transfer::SenderNotEnoughMoneyError
    { success: false, error: 'Transfer cancelled: not enough money in sender\'s fund.' }
  end

  def add_money_to_fund
    message = if recipient_description.present?
                recipient_description
              elsif recipient_fund.community?
                'ETransfer contribution'
              else
                'You added money'
              end

    message += " (Interac: #{masked_interac})" if @interac.present?

    @recipient_description = message

    execute
  end

  def send_money
    @sender_description = 'Money transfer' unless @sender_description.presence
    @recipient_description = 'Money transfer' unless @recipient_description.presence

    execute
  end

  private

  attr_accessor :amount, :recipient_fund, :sender_fund, :sender_description, :recipient_description, :interac

  def error_message
    'Please choose a valid amount to add to a fund.'
  end

  def masked_interac
    Transfer.new(interac: @interac).masked_interac
  end

  def amount_is_a_positive_number?
    amount.is_a?(Numeric) && amount > 0.0
  end

  def recipient_archived_message
    'You cannot send money to a close fund.'
  end

  def recipient_description_with_prefix
    return unless recipient_fund

    sender_fund.present? ? "[From #{sender_fund.name_for_transfer}] #{@recipient_description}" : @recipient_description
  end

  def sender_description_with_prefix
    return unless sender_fund

    recipient_fund.present? ? "[To #{recipient_fund.name_for_transfer}] #{@sender_description}" : @sender_description
  end
end
