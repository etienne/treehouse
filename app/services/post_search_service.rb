# frozen_string_literal: true

class PostSearchService
  NUMBER_OF_RESULTS_PER_PAGE = 10

  def initialize(params)
    @params = params || {}
  end

  def execute
    {
      total_count: posts.count,
      posts: posts_per_page
    }
  end

  def self.number_of_results_per_page
    NUMBER_OF_RESULTS_PER_PAGE
  end

  private

  attr_reader :params

  def posts_per_page
    posts.page(page_number).per(self.class.number_of_results_per_page)
  end

  def posts
    @posts ||= begin
      posts = Post.includes(:item_category).by_most_recent_first
      posts = posts.where(item_category_id: params[:category]) if params[:category].presence
      posts = posts.search_post(params[:search]) if params[:search].presence

      posts
    end
  end

  def page_number
    params[:page] || 1
  end
end
