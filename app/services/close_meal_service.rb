# frozen_string_literal: true

class CloseMealService
  def initialize(meal:)
    @meal = meal
  end

  def execute
    return { success: false, error: 'This meal is already closed.' } if @meal.closed?
    return { success: false, error: 'This meal cannot be closed as no cost was mentioned.' } unless @meal.cost

    meal_lead = @meal.user

    result = if meal_lead.settings.reimburse_to_fund?
               MoneyTransferService.new(
                 amount: @meal.cost,
                 sender_fund: sender_fund,
                 sender_description: description,
                 recipient_fund: meal_lead.fund,
                 recipient_description: description
               ).execute
             else
               res = MoneyTransferService.new(
                 amount: @meal.cost,
                 sender_fund: sender_fund,
                 sender_description: description
               ).execute

               MealMailer.notify_for_etransfer(meal: @meal).deliver_later

               res
             end

    return result if result && !result[:success]

    @meal.update!(closed: true)

    { success: true }
  end

  private

  attr_reader :meal

  def sender_fund
    Fund.common_meals
  end

  def description
    "Reimbursement for #{meal.meal_date.strftime('%b %d')} meal."
  end
end
