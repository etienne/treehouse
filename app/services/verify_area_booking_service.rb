# frozen_string_literal: true

class VerifyAreaBookingService
  def initialize(start_time: nil, end_time: nil)
    @start_time = start_time.is_a?(String) ? DateTime.parse(start_time) : start_time
    @end_time = end_time.is_a?(String) ? DateTime.parse(end_time) : end_time
  rescue ArgumentError
    @start_time = nil
    @end_time = nil
  end

  def execute
    { error_message: error_message, booking_datetime: booking_datetime, rooms: area_values }
  end

  private

  attr_reader :start_time, :end_time

  def booking_datetime
    return unless start_time && end_time
    return unless error_message.blank?

    AreaBooking.new(start_time: start_time, end_time: end_time).booking_datetime
  end

  def area_values
    return unless error_message.blank?

    id_available_map = {}
    booked_areas.each do |r|
      id_available_map[r.id.to_s] = false
    end
    available_areas.each do |r|
      id_available_map[r.id.to_s] = true
    end

    id_available_map
  end

  def booked_areas
    @booked_areas ||=
      if start_time && end_time
        Room.ch_areas.includes(:area_bookings)
            .where(
              '(area_bookings.start_time BETWEEN ? AND ?) OR (area_bookings.end_time BETWEEN ? AND ?) ' \
              'OR (area_bookings.start_time < ? AND area_bookings.end_time > ?)',
              start_time, end_time, start_time, end_time, start_time, end_time
            ).references(:area_bookings)
      else
        []
      end
  end

  def available_areas
    @available_areas ||= Room.all - booked_areas
  end

  def error_message
    if start_time.nil? || end_time.nil?
      'Error with booking times.'
    elsif start_time == end_time
      'Please select different start and end times.'
    elsif start_time < DateTime.current
      "Sorry, you can't create a booking in the past."
    elsif Room.ch_areas.count == booked_areas.count
      'Sorry, there is no CH area available for the times you have selected.'
    elsif (Time.parse(end_time.to_s) - Time.parse(start_time.to_s)) / 1.hour > AreaBooking::TIME_GAP_MAX
      "You cannot book an area for more than #{AreaBooking::TIME_GAP_MAX} hours."
    else
      ''
    end
  end
end
