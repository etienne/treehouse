# frozen_string_literal: true

class CreateNextWeekEggOrdersService
  def execute
    delivery_data.each do |delivery|
      delivery_date = Date.current.next_week(delivery['delivery_day'])
      closing_time_the_day_before = (delivery_date - 1.day).in_time_zone + delivery['closing_hour_on_day_before'].hours

      EggOrder.create!(
        purchase_date: delivery_date,
        closing_time: closing_time_the_day_before,
        price: delivery['cost'],
        max_boxes: delivery['max_boxes']
      )
    end

    { success: true }
  end

  private

  def delivery_data
    YAML.load_file(Rails.root.join('config', 'data', 'egg_schedule.yml'))['egg_orders']
  end
end
