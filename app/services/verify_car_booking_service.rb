# frozen_string_literal: true

class VerifyCarBookingService
  def initialize(start_time: nil, end_time: nil)
    @start_time = start_time.is_a?(String) ? DateTime.parse(start_time) : start_time
    @end_time = end_time.is_a?(String) ? DateTime.parse(end_time) : end_time
  rescue ArgumentError
    @start_time = nil
    @end_time = nil
  end

  def execute
    { error_message: error_message, time_summary: time_summary, cars: car_values }
  end

  private

  attr_reader :start_time, :end_time

  def time_summary
    return unless start_time && end_time
    return unless error_message.blank?

    CarBooking.new(start_time: start_time, end_time: end_time).day_time_summary
  end

  def car_values
    return unless error_message.blank?

    id_available_map = {}
    booked_cars.each do |c|
      id_available_map[c.id.to_s] = false
    end
    available_cars.each do |c|
      id_available_map[c.id.to_s] = true
    end

    id_available_map
  end

  def booked_cars
    @booked_cars ||=
      if start_time && end_time
        Car.includes(:car_bookings)
           .where(
             '(car_bookings.start_time BETWEEN ? AND ?) OR (car_bookings.end_time BETWEEN ? AND ?) ' \
             'OR (car_bookings.start_time < ? AND car_bookings.end_time > ?)',
             start_time, end_time, start_time, end_time, start_time, end_time
           ).references(:car_bookings)
      else
        []
      end
  end

  def available_cars
    @available_cars ||= Car.all - booked_cars
  end

  def error_message
    if start_time.nil? || end_time.nil?
      'Error with booking dates.'
    elsif start_time < DateTime.now
      "Sorry, you can't create a booking in the past."
    elsif Car.all.count == booked_cars.count
      'Sorry, there is no car available for the time you have selected.'
    else
      ''
    end
  end
end
