# frozen_string_literal: true

class MealTeamService
  LEAD = 'Meal lead'
  SERVICE = 'Service lead'
  PREP = 'Meal prep'
  CLEANER = 'Clean-up'

  def initialize(meal:)
    @meal = meal
  end

  def execute
    team_roles.each do |role|
      user = role == LEAD ? meal.user : nil

      MealTeamMember.create!(
        role: role,
        meal: meal,
        user: user
      )
    end

    MealNotificationJob.perform_later(meal)
  end

  private

  def team_roles
    roles = [LEAD, SERVICE]

    prep_number = meal_team_input['preps']
    (1..prep_number).each { |n| roles << "#{PREP} #{n}" } if prep_number.positive?

    cleaners_by_seating = meal_team_input['cleaners_by_seating']
    seatings = meal.meal_seatings.not_take_out

    if cleaners_by_seating.positive?
      seatings.each do |seating|
        (1..cleaners_by_seating).each do |n|
          roles << "#{seating.meal_time.strftime('%l.%M %P').strip} seating clean-up #{n}"
        end
      end
    end

    finish_cleaners = meal_team_input['finish_cleaners']
    if finish_cleaners.positive?
      (1..finish_cleaners).each do |n|
        roles << "#{seatings.last.meal_time.strftime('%l.%M %P').strip} seating clean-up #{cleaners_by_seating + n}"
      end
    end

    roles
  end

  def meal_team_input
    @meal_team_input ||= YAML.load_file(Rails.root.join('config', 'data', 'meals.yml'))['meals']
  end

  attr_reader :meal
end
