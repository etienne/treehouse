# frozen_string_literal: true

class CarChargingService
  HALF_HOUR_CHARGE_KMS = 20

  def initialize(car_booking)
    @car_booking = car_booking
  end

  def event_data
    {
      title: 'Charge time',
      start: car_booking.booking_end_time,
      end: end_of_charging_time,
      carname: car.title,
      booking_id: "#{car_booking.id}-charging",
      eventtype: 'charging',
      chargingduration: "#{charging_time_duration_mins} mins",
      color: past_charging_time? ? Booking::PAST_BOOKING_COLOR : lighten_color
    }
  end

  private

  attr_accessor :car_booking

  def car
    @car ||= car_booking.car
  end

  def end_of_charging_time
    car_booking.booking_end_time + (charging_time_duration_mins * 60)
  end

  def charging_time_duration_mins
    needed_half_hours = (car_booking.expected_distance.to_f / HALF_HOUR_CHARGE_KMS).round

    needed_half_hours * 30
  end

  def lighten_color
    amount = 0.2
    hex_color = car_booking.booking_color_to_use.gsub('#', '')
    rgb = hex_color.scan(/../).map(&:hex)
    rgb[0] = [(rgb[0].to_i + (255 * amount)).round, 255].min
    rgb[1] = [(rgb[1].to_i + (255 * amount)).round, 255].min
    rgb[2] = [(rgb[2].to_i + (255 * amount)).round, 255].min

    '#%02x%02x%02x' % rgb
  end

  def past_charging_time?
    end_of_charging_time < DateTime.now
  end
end
