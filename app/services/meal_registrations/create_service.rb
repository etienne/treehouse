# frozen_string_literal: true

module MealRegistrations
  class CreateService < BaseService
    def initialize(meal:, user:, params:)
      @meal = meal
      @user = user

      super(params: params)
    end

    def execute
      return { success: false, errors: [not_enough_fund_message] } unless enough_fund?

      errors = validate_params
      return { success: false, errors: errors } unless errors.empty?

      ActiveRecord::Base.transaction do
        meal_payment = create_meal_payment_and_pay!

        if user.meal_v2_enabled?
          create_meal_seating_choices_and_attendees(meal_payment)
        else
          create_attendees_for(meal_payment)
          create_portion_choices_for(meal_payment)
        end

        assign_member_to_role
      end

      { success: true }
    end

    private

    attr_reader :meal, :user

    def create_attendees_for(meal_payment)
      params[:people].each do |person|
        member = household_members.select { |m| m.first_name == person }.first
        next unless member

        MealPaymentAttendee.create!(
          meal: meal,
          meal_payment: meal_payment,
          attendee: member
        )
      end
    end

    def create_portion_choices_for(meal_payment)
      meal_portions.each do |portion|
        quantity = params[:choices][portion.id.to_s].to_i
        next unless quantity.positive?

        MealPortionChoice.create!(
          meal_portion: portion,
          quantity: quantity,
          meal_payment: meal_payment
        )
      end
    end

    def create_meal_payment_and_pay!
      if user.meal_v2_enabled?
        MealPayment.create!(
          meal: meal,
          user: user,
          household: user.household,
          amount: total_amount,
          note: params[:note].presence
        )
      else
        MealPayment.create!(
          meal: meal,
          user: user,
          household: user.household,
          guests: params[:guests].presence || 0,
          amount: total_amount,
          note: params[:note].presence,
          meal_seating: seating,
          take_out: take_out?
        )
      end
    end

    def assign_member_to_role
      return unless params[:roles].present?

      user_ids = params[:roles].keys

      user_ids.each do |user_id|
        team_member = MealTeamMember.find_by(id: params[:roles][user_id])
        next unless team_member

        team_member.update(user_id: user_id)
      end
    end

    def enough_fund?
      user.fund.balance >= total_amount
    end

    def not_enough_fund_message
      'You don\'t have enough money to pay for this meal. Please top-up your personal fund and try again.'
    end

    def household
      @household ||= user.household
    end

    def create_meal_seating_choices_and_attendees(meal_payment)
      meal.meal_seatings.each do |seating|
        create_registration_for(seating: seating, meal_payment: meal_payment)
      end
    end
  end
end
