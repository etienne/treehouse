# frozen_string_literal: true

module MealRegistrations
  class BaseService
    MealPortionInvalid = Class.new(StandardError)

    def initialize(params:)
      @params = params
    end

    private

    attr_reader :params

    def validate_params
      errors = []

      if user.meal_v2_enabled?
        errors += no_portion_chosen_error_messages
        errors += enough_spots_error_messages
      else
        errors << 'Select at least one person who will attend this meal' if params[:people].nil? || params[:people].empty?
        errors << 'Choose at least one portion' if no_portion_chosen?
        errors << 'Not enough spots available for you at this seating' unless enough_spots?
      end


      errors
    end

    def no_portion_chosen?
      params[:choices].nil? || params[:choices].values.map(&:to_i).uniq == [0]
    end

    def no_portion_chosen_error_messages
      return [] unless user.meal_v2_enabled? && params[:seatings].present?

      error_messages = []

      meal.meal_seatings.map do |s|
        next unless params[:seatings].include?(s.id.to_s)

        selected_seating = params[:choices][s.id.to_s]
        count = selected_seating.values.map(&:to_i)
        error_messages << "#{s.time_or_take_out_label_with_seating}: choose at least one portion" if count.sum.zero?
      end

      error_messages
    end

    def enough_spots_error_messages
      error_messages = []

      meal.meal_seatings.map do |s|
        seating_id = s.id.to_s
        next unless params[:seatings].include?(seating_id)

        number_selected_members = params[:people][seating_id].count
        number_of_guests = params[:guests][seating_id].to_i
        if number_of_guests + number_selected_members > s.spots_left
          error_messages << "#{s.time_or_take_out_label_with_seating}: not enough spots available"
        end
      end

      error_messages
    end

    def enough_spots?
      return true unless params[:seating] && params[:people]

      spots_left = meal.spots_left_by_seating_time[seating.meal_time.to_s]

      number_of_people <= spots_left
    end

    def seating
      @seating ||= MealSeating.find(params[:seating]) if params[:seating]
    end

    def number_of_people
      if user.meal_v2_enabled?
        params[:guests].values.map(&:to_i).sum +
          params[:people].values.flatten.count
      else
        params[:guests].to_i + params[:people].count
      end
    end

    def meal_portions
      @meal_portions ||= begin
        portions = []
        params[:choices].each_key do |k|
          portions << MealPortion.find_by(id: k)
        end

        raise MealPortionInvalid if portions.any?(nil)

        portions
      end
    end

    def household_members
      household.users + household.kids
    end

    def take_out?
      params[:take_out] == 'yes'
    end

    def total_amount
      amount = 0.0

      if user.meal_v2_enabled?
        portions = meal.meal_portions

        params[:choices].each do |seating_id, choices|
          next unless params[:seatings].include?(seating_id)

          choices.each do |portion_id, quantity|
            portion_amount = portions.find_by(id: portion_id.to_i).amount

            amount += portion_amount * quantity.to_i
          end
        end
      else
        meal_portions.each do |portion|
          amount += portion.amount * params[:choices][portion.id.to_s].to_i
        end
      end

      amount
    end

    def create_registration_for(seating:, meal_payment:)
      seating_id = seating.id.to_s
      choices = params[:choices][seating_id]
      people = params[:people]

      return unless choices && params[:seatings]
      return unless people&.key?(seating_id) || params[:guests]&.key?(seating_id)
      return unless choices.values.map(&:to_i).sum.positive?

      msc = MealSeatingChoice.create!(
        meal_payment: meal_payment,
        meal_seating: seating,
        guests: params[:guests][seating_id].to_i
      )

      if people
        attendees = people[seating_id]

        attendees.each do |attendee|
          member = household_members.select { |m| m.first_name == attendee }.first
          MealSeatingAttendee.create!(
            attendee: member,
            meal_seating_choice: msc
          )
        end
      end

      choices.each do |portion_id, quantity|
        next if quantity.to_i.zero?

        MealPortionChoice.create!(
          meal_portion_id: portion_id.to_i,
          quantity: quantity.to_i,
          meal_payment: meal_payment,
          meal_seating_choice: msc
        )
      end
    end
  end
end
