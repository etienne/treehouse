# frozen_string_literal: true

module MealRegistrations
  class UpdateService < BaseService
    def initialize(meal_payment:, params:)
      @meal_payment = meal_payment
      @meal = meal_payment.meal
      @user = meal_payment.user

      super(params: params)
    end

    def execute
      errors = validate_params
      return { success: false, errors: errors } unless errors.empty?

      ActiveRecord::Base.transaction do
        if user.meal_v2_enabled?
          update_meal_seating_choices_and_attendees(meal_payment)
        else
          update_attendees
          update_portion_choices
        end

        update_meal_payment
        reassign_member_to_role
      end

      { success: true }
    rescue StandardError => e
      messages = meal_payment.errors.present? ? meal_payment.errors.full_messages : [e.message]

      { success: false, errors: messages }
    end

    private

    attr_reader :meal_payment, :user, :meal

    # No need to check spot availability upon registration update
    def enough_spots?
      true
    end

    def update_attendees
      selected_members = []

      params[:people].each do |person|
        member = household_members.select { |m| m.first_name == person }.first
        next unless member

        selected_members << member
        MealPaymentAttendee.find_or_create_by!(
          meal: meal,
          meal_payment: meal_payment,
          attendee: member
        )
      end

      MealPaymentAttendee.where(
        meal: meal,
        meal_payment: meal_payment,
        attendee: household_members - selected_members
      ).each(&:destroy!)
    end

    def update_portion_choices
      meal_portions.each do |portion|
        quantity = params[:choices][portion.id.to_s].to_i
        next unless quantity >= 0

        existing_choice = MealPortionChoice.find_or_create_by!(
          meal_portion: portion,
          meal_payment: meal_payment
        )
        quantity.zero? ? existing_choice.destroy! : existing_choice.update(quantity: quantity)
      end
    end

    def update_meal_payment
      if user.meal_v2_enabled?
        meal_payment.update!(
          note: params[:note].presence,
          amount: total_amount
        )
      else
        meal_payment.update!(
          guests: params[:guests].presence || 0,
          note: params[:note].presence,
          amount: total_amount,
          meal_seating: seating,
          take_out: take_out?
        )
      end
    end

    def reassign_member_to_role
      return unless params[:roles].present?

      user_ids = params[:roles].keys

      user_ids.each do |user_id|
        role_id = params[:roles][user_id]

        team_member = MealTeamMember.find_by(id: role_id)
        previous_team_member = MealTeamMember.find_by(meal_id: meal_payment.meal_id, user_id: user_id)
        next if previous_team_member == team_member

        team_member&.update(user_id: user_id)
        previous_team_member&.update(user_id: nil)
      end
    end

    def meal
      @meal ||= meal_payment.meal
    end

    def household
      @household ||= meal_payment.household
    end

    def update_meal_seating_choices_and_attendees(meal_payment)
      meal.meal_seatings.each do |seating|
        choices = params[:choices][seating.id.to_s]
        seating_chosen = params[:seatings].include?(seating.id.to_s)
        recorded_seating_choice = MealSeatingChoice.find_by(meal_payment: meal_payment, meal_seating: seating)

        next if recorded_seating_choice.nil? && !seating_chosen

        if recorded_seating_choice.present? && !seating_chosen
          recorded_seating_choice.destroy!
          next
        end

        if recorded_seating_choice.nil? && seating_chosen
          create_registration_for(seating: seating, meal_payment: meal_payment)
          next
        end

        recorded_seating_choice.update(guests: params[:guests][seating.id.to_s].to_i)

        attendees = params[:people][seating.id.to_s]

        attendees.each do |attendee|
          member = household_members.select { |m| m.first_name == attendee }.first
          MealSeatingAttendee.find_or_create_by!(
            attendee: member,
            meal_seating_choice: recorded_seating_choice
          )
        end

        choices.each do |portion_id, quantity|
          existing_choice = MealPortionChoice.find_or_create_by!(
            meal_portion_id: portion_id.to_i,
            meal_payment: meal_payment,
            meal_seating_choice: recorded_seating_choice
          )

          quantity.to_i.zero? ? existing_choice.destroy! : existing_choice.update(quantity: quantity)
        end
      end
    end
  end
end
