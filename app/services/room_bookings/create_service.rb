# frozen_string_literal: true

module RoomBookings
  class CreateService
    THRESHOLD_FOR_CONFIRMATION = 4.months

    def initialize(booking, user: nil)
      @booking = booking
      @user = user
    end

    def execute
      return unless user

      booking.user = user
      booking.state = booking_state
      booking.confirmed_at = confirmed_at

      booking.save

      create_payment if booking.persisted? && booking.payment_required?

      booking
    end

    private

    def create_payment
      num_nights = (price_to_pay / RoomBooking::PRICE_PER_NIGHT).to_i
      RoomPayment.create(
        description: "Payment for #{num_nights} #{'night'.pluralize(num_nights)}",
        amount: price_to_pay,
        user: user,
        room_booking: booking
      )
    end

    def price_to_pay
      @price_to_pay ||= user.price_to_pay_for(booking)
    end

    def threshold_date_for_confirmation
      booking.start_date - THRESHOLD_FOR_CONFIRMATION
    end

    def confirmed_at
      if booking_state == RoomBooking.states[:pending]
        nil
      else
        Date.current
      end
    end

    def booking_state
      if price_to_pay.positive?
        RoomBooking.states[:payment_required]
      elsif Date.current > threshold_date_for_confirmation
        RoomBooking.states[:confirmed]
      else
        RoomBooking.states[:pending]
      end
    end

    attr_accessor :booking, :user
  end
end
