# frozen_string_literal: true

class DeviseMailer < Devise::Mailer
  default from: "TVE #{Rails.env.staging? ? 'Staging' : 'internal website'} <#{Rails.application.credentials.dig(:email, :sender)}>"
  layout 'mailer'
end
