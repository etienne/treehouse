# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: "TVE #{Rails.env.staging? ? 'Staging' : 'internal website'} <#{Rails.application.credentials.dig(:email, :sender)}>"
  layout 'mailer'

  before_action :set_return_path

  def set_return_path
    headers['Return-Path'] = Rails.application.credentials.dig(:email, :return_path)
  end
end
