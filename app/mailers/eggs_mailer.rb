# frozen_string_literal: true

class EggsMailer < ApplicationMailer
  def request_for_next_week(recipient:)
    return unless recipient

    @recipient = recipient
    @egg_orders = EggOrder.next_week_orders.order(purchase_date: :asc)

    mail(to: @recipient.email, subject: 'Egg order for next week')
  end

  def notify_purchasers_about_delivery(recipient:, egg_order:)
    return unless recipient && egg_order

    @egg_order = egg_order
    @recipient = recipient

    mail(to: @recipient.email, subject: 'Your eggs have arrived!')
  end
end
