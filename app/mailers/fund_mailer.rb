# frozen_string_literal: true

class FundMailer < ApplicationMailer
  include ActiveSupport::NumberHelper

  def notify_recipient(sender: nil, recipient:, amount:)
    return unless recipient && amount

    @sender = sender
    @recipient = recipient
    @amount = number_to_currency(amount)

    mail(to: @recipient.email, subject: "#{@amount} added to your TVE fund")
  end
end
