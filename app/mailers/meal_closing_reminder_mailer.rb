# frozen_string_literal: true

class MealClosingReminderMailer < ApplicationMailer
  def remind(meal)
    return unless meal

    @meal = meal
    @user = meal.user

    meal.update!(closing_reminded_at: DateTime.now)

    mail(to: @user.email, subject: "Let's close your meal")
  end
end
