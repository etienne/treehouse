# frozen_string_literal: true

class CarBookingMailer < ApplicationMailer
  def send_car_booking_usage_summary(user:, booking_payment:)
    @user = user
    @booking_payment = booking_payment
    @period_start = booking_payment.period_start
    @period_end = booking_payment.period_end

    @booking_summary = CarBooking.booking_summary_for(
      user: user,
      period_start: @period_start,
      period_end: @period_end
    )

    @total_amount = @booking_summary.sum { |b| b[:cost] }

    mail(to: @user.email, subject: "Your car usage from #{@period_start} to #{@period_end}")
  end
end
