# frozen_string_literal: true

class MealMailer < ApplicationMailer
  def notify_members(meal: nil, user: nil)
    return unless meal && user

    @meal = meal
    @user = user

    mail(to: @user.email, subject: "New meal: #{@meal.title} - #{@meal.meal_date.strftime('%A %B %d')} ")
  end

  def notify_for_etransfer(meal: nil)
    @user = Meal.user_managing_etransfers

    return unless meal && @user
    return unless meal.cost

    @meal = meal
    @meal_lead = meal.user

    mail(to: @user.email, subject: 'Meal lead to etransfer')
  end
end
