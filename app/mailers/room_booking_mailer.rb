# frozen_string_literal: true

class RoomBookingMailer < ApplicationMailer
  def confirmation(booking)
    return unless booking

    @booking = booking
    @user = booking.user

    mail(to: @booking.user.email, subject: 'Your guest room booking to confirm')
  end

  def last_reminder(booking)
    return unless booking

    @booking = booking
    @user = booking.user

    mail(to: @booking.user.email, subject: 'Reminder about your upcoming guest room booking')
  end
end
