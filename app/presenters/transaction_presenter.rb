# frozen_string_literal: true

class TransactionPresenter
  def initialize(fund:)
    @fund = fund
  end

  def present
    transactions.map do |transaction|
      {
        timestamp: timestamp_for(transaction),
        date: date_for(transaction),
        description: description_for(transaction),
        plus_or_minus: credit_debit_for(transaction),
        amount: amount_for(transaction),
        new_balance: new_balance_for(transaction)
      }
    end
  end

  private

  attr_reader :fund

  def transactions
    nil
  end

  def timestamp_for(transaction)
    transaction.created_at.to_i
  end

  def date_for(transaction)
    transaction.created_at.strftime('%e %b %Y')
  end

  def description_for(_transaction)
    nil
  end

  def credit_debit_for(_transaction)
    nil
  end

  def amount_for(transaction)
    transaction.amount
  end

  def new_balance_for(_transaction)
    nil
  end

  def transfers_as_sender
    Transfer.where(sender: fund)
  end
end
