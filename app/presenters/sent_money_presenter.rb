# frozen_string_literal: true

class SentMoneyPresenter < TransactionPresenter
  private

  def transactions
    fund.sent_transfers
  end

  def description_for(transfer)
    saved_description = transfer.sender_description
    return saved_description if saved_description.present?

    "Money sent to #{transfer.recipient.name}."
  end

  def credit_debit_for(_transfer)
    '-'
  end

  def new_balance_for(transfer)
    transfer.sender_new_balance
  end
end
