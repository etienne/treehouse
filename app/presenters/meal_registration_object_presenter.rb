# frozen_string_literal: true

class MealRegistrationObjectPresenter
  def initialize(meal_payment:)
    @meal_payment = meal_payment
  end

  def note
    meal_payment.note
  end

  def chosen_seating_ids
    meal_payment.meal_seating_choices.map(&:meal_seating_id)
  end

  def people_first_name_for_seating(seating)
    choice = meal_payment.meal_seating_choices.find_by(meal_seating_id: seating.id)
    return [] unless choice

    choice.meal_seating_attendees.map(&:attendee).map(&:first_name)
  end

  def number_of_guests_for_seating(seating)
    choice = meal_payment.meal_seating_choices.find_by(meal_seating_id: seating.id)
    return 0 unless choice

    choice.guests
  end

  def number_of_portions(seating:, portion:)
    choice = meal_payment.meal_seating_choices.find_by(meal_seating_id: seating.id)
    return 0 unless choice

    portion_choice = choice.meal_portion_choices.find_by(meal_portion_id: portion.id)

    portion_choice&.quantity || 0
  end

  def roles_for_users
    roles = meal_payment.meal.meal_team_members.where.not(user_id: nil)

    roles.each_with_object({}) { |el, h| h[el.user_id.to_s] = el.id.to_s }
  end

  private

  attr_reader :meal_payment
end
