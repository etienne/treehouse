# frozen_string_literal: true

class MealRegistrationPresenter
  def initialize(params:, meal:)
    @params = params
    @meal = meal
  end

  def note
    params[:note].presence
  end

  def chosen_seating_ids
    return [] unless params[:seatings]

    params[:seatings].map(&:to_i)
  end

  def people_first_name_for_seating(seating)
    return [] unless params[:people]

    params[:people][seating.id.to_s] || []
  end

  def number_of_guests_for_seating(seating)
    return 0 unless params[:guests]

    params[:guests][seating.id.to_s].to_i
  end

  def number_of_portions(seating:, portion:)
    return 0 unless seating && portion && params[:choices]

    params[:choices][seating.id.to_s][portion.id.to_s] || 0
  end

  def roles_for_users
    roles = params[:roles] || {}
    roles[meal.meal_lead.id.to_s] = meal.meal_team_members.find_by(role: 'Meal lead').id.to_s

    roles
  end

  private

  attr_reader :params, :meal
end
