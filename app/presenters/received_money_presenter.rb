# frozen_string_literal: true

class ReceivedMoneyPresenter < TransactionPresenter
  private

  def transactions
    fund.received_transfers
  end

  def description_for(transfer)
    saved_description = transfer.recipient_description
    return saved_description if saved_description.present?

    sender = transfer.sender
    recipient = transfer.recipient

    return 'Money added by member' if recipient.community?

    sender.present? ? "You received money from #{sender.name}." : 'You received money.'
  end

  def credit_debit_for(_transfer)
    '+'
  end

  def new_balance_for(transfer)
    transfer.recipient_new_balance
  end
end
