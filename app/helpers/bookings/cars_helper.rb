# frozen_string_literal: true

module Bookings
  module CarsHelper
    def car
      @car ||= Car.find_by(id: params[:car_id]) || @car_booking.car
    end

    def electric_car_note
      "When you come back from your trip to #{Setting.app_name}, please plug in this car to recharge it if the battery level is low"
    end

    def non_electric_car_note
      "Remember to get some gasoline on your way back to #{Setting.app_name}, bring it back with the gas level you found the car with."
    end
  end
end
