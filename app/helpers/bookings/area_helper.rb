# frozen_string_literal: true

module Bookings
  module AreaHelper
    def options_for_area_booking_level
      green = AreaBooking.green_level
      yellow = AreaBooking.yellow_level
      red = AreaBooking.red_level

      [
        [[green.color, green.label].join(' '), green.id],
        [[yellow.color, yellow.label].join(' '), yellow.id],
        [[red.color, red.label].join(' '), red.id]
      ]
    end
  end
end
