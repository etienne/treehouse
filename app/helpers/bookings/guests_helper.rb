# frozen_string_literal: true

module Bookings
  module GuestsHelper
    def guest_room
      @guest_room ||= Room.find_by(id: params[:room_id]) || @room_booking.room
    end

    def area_room
      @area_room ||= Room.find_by(id: params[:room_id]) || @area_booking.room
    end

    def booking_credit_left_for(booked_nights, year)
      number_of_booked_nights = booked_nights[year] || 0
      max_num_nights = RoomBooking::MAX_NUMBER_OF_FREE_NIGHTS
      nights_left = max_num_nights - number_of_booked_nights

      if nights_left.positive?
        "#{year}: #{nights_left} free #{'night'.pluralize(nights_left)} left"
      else
        "#{year}: $#{RoomBooking::PRICE_PER_NIGHT} per night"
      end
    end

    def days_since_confirmation_email_for(booking)
      return 0 unless booking
      return 0 if booking.confirmed_at

      confirmation_date = (booking.start_date - RoomBooking::CONFIRMATION_BEFORE_START_MONTHS.months).to_date

      [(Date.current - confirmation_date).to_i, 0].max
    end

    def label_for_purpose(purpose)
      {
        Room::GUEST => 'Guest room',
        Room::CH_AREA => 'Common House area'
      }[purpose]
    end
  end
end
