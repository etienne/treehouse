# frozen_string_literal: true

module ApplicationHelper
  def alert_close_button
    content_tag :button, '', class: 'btn-close', 'aria-label': 'Close', 'data-bs-dismiss': 'alert', type: 'button'
  end

  def alert_for(role:, text:)
    message = ''.html_safe
    message << alert_close_button
    message << content_tag(:p, text)
    content_tag :div, message, class: "alert alert-dismissible alert-#{role}", role: role
  end

  def phone_number_html_link_for(phone)
    return nil unless phone

    "<a href='tel:#{phone.gsub('-', '')}'>#{phone}</a>"
  end

  def email_html_link_for(email)
    return nil unless email

    "<a href='mailto:#{email}'>#{email}</a>"
  end
end
