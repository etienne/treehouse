# frozen_string_literal: true

module AdminHelper
  def email_addresses_for_user_subscribers
    User.active.includes(:user_setting).where(user_setting: { eggs: true } ).map(&:email).join('; ')
  end
end
