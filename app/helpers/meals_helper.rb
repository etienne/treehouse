# frozen_string_literal: true

module MealsHelper
  def spots_left_for(seating)
    seating.take_out? ? '' : " (#{pluralize(seating.spots_left, 'spot')} left)"
  end

  def meal_date_and_organizer_for(meal)
    "#{meal_date_for(meal)} - organized by #{meal.user.full_name}".html_safe
  end

  def meal_date_for(meal)
    parse_meal_date_for(meal)
  end

  def meal_reg_date_and_time_for(meal, household)
    meal_payment = meal.meal_payments.find_by(household: household)
    return '' unless meal_payment

    parse_meal_date_and_time_for(meal, meal_payment)
  end

  def meal_times_for(meal)
    meal.meal_seatings.map do |s|
      next unless s.meal_time.present?

      s.meal_time.strftime('%I.%M %P')
    end.compact.join(' / ')
  end

  def number_of_portions_for(meal)
    res = {}

    choices_per_portions =
      MealPayment
      .includes(meal_portion_choices: :meal_portion)
      .where(meal_id: meal.id)
      .map(&:meal_portion_choices).flatten
      .group_by { |c| [c.meal_portion.description, c.meal_payment.seating_or_take_out_label] }

    choices_per_portions.each { |k, choices| res[k] = choices.sum(&:quantity) }

    res
  end

  def meal_admin_action_call(user)
    if user.settings.reimburse_to_fund
      "This money will go directly to #{user.first_name}'s personal fund, as per their user settings."
    else
      'Please make sure to etransfer this amount to the person who did the groceries.'
    end
  end

  def num_portions_for_seating_by_portion_type(portions:, seating:)
    return 0 unless portions

    portions.select { |data, _| data[1] == seating }.map { |data, v| "#{data[0]}: #{v}" }
  end

  def num_portions_by_description(portions:, description:)
    return 0 unless portions

    portions.select { |data, _| data[0] == description }.values.sum
  end

  def cost_breakdown_for(meal_payment)
    summaries = []

    meal_payment.meal_seating_choices.each do |c|
      c.meal_portion_choices.each do |pc|
        summaries << "#{pc.quantity} x #{number_to_currency(pc.meal_portion.amount)}"
      end
    end

    summaries.join(' + ')
  end

  private

  def parse_meal_date_for(meal)
    Time.parse(meal.meal_date.to_s).strftime('%A %B %d').strip
  end

  def parse_meal_date_and_time_for(meal, meal_payment)
    "#{parse_meal_date_for(meal)}, #{meal_payment.seating_or_take_out_label}"
  end
end
