# frozen_string_literal: true

module PostsHelper
  PriceLabel = Struct.new(:id, :name, :label)

  PRICE_OPTIONS_LABELS = [
    PriceLabel.new(Post.price_options[:default], 'default', 'Default price'),
    PriceLabel.new(Post.price_options[:per_item], 'per_item', 'Price per item'),
    PriceLabel.new(Post.price_options[:see_description], 'see_description', 'Price(s) in description'),
    PriceLabel.new(Post.price_options[:contact_seller], 'contact_seller', 'Contact seller'),
    PriceLabel.new(Post.price_options[:barter], 'barter', 'Let\'s negociate!'),
    PriceLabel.new(Post.price_options[:best_offer], 'best_offer', 'Best offer'),
    PriceLabel.new(Post.price_options[:free], 'free', 'Free!'),
    PriceLabel.new(Post.price_options[:borrow], 'borrow', 'Borrow')
  ].freeze

  def item_categories
    @item_categories ||= ItemCategory.all
  end

  def price_options
    PRICE_OPTIONS_LABELS
  end

  def col_classes_for_show_post(post)
    col_md_width = post.photos.present? ? '6' : '9'

    ['col-12', "col-md-#{col_md_width}"].join(' ')
  end

  def result_summary
    return 'Sorry, there is no post matching your request.' if @posts.empty?

    page_number = (params[:page] || 1).to_i
    position = (page_number - 1) * PostSearchService.number_of_results_per_page
    min = position + 1
    max = position + @posts.count
    category = item_categories.find_by(id: params[:category])&.name
    search_text = "with keyword \"#{params[:search]}\"" if params[:search].presence

    [
      "Showing #{min} - #{max}",
      "from #{@total_count}",
      category,
      'item'.pluralize(@total_count),
      search_text
    ].compact.join(' ') << '.'
  end

  def contact_summary_html_for(poster)
    messages = ["Contact #{poster.first_name} at #{email_html_link_for(poster.email)}"]

    phone = poster.phone_number
    messages << "or call #{phone_number_html_link_for(phone)}" if phone.present?

    messages.join(' ') << '.'
  end

  def price_option_label_for(post)
    post_price_options = Post.price_options
    price_option_id = post_price_options[post.price_option]

    case price_option_id
    when post_price_options[:default]
      number_to_currency(post.price)
    when post_price_options[:per_item]
      "#{number_to_currency(post.price)} each"
    else
      PRICE_OPTIONS_LABELS.select { |l| l[:id] == price_option_id }.first.label
    end
  end
end
