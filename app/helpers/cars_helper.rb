# frozen_string_literal: true

module CarsHelper
  def last_booking_summary_for(car)
    booking = last_booking_for(car)

    return 'This car has not been booked yet.' unless booking

    "Last booking: #{booking.day_time_summary}, by #{booking.user.full_name}."
  end

  def upcoming_booking_summary_for(car)
    booking = upcoming_booking_for(car)

    return 'No upcoming booking yet.' unless booking

    "Next booking: #{booking.day_time_summary}, by #{booking.user.full_name}."
  end

  private

  def last_booking_for(car)
    CarBooking.includes(:user).past.where(car_id: car.id).order(end_time: :desc).first
  end

  def upcoming_booking_for(car)
    CarBooking.includes(:user).upcoming.where(car_id: car.id).order(start_time: :asc).first
  end
end
