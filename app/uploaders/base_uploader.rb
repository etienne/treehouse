# frozen_string_literal: true

class BaseUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  if Rails.env.development?
    storage :file
  else
    storage :fog
  end

  after :remove, :remove_empty_container_directory

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Remove container directories when deleting a record
  def remove_empty_container_directory(dir = store_dir)
    return unless storage.is_a?(CarrierWave::Storage::File)

    dir = Rails.root.join('public', dir)
    return unless Dir.exist?(dir) && Dir.empty?(dir)

    Dir.delete(dir)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
end
