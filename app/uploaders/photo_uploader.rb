# frozen_string_literal: true

class PhotoUploader < BaseUploader
  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url(_)
    ActionController::Base.helpers.asset_path([version_name, 'default_photo.png'].compact.join('_'))
  end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end
  process resize_to_fit: [800, 800]

  # Create different versions of your uploaded files:
  version :thumb do
    process resize_to_fill: [100, 100]
  end

  # Add an allowlist of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_allowlist
    %w[jpg jpeg png]
  end
end
