# frozen_string_literal: true

class DocumentUploader < BaseUploader
  def extension_allowlist
    %w[pdf]
  end
end
