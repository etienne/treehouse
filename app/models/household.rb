# frozen_string_literal: true

class Household < ApplicationRecord
  has_many :users
  has_many :kids
  has_many :eligibilities
  has_many :meal_payments

  validates_presence_of :name
end
