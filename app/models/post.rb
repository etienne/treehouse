# frozen_string_literal: true

class Post < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search_post, against: %i[title description]

  belongs_to :user
  belongs_to :item_category

  mount_uploaders :photos, ::PhotoUploader

  scope :by_most_recent_first, -> { order(created_at: :desc) }

  validates_presence_of :title, :description, :item_category, :user, :price_option
  validate :max_number_of_photos

  enum :price_option, {
    default: 0,
    per_item: 1,
    see_description: 2,
    contact_seller: 3,
    barter: 4,
    best_offer: 5,
    free: 6,
    borrow: 7
  }

  private

  def max_number_of_photos
    errors.add(:base, 'You cannot upload more than 3 photos.') if photos.count > 3
  end
end
