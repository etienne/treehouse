# frozen_string_literal: true

class Meal < ApplicationRecord
  has_many :meal_portions, dependent: :destroy
  has_many :meal_seatings, dependent: :destroy
  has_many :meal_payments, dependent: :destroy
  has_many :meal_payment_attendees, dependent: :destroy
  has_many :meal_team_members, dependent: :destroy
  belongs_to :user

  accepts_nested_attributes_for :meal_portions, reject_if: proc { |attrs| attrs[:description].blank? || attrs[:amount].blank? }
  accepts_nested_attributes_for :meal_seatings

  validates_presence_of :meal_date, :title, :description, :user
  validate :start_date_before_today
  validate :check_hall_availability, on: :create

  scope :upcoming, -> { where('(meal_date >= ?)', Date.current) }
  scope :past, -> { where('(meal_date < ?)', Date.current) }
  scope :remindable, -> { where(closing_reminded_at: false) }
  scope :opened, -> { where(closed: false) }
  scope :closed, -> { where(closed: true) }

  REGISTRATION_THRESHOLD_DAY = 3.days

  def attendence_for?(attendee)
    members = [attendee]
    household = attendee.household
    members += household.users if household

    payment = MealPayment.where(meal: self).where('user_id in (?)', members.map(&:id)).first

    payment.present?
  end

  def meal_lead
    return nil unless meal_team_members

    meal_team_members.find_by(role: 'Meal lead')&.user
  end

  def community_fund_contribution
    return unless cost

    cost - meal_payments.sum(&:amount)
  end

  def portions_chosen_by?(user:)
    MealPortionChoice.includes(:meal_portion).where(meal_payment: payment_for_user(user))
  end

  def meal_portion_choices_for(user)
    payment = payment_for_user(user)
    return [] unless payment

    payment.meal_portion_choices.includes(:meal_portion)
  end

  def serialize_for_calendar_for(current_user)
    {
      title: title,
      start: meal_date,
      end: meal_date,
      meal_id: id,
      period: "#{meal_date_mention}, at #{seating_times}",
      description: description,
      portions: meal_portions.map { |p| "#{p.description} ($#{print_amount(p.amount)})" }.join(' / '),
      color: past_meal? ? Booking::PAST_BOOKING_COLOR : booking_color_to_use,
      past_meal: past_meal?,
      attending: attendence_for?(current_user),
      sign_up_link: url_for_sign_up,
      meal_link: url_for_meal,
      update_reg_link: url_for_update_registration(current_user),
      too_late: too_late_to_register?
    }
  end

  def spots_left_by_seating_time
    meal_seatings.includes(:meal_payments).inject({}) do |hash, seating|
      payments = seating.meal_payments
      number_of_guests = payments.sum(&:guests)
      spots_left = max_attendees_per_seating - (payments.map(&:meal_payment_attendees).sum([]).count + number_of_guests)
      hash[seating.meal_time.to_s] = spots_left

      hash
    end
  end

  def max_attendees_per_seating
    meal_input['limit_per_seating']
  end

  def self.user_managing_etransfers
    User.find_by(id: meal_file['user_managing_etransfers_id'])
  end

  def too_late_to_register?
    return false unless meal_date

    deadline = meal_date - REGISTRATION_THRESHOLD_DAY

    Date.current > deadline
  end

  def simple_meal_attendees
    attendees_by_time.map do |meal_time, attendees|
      guest_number = attendees.map(&:meal_payment).uniq.sum(&:guests)
      member_number = attendees.map(&:meal_payment).count
      total = guest_number + member_number

      seating_label_with_total_for(meal_time, total)
    end
  end

  def simple_meal_attendees_v2
    meal_seatings.map do |seating|
      num_attendees = seating.number_of_attendees
      next unless num_attendees.positive?

      seating_label_with_total_for(seating.time_or_take_out_label, num_attendees)
    end.compact
  end

  def detailed_meal_attendees(breakdown: false)
    attendees_by_time.map do |meal_time, attendees|
      households = []
      all_names = ''

      if breakdown
        attendees.map(&:meal_payment).uniq.each do |mp|
          names = mp.meal_payment_attendees.map(&:attendee).map(&:first_name)
          names << "#{mp.guests} guest(s)" if mp.guests.positive?
          portions = mp.meal_portion_choices.map { |p| "#{p.meal_portion.description}: #{p.quantity}" }

          households << { name: names.join(', '), portions: portions }
        end
      else
        member_names = attendees.map(&:attendee).map(&:first_name).join(', ')
        guests = attendees.map(&:meal_payment).uniq.map(&:guests).sum(&:to_i)

        all_names = guests >= 1 ? "#{member_names}, #{guests} guest(s)" : member_names
      end

      { meal_time: meal_time, seating: seating_label_for(meal_time), names: all_names, households: households }
    end
  end

  def past_meal?
    return false unless meal_date

    meal_date < Date.current
  end

  def unassign_household_from_team(household:)
    MealTeamMember.where(meal: self, user: household.users).each do |team_member|
      team_member.update(user_id: nil)
    end
  end

  def meal_seatings_for(current_user)
    return meal_seatings if current_user.meal_v2_enabled?

    meal_seatings.reject { |s| s.meal_time.nil? }
  end

  def self.closed_to_csv
    CSV.generate(col_sep: ',') do |csv|
      csv << %w[Title Date Revenue Cost Diff]
      closed.includes(:meal_payments).find_each do |meal|
        revenue = meal.meal_payments.sum(&:amount).round(2)
        cost = meal.cost.round(2)
        values = [meal.title, meal.meal_date, revenue, cost, (revenue - cost).round(2)]
        csv << values
      end
    end
  end

  private

  def meal_date_mention
    meal_date.strftime('%A %B %d').strip
  end

  def seating_times
    meal_seatings.map { |s| s.meal_time&.strftime('%I.%M %P') }.compact.join(' / ')
  end

  def url_for_sign_up
    Rails.application.routes.url_helpers.meal_sign_up_url(self)
  end

  def url_for_meal
    Rails.application.routes.url_helpers.meal_url(self)
  end

  def url_for_update_registration(user)
    payment = payment_for_user(user)
    return unless payment

    Rails.application.routes.url_helpers.edit_meal_sign_up_url({ id: id, payment_id: payment.id })
  end

  def start_date_before_today
    return if changing_cost_or_closing?
    return unless meal_date && meal_date < Date.current

    errors.add(:base, 'The date of this meal cannot be before today\'s date.')
  end

  def booking_color_to_use
    '#1a5fb4'
  end

  def print_amount(value)
    return '0.00' unless value

    sprintf('%04.2f', value)
  end

  def changing_cost_or_closing?
    (changes.keys - %w[cost closed]).empty?
  end

  def meal_input
    self.class.meal_file['meals']
  end

  def self.meal_file
    @meal_file ||= YAML.load_file(Rails.root.join('config', 'data', 'meals.yml'))
  end

  def payment_for_user(user)
    MealPayment.find_by(user: user, meal: self)
  end

  def self.dining_hall
    return Room.find_by(name: 'The Gathering Hall') if Rails.env.development?

    Room.find_by(id: meal_file['dining_hall_id'])
  end

  def attendees_by_time
    MealPaymentAttendee
      .includes(:meal_payment, :attendee)
      .where(meal: self)
      .group_by { |a| a.meal_payment.seating_or_take_out_label }
      .sort.to_h
  end

  def seating_label_for(meal_time)
    meal_time == MealPayment::TAKE_OUT_LABEL ? '🏃 Take-out' : "🪑 #{meal_time} attendees"
  end

  def seating_label_with_total_for(meal_time, total)
    meal_time == MealPayment::TAKE_OUT_LABEL ? "🏃 #{total} for take-out" : "🪑 #{total} coming at #{meal_time}"
  end

  def check_hall_availability
    return unless meal_date

    existing_area_bookings =
      AreaBooking
      .where('start_time >= ? AND end_time <= ?', meal_date.beginning_of_day, meal_date.end_of_day)
      .where(room: self.class.dining_hall)

    return unless existing_area_bookings

    seatings = meal_seatings.not_take_out.map(&:meal_time)
    overlap = existing_area_bookings.any? do |booking|
      seatings.min.between?(booking.start_time, booking.end_time + 1.hour) ||
        seatings.max.between?(booking.start_time, booking.end_time + 1.hour)
    end

    return unless overlap

    errors.add(:base, "#{self.class.dining_hall.name} is already booked around the time of one seating here. Please update that seating time.")
  end
end
