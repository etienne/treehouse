# frozen_string_literal: true

class EggOrder < ApplicationRecord
  has_many :egg_payments

  validates :purchase_date, :closing_time, :price, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0.0 }
  validate :ensure_enough_boxes

  scope :next_week_orders, -> {
    where('purchase_date >= ? and purchase_date <= ?',
          Date.current.next_week.beginning_of_week,
          Date.current.next_week.end_of_week)
  }

  def delivery_within_next_week?
    (Date.current..(Date.current + 1.week)).include?(purchase_date)
  end

  def total_number_of_ordered_boxes
    egg_payments.sum(&:number_of_boxes)
  end

  def delivery_date_label
    purchase_date.strftime('%A %B %d')
  end

  def delivery_date_label_short
    purchase_date.strftime('%a %b %d')
  end

  class << self
    def revenue_for_three_last_months
      res = {}

      (0..2).each do |n|
        before = (Date.current - n.months)
        start_day = before.beginning_of_month
        end_day = before.end_of_month
        label = start_day.strftime('%B %Y')
        orders = EggOrder.includes(:egg_payments).where('purchase_date >= ? AND purchase_date <= ?', start_day, end_day)
        res[label] = {}

        egg_schedule_days.each do |egg_day|
          day = egg_day['delivery_day'].to_s
          price = egg_day['cost']
          relevant_orders = orders.select { |o| o.purchase_date.strftime('%A').downcase == day }

          total_for_month_for_given_day = relevant_orders.sum do |order|
            payments = order.egg_payments
            payments.sum { |p| p.number_of_boxes * price }
          end

          res[label][day] = total_for_month_for_given_day
        end
      end

      res
    end

    private

    def egg_schedule_days
      YAML.load_file(Rails.root.join('config', 'data', 'egg_schedule.yml'))['egg_orders']
    end
  end

  private

  def ensure_enough_boxes
    return unless max_boxes
    return if max_boxes >= total_number_of_ordered_boxes

    errors.add(:base, 'The number of available boxes cannot be lower that what has already been ordered.')
  end
end
