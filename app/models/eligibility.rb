# frozen_string_literal: true

class Eligibility < ApplicationRecord
  validates :email, presence: true, uniqueness: true

  belongs_to :household, optional: true
end
