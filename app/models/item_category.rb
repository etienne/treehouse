# frozen_string_literal: true

class ItemCategory < ApplicationRecord
  has_many :posts

  validates_presence_of :name, :description
end
