# frozen_string_literal: true

class RoomPayment < Transaction
  self.table_name = 'room_payments'

  belongs_to :user
  belongs_to :room_booking

  validates_presence_of :user, :room_booking, :amount

  def pay_now!
    paid!
  end
end
