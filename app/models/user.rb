# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable

  validates_presence_of :email, :first_name, :last_name
  validate :eligibility?, on: :create

  has_many :room_bookings
  has_many :room_payments
  has_many :car_bookings
  has_many :car_booking_payments
  has_many :area_bookings
  has_many :posts
  has_many :meals
  has_many :meal_payments
  has_many :egg_payments
  has_many :meal_payment_attendees, as: :attendee
  has_many :meal_team_members
  has_many :funds
  has_and_belongs_to_many :roles
  belongs_to :household, optional: true
  has_one :car
  has_one :user_setting, dependent: :destroy

  alias settings user_setting

  scope :active, -> { where(archived: false) }

  before_create :set_household
  after_create :create_new_personal_fund!
  after_create :create_user_settings!
  after_create :auto_confirm_admin, if: :setting_up_app?

  def full_name
    "#{first_name} #{last_name}"
  end

  def admin?
    roles_slugs.any?
  end

  def fund
    @fund ||= funds.find_by(community: false)
  end

  def guest_room_coordinator?
    roles_slugs.include?(:guest_room_coordinator)
  end

  def area_booking_coordinator?
    roles_slugs.include?(:area_booking_coordinator)
  end

  def car_booking_payment_admin?
    roles_slugs.include?(:car_booking_payment_admin)
  end

  def fund_admin?
    roles_slugs.include?(:fund_admin)
  end

  def egg_admin?
    roles_slugs.include?(:egg_admin)
  end

  def car_admin?
    roles_slugs.include?(:car_admin)
  end

  def common_meal_admin?
    roles_slugs.include?(:common_meal_admin)
  end

  def household_mention
    return full_name unless household

    "#{household.name} household"
  end

  def num_booked_nights
    bookings = RoomBooking.includes(user: :household)
                          .non_free
                          .this_year_and_next_year
                          .where(user: { household_id: household_id })
                          .group_by do |b|
                            b.start_date.year
                          end

    nights_for_bookings(bookings)
  end

  def price_to_pay_for(booking)
    nights_to_pay = 0

    booking.number_of_nights_per_year.map do |year, booking_num_of_nights|
      night_deficit = booking_num_of_nights - remaining_nights_for(year)
      nights_to_pay += night_deficit if night_deficit.positive?
    end

    nights_to_pay * RoomBooking::PRICE_PER_NIGHT
  end

  def archive!
    unless room_payment_cleared?
      errors.add(:base, 'This account cannot be archived as it has payments that are still pending.')
      return false
    end

    return false unless update!(archived: true)

    posts.each(&:destroy!)
    room_bookings.each(&:destroy!)
    eligibility&.destroy!
    # transfer_money_to_common_meals # TODO

    true
  end

  def household_members
    return [self] unless household

    household.users + household.kids
  end

  def upcoming_meals
    ids = household.present? ? household.users.map(&:id) : [id]

    Meal.includes(:meal_payments)
        .upcoming
        .where(meal_payments: { user_id: ids })
        .order(meal_date: :asc)
  end

  def admin_for_meal?(meal)
    meal_lead = meal&.meal_lead

    common_meal_admin? || self == meal_lead || household&.users&.include?(meal_lead)
  end

  def alone_in_household?
    return true unless household

    household.users.count == 1
  end

  def meal_v2_enabled?
    Flipper.enabled?(:meal_v2, self)
  end

  private

  def room_payment_cleared?
    return true unless room_payments

    room_payments.pending.none?
  end

  def eligibility
    Eligibility.find_by(email: email.downcase)&.destroy!
  end

  def set_household
    e = Eligibility.find_by(email: email.downcase)

    self.household_id = e.household.id if e&.household_id
  end

  def remaining_nights_for(year)
    [RoomBooking::MAX_NUMBER_OF_FREE_NIGHTS - num_booked_nights_for(year), 0].max
  end

  def num_booked_nights_for(year)
    bookings = RoomBooking.includes(user: :household)
                          .for_year(year)
                          .where(user: { household_id: household_id })
                          .group_by do |b|
                            b.start_date.year
                          end

    nights_for_bookings(bookings)[year] || 0
  end

  def nights_for_bookings(bookings)
    nights = {}

    bookings.map do |year, bkgs|
      nights[year] = bkgs.map(&:number_of_nights).sum
    end

    nights
  end

  def eligibility?
    return if override_eligibility_check?
    return if setting_up_app?
    return if email && eligibility.present?

    errors.add(:base, 'This email address does not belong to an eligible TVE member')
  end

  def override_eligibility_check?
    Rails.env.test?
  end

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  def roles_slugs
    @roles_slugs ||= roles.map { |r| r.slug.to_sym }
  end

  def create_new_personal_fund!
    return if fund

    Fund.create!(user: self, name: full_name, community: false)
  end

  def create_user_settings!
    UserSetting.create!(user: self)
  end

  def confirmation_required?
    return super unless setting_up_app?

    false
  end

  def setting_up_app?
    !Setting.setup_done?
  end

  def auto_confirm_admin
    confirm

    roles << Role.app_admin
  end

  # def transfer_money_to_common_meals
  #   MoneyTransferService.new(
  #     amount: fund.balance,
  #     recipient_fund: Fund.common_meals,
  #     sender_fund: fund,
  #     description: "Donation from #{full_name}"
  #   ).execute
  # end
end
