# frozen_string_literal: true

class MealPaymentAttendee < ApplicationRecord
  belongs_to :attendee, polymorphic: true
  belongs_to :meal
  belongs_to :meal_payment
end
