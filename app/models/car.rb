# frozen_string_literal: true

class Car < ApplicationRecord
  mount_uploaders :photos, ::PhotoUploader
  mount_uploaders :documents, ::DocumentUploader

  has_many :car_bookings, dependent: :destroy
  belongs_to :user, optional: true
  alias owner user

  validates_presence_of :title, :description, :odometer, :calendar_color
  validates :electric, inclusion: [true, false]
  validate :max_number_of_photos

  def title_with_emoji
    electric? ? "#{title} ⚡" : title
  end

  private

  def max_number_of_photos
    errors.add(:base, 'You cannot upload more than 3 photos.') if photos.count > 3
  end
end
