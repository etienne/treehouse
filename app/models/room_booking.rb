# frozen_string_literal: true

class RoomBooking < Booking
  self.table_name = 'room_bookings'

  belongs_to :room
  has_one :room_payment, dependent: :nullify

  scope :upcoming, -> { where('(start_date >= ?) OR (end_date > ?)', Date.current, Date.current) }
  scope :around_same_period, ->(booking) do
    where('(start_date >= ?) AND (end_date <= ?)',
          booking.start_date - 1.month,
          booking.end_date + 1.month)
  end
  scope :with_pending_confirmation, -> { where(confirmed_at: nil) }
  scope :with_payment_required, -> { where(state: states[:payment_required]) }
  scope :close_to_start_date, -> do
    where('room_bookings.start_date > ? AND room_bookings.start_date <= ?',
          Date.current,
          Date.current + 3.months)
  end
  scope :this_year_and_next_year, -> do
    where('(start_date >= ?) AND (end_date <= ?)',
          Date.current.beginning_of_year,
          Date.current.next_year.end_of_year)
  end
  scope :for_year, ->(year) do
    where('(start_date >= ?) AND (end_date <= ?)',
          Date.new(year),
          Date.new(year).end_of_year)
  end
  scope :confirmable_today, -> { where(start_date: (Date.current + CONFIRMATION_BEFORE_START_MONTHS.months)) }
  scope :to_be_reminded_about_today, -> { where(start_date: (Date.current + REMINDER_BEFORE_START_MONTHS.months)) }
  scope :non_free, -> { where(free: false) }

  validates_presence_of :start_date, :end_date, :user_id, :room_id
  validate :start_datetime_before_now, on: :create
  validate :start_datetime_and_end_datetime
  validate :validate_number_of_nights
  validate :check_datetime_overlap

  alias_attribute :start_datetime, :start_date
  alias_attribute :end_datetime, :end_date

  enum :state, { initial: 0, confirmed: 1, pending: 2, payment_required: 3 }

  MAX_NUMBER_OF_FREE_NIGHTS = 10
  PRICE_PER_NIGHT = 15
  CONFIRMATION_BEFORE_START_MONTHS = 3
  REMINDER_BEFORE_START_MONTHS = 1
  MAX_NUMBER_NIGHTS_IN_ONE_BOOKING = 10

  def summary
    num_nights = (end_date.to_date - start_date.to_date).to_i
    "#{num_nights} #{'night'.pluralize(num_nights)} in #{room.name}"
  end

  def date_summary
    start = start_date.strftime('%d %b %Y')
    enddate = end_date.strftime('%d %b %Y')

    "From #{start} to #{enddate}"
  end

  def serialize_for_calendar_for(current_user)
    super.merge(
      {
        max_nights: MAX_NUMBER_NIGHTS_IN_ONE_BOOKING,
        room: room.slug,
        roomname: room.name,
        datesummary: date_summary,
        notes: description,
        viewroomtitle: "Room booking for #{user.full_name}"
      }
    )
  end

  def success_message
    if confirmed?
      "Your guest room booking has been saved and confirmed. You're all good!"
    elsif pending?
      'Your guest room booking has been saved. ' \
        "You'll be sent an email three months before your booking starts to reconfirm it."
    elsif payment_required? && room_payment
      formatted_amount = format('%.2f', room_payment.amount)
      "Your guest room booking has been saved. You will need to pay $#{formatted_amount} to confirm it."
    end
  end

  def number_of_nights
    return 0 unless end_date && start_date

    (end_date.to_date - start_date.to_date).to_i
  end

  def number_of_nights_per_year
    current_year = start_date.year
    last_day_of_year = start_date.end_of_year
    nights = {}

    if end_date > last_day_of_year
      first_day_of_next_year = end_date.beginning_of_year

      nights[current_year] = (last_day_of_year.to_date - start_date.to_date).to_i
      nights[current_year + 1] = (end_date.to_date - first_day_of_next_year.to_date).to_i + 1
    else
      nights[current_year] = number_of_nights
      nights[current_year + 1] = 0
    end

    nights
  end

  private

  def past_booking?
    end_date < Date.current
  end

  def datetime_threshold
    Date.current
  end

  def booked_too_many_nights?
    number_of_nights > MAX_NUMBER_NIGHTS_IN_ONE_BOOKING
  end

  def validate_number_of_nights
    return unless booked_too_many_nights?

    errors.add(:base, "Room bookings cannot exceed #{MAX_NUMBER_NIGHTS_IN_ONE_BOOKING} nights.")
  end

  def booking_start_time
    "#{start_date.to_date} 12:00"
  end

  def booking_end_time
    "#{end_date.to_date} 12:00"
  end

  def booking_color_to_use
    room.calendar_color
  end

  def booking_datetime
    ''
  end

  def url_for_booking_link
    Rails.application.routes.url_helpers.bookings_guest_url(self)
  end

  def user_full_name
    description.present? ? "#{user.full_name} 💬" : user.full_name
  end
end
