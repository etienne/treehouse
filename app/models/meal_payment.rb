# frozen_string_literal: true

class MealPayment < ApplicationRecord
  belongs_to :meal
  belongs_to :user
  belongs_to :household
  belongs_to :meal_seating, optional: true
  # has_many :meal_seatings, through: :meal_seating_choices
  has_many :meal_payment_attendees, dependent: :destroy
  has_many :meal_seating_choices, dependent: :destroy
  has_many :meal_portion_choices, dependent: :destroy

  validate :registration_before_deadline, on: :create
  validate :update_registration_before_meal_date, on: :update

  after_create :process_payment
  after_save :reconcile

  enum :state, { initial: 0, paid: 1, pending: 2 }

  TAKE_OUT_LABEL = 'Take-out'

  def seating_or_take_out_label
    return TAKE_OUT_LABEL if take_out
    return '' unless meal_seating.present?
    return TAKE_OUT_LABEL if user.meal_v2_enabled? && meal_seating.meal_time.nil?

    meal_seating.meal_time.strftime('%I.%M %P')
  end

  def reimburse
    meal_date = meal.meal_date.strftime('%b %d')
    description = "Registration cancellation for #{meal_date} meal"

    MoneyTransferService.new(
      amount: amount,
      sender_fund: Fund.common_meals,
      recipient_fund: user.fund,
      sender_description: description,
      recipient_description: description
    ).execute
  end

  private

  def registration_before_deadline
    return unless meal

    return unless meal.too_late_to_register?

    errors.add(:base, 'You cannot register less than three days before the meal starts.')
  end

  def update_registration_before_meal_date
    return unless meal&.past_meal?

    errors.add(:base, 'You cannot update a registration for a meal that is over.')
  end

  def process_payment
    if fund.balance >= amount
      result = MoneyTransferService.new(
        sender_fund: fund,
        recipient_fund: Fund.common_meals,
        amount: amount,
        sender_description: transfer_message,
        recipient_description: transfer_message
      ).execute

      result[:success] ? paid! : pending!
    else
      pending!
    end
  end

  def enough_fund?
    fund.balance >= amount
  end

  def fund
    @fund ||= user.fund
  end

  def transfer_message
    "#{meal_date} meal payment"
  end

  def meal_date
    meal.meal_date.strftime('%a %b %d')
  end

  def reconcile
    amount_changes = saved_changes['amount']
    return unless amount_changes

    previous_amount = amount_changes[0]
    return unless previous_amount

    new_amount = amount_changes[1]

    if new_amount > previous_amount
      diff = new_amount - previous_amount

      MoneyTransferService.new(
        sender_fund: fund,
        recipient_fund: Fund.common_meals,
        amount: diff,
        sender_description: reconcile_payment_message,
        recipient_description: reconcile_payment_message
      ).execute
    elsif previous_amount > new_amount
      diff = previous_amount - new_amount

      MoneyTransferService.new(
        sender_fund: Fund.common_meals,
        recipient_fund: fund,
        amount: diff,
        sender_description: reconcile_reimbusement_message,
        recipient_description: reconcile_reimbusement_message
      ).execute
    end
  end

  def reconcile_reimbusement_message
    "Reimbursement on #{meal_date} meal registration update"
  end

  def reconcile_payment_message
    "Payment for #{meal_date} meal registration update"
  end
end
