# frozen_string_literal: true

class MealSeatingChoice < ApplicationRecord
  belongs_to :meal_seating
  belongs_to :meal_payment
  has_many :meal_portion_choices, dependent: :destroy
  has_many :meal_seating_attendees, dependent: :destroy

  def attendee_labels(with_guests: true)
    people = attendees.map(&:first_name)
    people << "#{guests} guest(s)" if guests.positive? && with_guests

    people
  end

  def attendees
    meal_seating_attendees.map(&:attendee)
  end

  def number_of_attendees
    attendees.count + guests
  end
end
