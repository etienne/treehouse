# frozen_string_literal: true

class Transaction < ApplicationRecord
  self.abstract_class = true

  NotImplementedError = Class.new(StandardError)

  enum :state, { pending: 0, paid: 1, cancelled: 2 }

  def pay_now!
    raise NotImplementedError
  end
end
