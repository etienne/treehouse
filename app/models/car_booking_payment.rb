# frozen_string_literal: true

class CarBookingPayment < ApplicationRecord
  belongs_to :user

  validates_presence_of :period_start, :period_end, :amount, :state

  after_create :process_payment

  enum :state, { initial: 0, paid: 1, pending: 2 }

  private

  def process_payment
    renter_fund = user.fund

    if renter_fund.balance >= amount
      result = MoneyTransferService.new(
        sender_fund: renter_fund,
        recipient_fund: Fund.car_rentals,
        amount: amount,
        sender_description: sender_transfer_message,
        recipient_description: recipient_transfer_message
      ).execute

      result[:success] ? paid! : pending!
    else
      pending!
    end
  end

  def sender_transfer_message
    "Payment for car sharing (#{period_start} - #{period_end})"
  end

  def recipient_transfer_message
    "Car sharing payment (#{period_start} - #{period_end})"
  end
end
