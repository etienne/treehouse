# frozen_string_literal: true

class MealTeamMember < ApplicationRecord
  belongs_to :meal
  belongs_to :user, optional: true

  scope :unassigned, -> { where(user_id: nil) }
  scope :unassigned_and_self, ->(user) { where('user_id IS NULL or user_id = ?', user.id) }
  scope :assigned, -> { where.not(user_id: nil) }
end
