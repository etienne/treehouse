# frozen_string_literal: true

class Role < ApplicationRecord
  has_and_belongs_to_many :users

  validates_presence_of :name, :slug
  validates_uniqueness_of :slug

  def self.app_admin
    find_by(slug: 'app_admin')
  end
end
