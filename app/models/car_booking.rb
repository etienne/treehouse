# frozen_string_literal: true

class CarBooking < Booking
  self.table_name = 'car_bookings'

  belongs_to :car

  # TODO: write tests for these scopes
  scope :upcoming, -> { where('(start_time >= ?) OR (end_time > ?)', DateTime.now, DateTime.now) }
  scope :past, -> { where('end_time < ?', DateTime.now) }
  scope :for_period, ->(start_date, end_date) do
    where('start_time >= ? AND end_time <= ?', start_date.beginning_of_day, end_date.end_of_day)
  end

  after_initialize :set_expected_distance, if: :new_record?
  validate :check_before_update, on: :update
  # validate :check_before_destroy, on: :destroy

  enum :state, { scheduled: 0, verified: 1, closed: 2 }

  class << self
    def booking_summary_for(user:, period_start:, period_end:)
      bookings = for_period(period_start, period_end).where(user: user)
      return unless bookings.any?

      bookings.group_by { |b| b.start_time.to_date }
              .map { |date, booking_group| summary_for_booking_group(date, booking_group) }
    end

    def max_number_daily_billable_hours
      car_booking_data['max_number_daily_billable_hours'].to_f
    end

    def hourly_rate
      car_booking_data['hourly_rate'].to_f
    end

    private

    def summary_for_booking_group(date, booking_group)
      {
        date: date,
        usage_time: booking_group.sum(&:diff_in_hours),
        cost: [booking_group.sum(&:cost), max_cost_per_day].min
      }
    end

    def max_cost_per_day
      max_number_daily_billable_hours * hourly_rate
    end

    def car_booking_data
      YAML.load_file(Rails.root.join('config', 'data', 'car_bookings.yml'))['car_bookings']
    end
  end

  def serialize_for_calendar_for(current_user)
    super.merge(
      {
        carname: car.title,
        eventtype: 'booking',
        electric: car.electric?,
        expected: car.electric? ? expected_distance_mention : 'N/A',
        charging: car.electric? ? "#{recommended_charging_duration} " : 'N/A',
        viewcartitle: "Car booked by #{user.full_name}",
        show_link: url_for_booking_link,
        note: note || ''
      }
    )
  end

  def summary
    "#{car.title} on #{booking_date}"
  end

  def day_time_summary
    day = start_time.strftime('%a %b %-d')
    start_short = start_time.strftime('%l:%M %p').strip
    end_short = end_time.strftime('%l:%M %p').strip

    "#{day}, from #{start_short} to #{end_short}"
  end

  def booking_start_time
    start_time
  end

  def booking_end_time
    end_time
  end

  def booking_color_to_use
    car.calendar_color
  end

  def booking_datetime
    starttime = Time.parse(start_time.to_s).strftime('%l:%M %p').strip
    endtime = Time.parse(end_time.to_s).strftime('%l:%M %p').strip

    "#{booking_date}, from #{starttime} to #{endtime}"
  end

  def expected_distance_mention
    return 'Not specified' if expected_distance.zero?

    "#{expected_distance} kms"
  end

  def recommended_charging_duration
    return 'N/A' if expected_distance.zero?

    CarChargingService.new(self).event_data[:chargingduration]
  end

  def cost
    [diff_in_hours, self.class.max_number_daily_billable_hours].min * self.class.hourly_rate
  end

  def diff_in_hours
    ((end_time - start_time) / 3600.0).round(2)
  end

  # TODO: leverage ActiveRecord callbacks instead - see what's going on with on: :destroy
  def past_or_in_progress_booking?
    DateTime.now > start_time
  end

  def past_booking?
    DateTime.now > end_time
  end

  private

  def check_before_update
    return unless DateTime.now > end_time

    errors.add(:base, 'This booking cannot end before now.')
  end

  # def check_before_destroy
  #   return unless DateTime.now > start_time
  #
  #   errors.add(:base, 'This booking cannoe be canncelled as it already started.')
  #   throw :abort
  # end

  def booking_date
    Date.parse(start_time.to_s).strftime('%a %b %d')
  end

  def electric_car?
    car&.electric?
  end

  def url_for_booking_link
    Rails.application.routes.url_helpers.bookings_car_url(self)
  end

  def set_expected_distance
    self.expected_distance = 0 if expected_distance.nil? || expected_distance.negative?
  end

  def user_full_name
    note.present? ? "#{user.full_name} 💬" : user.full_name
  end
end
