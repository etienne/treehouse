# frozen_string_literal: true

class MealPortion < ApplicationRecord
  belongs_to :meal
  has_many :meal_portion_choices, dependent: :destroy

  validates_presence_of :description, :amount
  validates :amount, numericality: { greater_than_or_equal_to: 0.0 }
end
