# frozen_string_literal: true

class AreaBooking < Booking
  self.table_name = 'area_bookings'

  TIME_GAP_MAX = 4

  scope :upcoming, -> { where('(start_time >= ?) OR (end_time > ?)', DateTime.now, DateTime.now) }
  scope :with_pending_confirmation, -> { where(state: 'pending') }

  scope :around_same_period, ->(booking) do
    where('(start_time >= ?) AND (end_time <= ?)',
          (booking.start_time - 1.day).beginning_of_day,
          (booking.end_time + 1.day).end_of_day)
  end

  belongs_to :user
  belongs_to :room

  validates_presence_of :start_time, :end_time, :name, :description
  validate :start_datetime_before_now, on: :create
  validate :start_datetime_and_end_datetime
  validate :check_datetime_overlap
  validate :four_hours_max

  alias_attribute :start_datetime, :start_time
  alias_attribute :end_datetime, :end_time

  enum :level, { open_event: 0, semi_private_event: 1, private_event: 2 }

  enum :state, { confirmed: 0, pending: 1 }

  AreaBookingLevel = Struct.new(:color, :label, :id)

  def booking_start_time
    start_time
  end

  def booking_end_time
    end_time
  end

  def booking_color_to_use
    room.calendar_color
  end

  def serialize_for_calendar_for(current_user)
    super.merge(
      {
        eventtype: 'booking',
        areaname: room.name,
        title: [area_booking_level.color, warning_sign, name].compact.join(' '),
        host: user.full_name,
        arealevel: area_level,
        show_message: pending?,
        notes: description || ''
      }
    )
  end

  def area_level
    [area_booking_level.color, area_booking_level.label, 'event'].join(' ')
  end

  def booking_datetime
    starttime = Time.parse(start_time.to_s).strftime('%l:%M %p').strip
    endtime = Time.parse(end_time.to_s).strftime('%l:%M %p').strip

    "#{booking_date}, from #{starttime} to #{endtime}"
  end

  def summary
    "#{room.name} booked on #{booking_date}"
  end

  def self.green_level
    AreaBookingLevel.new('🟢', 'Open', levels['open_event'])
  end

  def self.yellow_level
    AreaBookingLevel.new('🟡', 'Semi-private', levels['semi_private_event'])
  end

  def self.red_level
    AreaBookingLevel.new('🔴', 'Private', levels['private_event'])
  end

  def self.level_color_map
    {
      'open_event' => green_level,
      'semi_private_event' => yellow_level,
      'private_event' => red_level
    }
  end

  private

  def booking_date
    Date.parse(start_time.to_s).strftime('%a %B %d')
  end

  def past_booking?
    end_time < DateTime.now
  end

  def datetime_threshold
    DateTime.now
  end

  def url_for_booking_link
    Rails.application.routes.url_helpers.bookings_area_url(self)
  end

  def user_full_name
    description.present? ? "#{user.full_name} 💬" : user.full_name
  end

  def area_booking_level
    self.class.level_color_map[level]
  end

  def warning_sign
    pending? ? '⚠️' : nil
  end

  def four_hours_max
    return unless end_time && start_time

    time_gap = (end_time - start_time) / 1.hour
    return unless time_gap > TIME_GAP_MAX.to_f

    errors.add(:base, 'Your booking cannot be more than 4 hour long.')
  end
end
