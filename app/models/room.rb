# frozen_string_literal: true

class Room < ApplicationRecord
  GUEST = 'guest'
  CH_AREA = 'ch_area'

  has_many :room_bookings
  has_many :area_bookings

  scope :for_guests, -> { where(purpose: GUEST) }
  scope :ch_areas, -> { where(purpose: CH_AREA) }

  validates_presence_of :name, :description, :calendar_color

  # TODO: create a require_approval boolean column
  def self.gathering_hall
    find(6)
  end
end
