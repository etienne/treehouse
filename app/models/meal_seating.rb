# frozen_string_literal: true

class MealSeating < ApplicationRecord
  belongs_to :meal
  has_many :meal_payments, dependent: :nullify
  has_many :meal_seating_choices, dependent: :destroy

  validates_uniqueness_of :meal_time, allow_nil: true
  validate :unique_take_out, on: :create

  scope :not_take_out, -> { where.not(meal_time: nil) }

  def take_out?
    meal_time.nil?
  end

  def time_or_take_out_label
    take_out? ? 'Take-out' : meal_time.strftime('%I.%M %P')
  end

  def time_or_take_out_label_with_seating
    take_out? ? 'Take-out' : "#{meal_time.strftime('%I.%M %P')} seating"
  end

  def spots_left
    return if take_out?

    total_number_of_attendees = meal_seating_choices.sum(&:number_of_attendees)
    meal.max_attendees_per_seating - total_number_of_attendees
  end

  def time_or_take_out_label_with_emoji
    take_out? ? "🏃 #{time_or_take_out_label}" : "🪑 #{time_or_take_out_label} attendees"
  end

  def attendees
    names = meal_seating_choices.map { |c| c.attendee_labels(with_guests: false) }.compact_blank

    num_guests = meal_seating_choices.sum(&:guests)
    names << "#{num_guests} guest(s)" if num_guests.positive?

    names.join(', ')
  end

  def number_of_attendees
    num_members = meal_seating_choices.map(&:attendees).flatten.count
    num_guests = meal_seating_choices.sum(&:guests)

    num_guests + num_members
  end

  def portion_choices_by_portion
    meal_seating_choices
      .map(&:meal_portion_choices)
      .flatten
      .group_by {|p| p.meal_portion }
  end

  private

  def unique_take_out
    return unless meal&.meal_seatings&.find_by(meal_time: nil)

    errors.add(:base, 'Please indicate a meal time.')
  end
end
