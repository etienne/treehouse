# frozen_string_literal: true

class Setting < RailsSettings::Base
  # cache_prefix { 'v1' }

  scope :application do
    field :app_name, default: 'Treehouse'
    field :app_color, default: '#056839', validates: { presence: true, length: { is: 7 } }
    field :setup_done, type: :boolean, default: false
  end

  alias setup_done? setup_done
end
