# frozen_string_literal: true

class Kid < ApplicationRecord
  belongs_to :household
  has_many :meal_payment_attendees, as: :attendee

  validates_presence_of :first_name, :last_name
end
