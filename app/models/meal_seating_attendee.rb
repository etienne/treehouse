# frozen_string_literal: true

class MealSeatingAttendee < ApplicationRecord
  belongs_to :attendee, polymorphic: true
  belongs_to :meal_seating_choice
end
