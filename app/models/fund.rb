# frozen_string_literal: true

class Fund < ApplicationRecord
  belongs_to :user, optional: true
  has_many :sent_transfers, class_name: 'Transfer', foreign_key: 'sender_id'
  has_many :received_transfers, class_name: 'Transfer', foreign_key: 'recipient_id'

  validates :balance, presence: true

  scope :community, -> { where(community: true) }
  scope :personal, -> { where(community: false) }
  scope :active, -> { where(archived: false) }

  def self.common_meals
    return Fund.community.first || Fund.first if Rails.env.development?

    Fund.find_by(id: meal_fund_id)
  end

  def self.egg_orders
    Fund.find_by(id: fund_id_for_egg_orders_fund) || common_meals
  end

  def self.car_rentals
    Fund.find_by(id: fund_id_for_car_payments) || common_meals
  end

  def transaction_presenters
    @transaction_presenters ||= begin
      presenters =
        ::SentMoneyPresenter.new(fund: self).present +
        ::ReceivedMoneyPresenter.new(fund: self).present

      presenters.sort_by { |t| t[:timestamp] }.reverse
    end
  end

  def name_for_transfer
    community? ? "\"#{name}\" fund" : user.first_name
  end

  def community_display_name
    "[Community] #{name}"
  end

  def community_display_name_with_balance
    "#{community_display_name} - $#{print_amount(balance)}"
  end

  def self.meal_fund_id
    YAML.load_file(Rails.root.join('config', 'data', 'meals.yml'))['meal_fund_id']
  end

  def self.fund_id_for_egg_orders_fund
    YAML.load_file(Rails.root.join('config', 'data', 'egg_schedule.yml'))['egg_order_recipient_fund_id']
  end

  def self.fund_id_for_car_payments
    YAML.load_file(Rails.root.join('config', 'data', 'car_bookings.yml'))['fund_id_for_car_payments']
  end

  def print_amount(value)
    sprintf('%04.2f', value)
  end
end
