# frozen_string_literal: true

class MealPortionChoice < ApplicationRecord
  belongs_to :meal_portion
  belongs_to :meal_payment
  belongs_to :meal_seating_choice, optional: true
end
