# frozen_string_literal: true

class Booking < ApplicationRecord
  self.abstract_class = true

  belongs_to :user

  scope :for_room, ->(room) { where(room: room) }

  PAST_BOOKING_COLOR = '#9d9d9d'

  def serialize_for_calendar_for(current_user)
    {
      title: user_full_name,
      start: booking_start_time,
      end: booking_end_time,
      color: past_booking? ? PAST_BOOKING_COLOR : booking_color_to_use,
      booking_id: id,
      period: booking_datetime,
      show_link: show_edit_link_for?(current_user) ? url_for_booking_link : ''
    }
  end

  def show_edit_link_for?(current_user)
    return true if current_user.id == user.id

    household = current_user.household
    return false unless household

    household.id == user.household&.id
  end

  def user_full_name
    user.full_name
  end

  def start_datetime_overlaping_with?(other_booking)
    (other_booking.start_datetime <= start_datetime) && (other_booking.end_datetime > start_datetime)
  end

  def end_datetime_overlaping_with?(other_booking)
    (other_booking.start_datetime < end_datetime) && (other_booking.end_datetime >= end_datetime)
  end

  def complete_overlap_with?(other_booking)
    (start_datetime < other_booking.start_datetime) && (end_datetime > other_booking.end_datetime)
  end

  def overlapping_other_booking?(other_bookings)
    other_bookings.any? do |booking|
      start_datetime_overlaping_with?(booking) ||
        end_datetime_overlaping_with?(booking) ||
        complete_overlap_with?(booking)
    end
  end

  def other_bookings_around_same_period_for_same_room
    bookings = self.class.around_same_period(self).for_room(room)
    bookings = bookings.where('id != ?', id) if persisted?

    bookings
  end

  def start_datetime_and_end_datetime
    return unless start_datetime && end_datetime
    return unless start_datetime > end_datetime

    errors.add(:base, 'This booking cannot end before it starts.')
  end

  def start_datetime_before_now
    return unless start_datetime
    return unless start_datetime < datetime_threshold

    errors.add(:base, 'This booking cannot start in the past.')
  end

  def check_datetime_overlap
    return unless start_datetime && end_datetime

    other_bookings = other_bookings_around_same_period_for_same_room
    return unless other_bookings
    return unless overlapping_other_booking?(other_bookings)

    errors.add(:base, 'This booking is overlapping with another booking for this same room. ' \
                      'Please review the room booking calendar.')
  end

end
