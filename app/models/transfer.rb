# frozen_string_literal: true

class Transfer < Transaction
  self.table_name = 'transfers'

  belongs_to :sender, class_name: 'Fund', optional: true
  belongs_to :recipient, class_name: 'Fund', optional: true

  validates :interac,
            length: { minimum: 6, maximum: 20, message: 'number must be between 6 and 20 characters.' },
            allow_blank: true
  validate :same_sender_recipient

  scope :without_sender, -> { where(sender: nil) }

  SenderNotEnoughMoneyError = Class.new(StandardError)

  def pay_now!
    check_sender_balance

    self.recipient_new_balance = set_recipient_new_balance!
    self.sender_new_balance = set_sender_new_balance!

    self.state = 'paid'

    save!
  end

  def masked_interac
    return nil unless interac

    length = interac.length
    sub_to_replace = interac[0..(length - 5)]
    sub_to_keep = interac[-4..-1]

    "#{sub_to_replace.gsub(/./, '*')}#{sub_to_keep}"
  end

  private

  def set_recipient_new_balance!
    return unless recipient

    recipient_new_balance = recipient.balance + amount
    recipient&.update(balance: recipient_new_balance)

    recipient_new_balance
  end

  def set_sender_new_balance!
    return unless sender

    sender_new_balance = sender.balance - amount
    sender&.update(balance: sender_new_balance)

    sender_new_balance
  end

  def check_sender_balance
    return unless sender

    raise SenderNotEnoughMoneyError if sender.balance < amount
  end

  def same_sender_recipient
    return if sender.nil? || recipient.nil?
    return unless sender == recipient

    errors.add(:base, 'The sender and recipient cannot be the same fund.')
  end
end
