# frozen_string_literal: true

class EggPayment < ApplicationRecord
  belongs_to :egg_order
  belongs_to :user

  validates :number_of_boxes, numericality: { greater_than_or_equal_to: 0 }
  validate :check_order_closing_time

  after_create :process_payment

  scope :upcoming_deliveries, -> { joins(:egg_order).where('egg_orders.purchase_date >= ?', Date.current) }

  enum :state, { initial: 0, paid: 1, pending: 2 }

  private

  def check_order_closing_time
    return unless egg_order
    return if DateTime.now < egg_order.closing_time
    return if state_changed?

    errors.add(:base, 'Sorry, the registration for this order is now closed.')
  end

  def process_payment
    amount_to_pay = number_of_boxes * egg_order.price
    purchaser_fund = user.fund

    if purchaser_fund.balance >= amount_to_pay
      result = MoneyTransferService.new(
        sender_fund: purchaser_fund,
        recipient_fund: Fund.egg_orders,
        amount: amount_to_pay,
        sender_description: sender_transfer_message,
        recipient_description: recipient_transfer_message
      ).execute

      result[:success] ? paid! : pending!
    else
      pending!
    end
  end

  def sender_transfer_message
    "Purchase of #{number_of_boxes} dozen of eggs."
  end

  def recipient_transfer_message
    "Purchase for #{egg_order.delivery_date_label_short} - #{number_of_boxes} dozen"
  end
end
